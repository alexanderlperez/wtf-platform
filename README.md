# WeTheFashion.com Platform

Full project repository with:

- Docker setups for dev, staging, testing/building migrations
- AWS utility scripts
- Platform
  - Client
  - Server

## Local Development

Make sure the following directories exist:

- app/server/public/uploads

A fully-configured development environment can be set up with `./utils/start-dev`. This is best to run in its own terminal window. It will start the docker containers, run the webpack watcher, and load the app backend with nodemon.

The environment can be ended with a Ctrl-C.

### Docker quick-boot for exploring DB data
`./utils/local-up` will load the MySQL and MongoDB docker containers together as a "migration test-bed".

### Running the WTF-App
Navigate to `app/server` and run `node app.js` to get the backend running. Use `nodemon app.js` to get live-reload of backend.

### Webpack Watcher for Frontend development
Webpack is set up in `app/`, start it up with `npm run watch:client:dev`

### Preparing EC2 instance environments

1. Make sure both wtf-mongo and wtf-app are up-to-date. Information you'll need:
  - repositories for wtf-mongo and wtf-app
  - cluster name 

2. *Update `utils/AWS.ENV` to reflect AWS configurations*

3. Run `./utils/setup-ec2-env` to upload environment files and install useful packages

### Keeping staging up-to-date

1. Run `./utils/provisition-staging-update-experimental` to compile docker images locally and push them to the repo, update the staging instance with the new images, and mirror the local DB onto staging.

## Development Tools

There are "quick build" scripts to help scaffold parts of the project.  I use them a lot, and you might find some use, too.

Run these with `npm run`:

```
build:component # simple, unstateful, presentational component, function
build:component:class # stateful component, class
build:service # just the 'export' prepped
build:view 
build:layout 
build:query # ready for gql template string
```

TODO:
- have the start-dev script pre-build `mysql-backup`
