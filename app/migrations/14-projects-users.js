const ora = require('ora');
const mysql = require('mysql-promise')();
const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
require('dotenv').load();

module.exports.id = "projects-users";

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const client = await MongoClient.connect(`mongodb://localhost:27017/wtf`)
    const db = client.db('wtf')

    const COLLECTION = 'Projects';
    const Projects = db.collection(COLLECTION);
    const Users = db.collection('Users');

    // 1. Query each project
    const allUsers = await Users.find({}).toArray();

    if (!allUsers.length) {
        done();
        return;
    }

    spinner.text = "Collecting users, saving to Projects";

    let count = 0;

    let promises = allUsers.map(function (user) {
        if(!user.sql) {
            return Promise.resolve();
        }
        return Projects.update({
            "sql.register_id":user.sql.register_id
        },{
            $set:{
                user: user._id
            }
        },{
            multi:true
        })
    })

    spinner.text = `Updating projects for ${allUsers.length} users`;
    
    Promise.all(promises)
        .then(() => {
            spinner.succeed("All Done")
            done();
        })
};

module.exports.down = function (done) {
    console.log('Running rollback');

    const COLLECTION = 'Projects';
    const Projects = this.db.collection(COLLECTION);
    Projects.update({}, { $set: { users: null } }, { multi: true })
    done();
};
