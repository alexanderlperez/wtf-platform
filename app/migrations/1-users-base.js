'use strict';

require('dotenv').load();

module.exports.id = "users-base";

module.exports.up = function (done) {
    const mysql = require('mysql-promise')();

    console.log('Connecting');

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    console.log('Connection Successful');

    // create the Users collection

    console.log('creating the collection');

    const self = this;

    this.db.createCollection('Users').then(function (collection) {
        console.log('Collection created');
        // gather the base User data from the SQL database

        return mysql.query(`
          select ml.username, 
              ml.register_id,
              ml.password,
              may.title,
              may.description,
              may.upload_resume_path,
              may.upload_brochures_path,
              mci.country_code,
              mci.phone_number,
              mci.email_address,
              mci.personal_detail,
              mr.creation_date,
              mr.user_type,
              mr.register_for,
              mr.register_type,
              mr.user_profile_url,
              mr.user_name_update,
              mr.user_gender,
              mr.dob,
              mr.first_name,
              mr.last_name,
              mr.website_url,
              mr.city,
              mr.state,
              mr.country,
              mr.zip_code,
              mr.profession,
              muid.profile_image,
              muid.timeline_image,
              cats.category_name,
              cities.name AS city_name,
              states.name AS state_name,
              countries.name AS country_name,
              countries.sortname AS country_short_name
          from member_login ml
          left join member_about_yourself may on may.register_id = ml.register_id
          left join member_contact_information mci on mci.register_id = ml.register_id
          left join member_register mr on mr.register_id = ml.register_id
          left join member_user_image_details muid on muid.register_id = ml.register_id
          left join global_categorys cats on cats.cat_id = mr.profession
          left join cities on cities.id = mr.city
          left join states on states.id = mr.state
          left join countries on countries.id = mr.country   
        `)
        .catch(function (err) {
            done(err);
        })
        .spread(res => {

            // return basic User docs
            // return res.map(entry => {
            //     return {
            //         register_id: entry.register_id,
            //         username: entry.username,
            //         password: entry.password,
            //         created_on: entry.creation_date,
            //         type: entry.user_type,
            //         register_for: entry.register_for,
            //         subscribed: entry.register_type,
            //         profile_url: entry.user_profile_url,
            //         changed_username: entry.user_name_update,

            //         about: {
            //             title: entry.title,
            //             description: entry.description,
            //             uploadResumePath: entry.upload_resume_path,
            //             uploadBrochuresPath: entry.upload_brochures_path,
            //             gender: entry.user_gender,
            //             birthdate: entry.dob,
            //         },

            //         contact: {
            //             firstName: entry.first_name,
            //             lastName: entry.last_name,
            //             phone: entry.phone_number,
            //             email: entry.email_address,
            //             personalDetail: entry.personal_detail,
            //             websiteUrl: entry.website_url,
            //         },

            //         language: {
            //             name: entry.language_name,
            //             proficiency: entry.proficiency_level,
            //         }
            //     }
            // })
            return res.map(entry => transformSqltoMongodb(entry))
        })
    })
    .then(function (Users) {
        // save the recently-created User objects to the Users collection
        Users = Users.filter((a) => {
            return a!==null
        })
        console.log('saving the newly created users');
        self.db.collection('Users', {}, function (err, collection) {
            collection.insertMany(Users, {}, function (err, res) {
                if (err) {
                    console.error('error saving users');
                    done(err);
                    return;
                }

                mysql.end();
                done();
            })
        })
    })
};

function registerType(type) {
    let types = {
        S:'student',
        F:'freelancer',
        W:'boutique',
        NU:'NU',
        NA:'NA',
        C:'C'
    }
    if(types[type]) {
        return types[type]
    }
    return 'student';
}

function getSlug(url) {
    if(!url || !url.length) {
        return '';
    }
    let splits = url.split('/');
    return splits[splits.length-1];
}

function resumePath(path) {
    if(path=='' || path==null || path=='NULL' || path==0 || path=='0') {
        return ''
    }
    return path;
}

function transformSqltoMongodb(entry) {
    if(!entry.username) {
        return null;
    }
    return {
        sql:{
            register_id: Number(entry.register_id)
        },
        username: entry.username,
        password: entry.password ? entry.password.toLowerCase() : '',

        profileImages:{
            avatar: entry.profile_image || '',
            timeline: entry.timeline_image || ''
        },

        contact:    {
            firstName: entry.first_name || '',
            lastName: entry.last_name || '',
            address: '',
            city: entry.city_name || '',
            state: {
                name: entry.state_name || '',
                short: ''
            },
            zipcode: entry.zip_code || '',
            country: {
                name: entry.country_name || '',
                short: entry.country_short_name || ''
            },
            email: entry.email_address ? entry.email_address.toLowerCase() : '',
            phone: entry.phone_number || '',
            countryCode: entry.country_code || ''
        },
        accountMeta: {
            changedUsername: Boolean(entry.user_name_update=='Y' || entry.user_name_update=='y'), // Y/N
            slug: getSlug(entry.user_profile_url),
            registerFor: entry.register_for || '',
            registerType: registerType(entry.user_type),
            subscription: (entry.register_type=='PRO' ? 'PRO' : 'FREE')
        },
        about: {
            title: entry.title || '',
            gender: entry.user_gender || '',
            description: entry.description || '',
            birthdate: entry.dob || '',
            personalDetail: 0, // Seems unused?
            profession: entry.category_name || 'Fashionista',
        },
        resume:{
            resumePath: resumePath(entry.upload_resume_path),
            brochuresPath: resumePath(entry.upload_brochures_path)
        },
        awards:[],
        education:[],
        languages: [],
        workExperience:[],

        skills: [],
        socialLinks:[{
            link: entry.website_url || '',
            type:'website'
        }],

        verifications:[],

        tokens: {
            user:'',
            timeline:'',
            notification:''
        },

        createdAt:new Date(entry.creation_date)
    }
}

module.exports.down = function (done) {
    console.log('running rollback');

    this.db.collection('Users', {}, function (err, collection) {
        if (err) {
            done(err);
            return;
        }

        collection.drop({}, function (err, res) {
            if (err) {
                done(err);
                return;
            }

            done();
        })
    })
};
