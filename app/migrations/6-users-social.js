const ora = require('ora');
const chalk = require('chalk');
const Promise = require('bluebird');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "users-social"; 

module.exports.up = async function (done) {
    const spinner = ora('Connecting to MySQL DB').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = 'Users'; 
    const Users = this.db.collection(COLLECTION);
    const usersWithTimelines = Promise
        .resolve(Users.find({}).toArray())
        .map(async user => {
            let links = await mysql
                .query(`
                    SELECT * 
                    FROM member_social_links
                    WHERE register_id=${user.sql.register_id}
                `)
                .catch(err => done(err))
                .spread(res => res)
                .map(entry => {
                    return {
                        link: entry.links,
                        type: socialType(entry.social_type)
                    }
                })

            return Users.update(
                { 'sql.register_id': user.sql.register_id },
                { $push: {
                    socialLinks:{ $each: links }
                } }
            )
        })

    spinner.text = `Updating timeline for ${usersWithTimelines.length} users`;

    Promise.all(usersWithTimelines)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

function socialType(id) {
    let types = {
        FB:'facebook',
        TW:'twitter',
        LD:'linkedin',
        GM:'googleplus'
    }
    return types[id] || 'website'
}

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

