const ora = require('ora');
const mysql = require('mysql-promise')();
const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
require('dotenv').load();

module.exports.id = "projects-likes";

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const client = await MongoClient.connect(`mongodb://localhost:27017/wtf`)
    const db = client.db('wtf')

    mysql.configure({
        host: 'localhost',
        user: 'root',
        password: 'mysqlr00t',
        database: 'wtf_old'
    });

    const COLLECTION = 'Projects';
    const Projects = db.collection(COLLECTION);
    const Users = db.collection('Users');
    const Likes = db.collection('Likes');

    spinner.text = "Collecting likes for projects";

    let count = 0;


    const query = `SELECT * FROM project_like_details`;

    let likes = await mysql.query(query)
            .catch((err) => { 
                console.log(err) 
            })
            .spread(res => {
                return res.map(like => {
                    return new Promise((resolve, reject) => {
                        if(like.project_like=='N') {
                            return resolve(null);
                        }
                        let l = {
                            user: null,
                            item: null,
                            col: 'Projects',
                            createdAt: new Date(like.like_date)
                        }
                        Users.findOne({
                            "sql.register_id":like.register_id
                        }, {_id:true}, function(err, user) {
                            if(err || !user) {
                                return resolve(null)
                            }
                            l.user = user._id
                            Projects.findOne({
                                "sql.project_id":like.project_id
                            }, {_id:true}, function(err, project) {
                                if(err || !project) {
                                    return resolve(null)
                                }
                                l.item = project._id;
                                resolve(l);
                            })
                        })
                    })
                })
            })
    Promise.all(likes).then(ls => {
        this.db.collection('Likes').insertMany(ls.filter(a => a), {}, function (err, res) {
            if (err) {
                console.error('error saving likes');
                done(err);
                return;
            }
    
            spinner.succeed("Finished migrating Project Likes");
    
            mysql.end();
            done();
        })
    })


        


};

module.exports.down = function (done) {
    console.log('Running rollback');

    // const COLLECTION = 'Projects';
    // const Projects = this.db.collection(COLLECTION);
    // Projects.update({}, { $unset: { images: "" } }, { multi: true })
    done();
};
