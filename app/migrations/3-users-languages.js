const ora = require('ora');
const chalk = require('chalk');
const Promise = require('bluebird');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "users-languages"; 

module.exports.up = async function (done) {
    const spinner = ora('Connecting to MySQL DB').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = 'Users'; 
    const Users = this.db.collection(COLLECTION);
    const usersWithTimelines = Promise
        .resolve(Users.find({}).toArray())
        .map(async user => {
            let languages = [];
            let works = await mysql
                .query(`
                    SELECT * 
                    FROM member_language_details
                    WHERE register_id=${user.sql.register_id}
                `)
                .catch(err => done(err))
                .spread(res => {
                    res.forEach((entry) => {
                        let names = entry.language_name.split(',');
                        names.forEach((name) => {
                            languages.push({
                                language: name.toLowerCase().trim(),
                                proficiency: entry.proficiency_level
                            })
                        })
                    })
                })

            return Users.update(
                { 'sql.register_id': user.sql.register_id },
                { $push: {
                    languages:{ $each: languages }
                } }
            )
        })

    spinner.text = `Updating timeline for ${usersWithTimelines.length} users`;

    Promise.all(usersWithTimelines)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

