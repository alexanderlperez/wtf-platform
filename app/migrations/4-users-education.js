const ora = require('ora');
const chalk = require('chalk');
const Promise = require('bluebird');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "users-education"; 

module.exports.up = async function (done) {
    const spinner = ora('Connecting to MySQL DB').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = 'Users'; 
    const Users = this.db.collection(COLLECTION);
    const usersWithTimelines = Promise
        .resolve(Users.find({}).toArray())
        .map(async user => {
            let educations = await mysql
                .query(`
                    SELECT 
                        med.school_name,
                        med.school_website,
                        med.degree,
                        med.edu_start_time,
                        med.edu_end_time,
                        cities.name AS city_name,
                        states.name AS state_name,
                        countries.name AS country_name,
                        countries.sortname AS country_short_name
                    FROM member_education_details med
                    left join cities on cities.id = med.city
                    left join states on states.id = med.state
                    left join countries on countries.id = med.country
                    WHERE register_id=${user.sql.register_id}
                `)
                .catch(err => done(err))
                .spread(res => res)
                .map(entry => {
                    return {
                        name: entry.school_name,
                        website: entry.school_website,
                        degree: entry.degree,
                        city: entry.city_name || '',
                        state: {
                            name: entry.state_name || '',
                            short: ''
                        },
                        country: {
                            name: entry.country_name || '',
                            short: entry.country_short_name || ''
                        },
                        entryDate:{
                            year:Number(entry.edu_start_time),
                            month:1
                        },
                        duration: {
                            years:Number(entry.edu_end_time=='Present' ? 2017 : entry.edu_end_time) - Number(entry.edu_start_time),
                            months:0
                        },
                        graduated: entry.edu_end_time!=='Present'
                    }
                })

            return Users.update(
                { 'sql.register_id': user.sql.register_id },
                { $push: {
                    education:{ $each: educations }
                } }
            )
        })

    spinner.text = `Updating timeline for ${usersWithTimelines.length} users`;

    Promise.all(usersWithTimelines)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

