const ora = require('ora');
const chalk = require('chalk');
const Promise = require('bluebird');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "users-verifications"; 

module.exports.up = async function (done) {
    const spinner = ora('Connecting to MySQL DB').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = 'Users'; 
    const Users = this.db.collection(COLLECTION);
    const usersWithTimelines = Promise
        .resolve(Users.find({}).toArray())
        .map(async user => {
            let verifications = await mysql
                .query(`
                    SELECT * 
                    FROM member_verification
                    WHERE register_id=${user.sql.register_id}
                `)
                .catch(err => done(err))
                .spread(res => res)
                .map(entry => {
                    if (!entry.type) {
                        console.log('no type for entry', entry);
                    }
                    return {
                        token: entry.token,
                        expiresAt: Date(entry.expiry_date),
                        createdAt: Date(entry.verification_date),
                        verified: entry.status=='Y',
                        type: entry.verification_type
                    }
                })

            return Users.update(
                { 'sql.register_id': user.sql.register_id },
                { $push: {
                    verifications:{ $each: verifications }
                } }
            )
        })

    spinner.text = `Updating timeline for ${usersWithTimelines.length} users`;

    Promise.all(usersWithTimelines)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

