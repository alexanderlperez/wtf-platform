const ora = require('ora');
const mysql = require('mysql-promise')();
const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
require('dotenv').load();

module.exports.id = "projects-comments";

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const client = await MongoClient.connect(`mongodb://localhost:27017/wtf`)
    const db = client.db('wtf')

    mysql.configure({
        host: 'localhost',
        user: 'root',
        password: 'mysqlr00t',
        database: 'wtf_old'
    });

    const COLLECTION = 'Projects';
    const Projects = db.collection(COLLECTION);
    const Users = db.collection('Users');
    const Comments = db.collection('Comments');

    spinner.text = "Collecting comments for projects";

    let count = 0;


    const query = `SELECT * FROM project_comment_details`;

    let comments = await mysql.query(query)
            .catch((err) => { 
                console.log(err) 
            })
            .spread(res => {
                return res.map(comment => {
                    return new Promise((resolve, reject) => {
                        console.log('comment', comment);
                        let c = {
                            user: null,
                            message: comment.post_comment,
                            item: null,
                            col: 'Projects',
                            createdAt: new Date(comment.comment_date)
                        }
                        Users.findOne({
                            "sql.register_id":comment.register_id
                        }, {_id:true}, function(err, user) {
                            if(err || !user) {
                                return resolve(null)
                            }
                            c.user = user._id
                            Projects.findOne({
                                "sql.project_id":comment.project_id
                            }, {_id:true}, function(err, project) {
                                if(err || !project) {
                                    return resolve(null)
                                }
                                c.item = project._id;
                                console.log('c', c);
                                resolve(c);
                            })
                        })
                    })
                })
            })
    Promise.all(comments).then(cs => {
        this.db.collection('Comments').insertMany(cs.filter(a => a), {}, function (err, res) {
            if (err) {
                console.error('error saving comments');
                done(err);
                return;
            }
    
            spinner.succeed("Finished migrating Project Comments");
    
            mysql.end();
            done();
        })
    })


        


};

module.exports.down = function (done) {
    console.log('Running rollback');

    // const COLLECTION = 'Projects';
    // const Projects = this.db.collection(COLLECTION);
    // Projects.update({}, { $unset: { images: "" } }, { multi: true })
    done();
};
