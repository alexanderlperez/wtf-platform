const ora = require('ora');
const mysql = require('mysql-promise')();
const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
require('dotenv').load();

module.exports.id = "projects-images";

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const client = await MongoClient.connect(`mongodb://localhost:27017/wtf`)
    const db = client.db('wtf')

    mysql.configure({
        host: 'localhost',
        user: 'root',
        password: 'mysqlr00t',
        database: 'wtf_old'
    });

    const COLLECTION = 'Projects';
    const Projects = db.collection(COLLECTION);

    // 1. Query each project
    const allProjects = await Projects.find({}).toArray();

    if (!allProjects.length) {
        done();
        return;
    }

    spinner.text = "Collecting images, saving to Projects";

    let count = 0;

    let promises = allProjects.map(function (project) {
        const query = `SELECT * FROM project_image_details WHERE project_id = "${project.project_id}"`;

        return mysql.query(query)
            .catch((err) => { 
                console.log(err) 
            })
            .spread(res => {
                const images = res.map(image => ({
                    description: image.image_description,
                    path: image.image_path,
                    thumbnail: image.image_path
                }))

                return Projects.update(
                        { 'sql.project_id': project.sql.project_id },
                        { $push: {
                            images:{ $each: images }
                        } }
                    )
            })
    })

    spinner.text = `Updating project images for ${allProjects.length} projects`;
    
    Promise.all(promises)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
    console.log('Running rollback');

    const COLLECTION = 'Projects';
    const Projects = this.db.collection(COLLECTION);
    Projects.update({}, { $unset: { images: "" } }, { multi: true })
    done();
};
