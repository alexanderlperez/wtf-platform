const ora = require('ora');
const chalk = require('chalk');
const Promise = require('bluebird');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "users-work"; 

module.exports.up = async function (done) {
    const spinner = ora('Connecting to MySQL DB').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = 'Users'; 
    const Users = this.db.collection(COLLECTION);
    const usersWithTimelines = Promise
        .resolve(Users.find({}).toArray())
        .map(async user => {
            let works = await mysql
                .query(`
                    SELECT 
                        mwe.company_name,
                        mwe.company_website,
                        mwe.position,
                        mwe.work_starting_from,
                        mwe.work_ending_in,
                        cities.name AS city_name,
                        states.name AS state_name,
                        countries.name AS country_name,
                        countries.sortname AS country_short_name
                    FROM member_work_experience mwe
                    left join cities on cities.id = mwe.city
                    left join states on states.id = mwe.state
                    left join countries on countries.id = mwe.country
                    WHERE register_id=${user.sql.register_id}  
                `)
                .catch(err => done(err))
                .spread(res => res)
                .map(entry => {
                    return {
                        companyName: entry.company_name,
                        companyWebsite: entry.company_website,
                        city: entry.city_name || '',
                        state: {
                            name: entry.state_name || '',
                            short: ''
                        },
                        country: {
                            name: entry.country_name || '',
                            short: entry.country_short_name || ''
                        },
                        title: entry.position,
                        details: '',
                        startYear: Number(entry.work_starting_from),
                        startMonth: 1,
                        endYear: entry.work_ending_in == 'Present' ? 0 : Number(entry.work_ending_in),
                        endMonth: entry.work_ending_in == 'Present' ? 0 : 1
                    }
                })

            return Users.update(
                { 'sql.register_id': user.sql.register_id },
                { $push: {
                    workExperience:{ $each: works }
                } }
            )
        })

    spinner.text = `Updating timeline for ${usersWithTimelines.length} users`;

    Promise.all(usersWithTimelines)
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

