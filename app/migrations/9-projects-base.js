const ora = require('ora');
const mysql = require('mysql-promise')();
require('dotenv').load();

module.exports.id = "projects-base";

module.exports.up = async function (done) {
    const spinner = new ora(`Connecting to the DB for ${module.exports.id}`)

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    const COLLECTION = 'Projects';

    spinner.info(`Creating the collection ${COLLECTION}`).start();

    this.db.createCollection(COLLECTION)

    const query = `
        select
            project_upload_details.*,
            global_categorys.*,
            member_register.register_type,
            gc.category_name AS set_name
        from project_upload_details
        left join global_categorys on global_categorys.cat_id = project_upload_details.category_id
        left join member_register on member_register.register_id = project_upload_details.register_id
        left join member_sets_details on member_sets_details.project_id = project_upload_details.project_id
        left join global_categorys gc on gc.cat_id = member_sets_details.set_cat_id
    `;

    const entries = await mysql.query(query)
        .catch(err => done(err)) 
        .spread(res => { 
            return res.map(entry => {
                return {
                    sql:{
                        register_id: entry.register_id,
                        project_id: entry.project_id
                    },
                    user: null,
                    
                    category: entry.category_name || 'Makeup',
                    title: entry.project_title || '',
                    description: entry.project_description || '',
                    tags: entry.project_tags ? entry.project_tags.split('#').join('').split(',').map((a) => a.trim()) : [],
                    images: [],
                    coverImage: entry.cover_image || '',
                    license: 'PUBLIC',
                    editorChoice: (entry.editor_choice=='Y' || entry.editor_choice=='y'),
                    projectPrivacy: entry.project_privacy || 'PUBLIC',
                    type: entry.register_type=='FREE' ? 'lookbook' : 'project',
                    setName:entry.set_name || '',
                    createdAt: new Date(entry.creation_date),
                
                    token: ''
                }
            });
        })

    spinner.info(`saving the newly created ${COLLECTION} entries`).start()

    this.db.collection(COLLECTION).insertMany(entries, {}, function (err, res) {
        if (err) {
            console.error('error saving users');
            done(err);
            return;
        }

        spinner.succeed("Finished migrating Projects");

        mysql.end();
        done();
    })
};

module.exports.down = function (done) {
    console.log('running rollback');

    this.db.collection('Projects', {}, function (err, collection) {
        if (err) {
            done(err);
            return;
        }

        collection.drop({}, function (err, res) {
            if (err) {
                done(err);
                return;
            }

            done();
        })
    })
};
