const ora = require('ora');
const mysql = require('mysql-promise')();
const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
require('dotenv').load();

module.exports.id = "user-follows";

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const client = await MongoClient.connect(`mongodb://localhost:27017/wtf`)
    const db = client.db('wtf')

    mysql.configure({
        host: 'localhost',
        user: 'root',
        password: 'mysqlr00t',
        database: 'wtf_old'
    });

    const Users = db.collection('Users');
    const Likes = db.collection('UserFollows');

    spinner.text = "Collecting follows for users";

    let count = 0;


    const query = `SELECT * FROM member_follow_details`;

    let follows = await mysql.query(query)
            .catch((err) => { 
                console.log(err) 
            })
            .spread(res => {
                return res.map(follow => {
                    return new Promise((resolve, reject) => {
                        let f = {
                            user:null,
                            target:null,
                            createdAt:new Date(follow.follow_date)
                        }
                        Users.findOne({
                            "sql.register_id":follow.register_id
                        }, {_id:true}, function(err, user) {
                            if(err || !user) {
                                return resolve(null)
                            }
                            f.user = user._id
                            Users.findOne({
                                "sql.register_id":follow.follow_member_id
                            }, {_id:true}, function(err, target) {
                                if(err || !target) {
                                    return resolve(null)
                                }
                                f.target = target._id;
                                resolve(f);
                            })
                        })
                    })
                })
            })
    Promise.all(follows).then(fs => {
        this.db.collection('UserFollows').insertMany(fs.filter(a => a), {}, function (err, res) {
            if (err) {
                console.error('error saving follows');
                done(err);
                return;
            }
    
            spinner.succeed("Finished migrating User Follows");
    
            mysql.end();
            done();
        })
    })


        


};

module.exports.down = function (done) {
    console.log('Running rollback');

    // const COLLECTION = 'Projects';
    // const Projects = this.db.collection(COLLECTION);
    // Projects.update({}, { $unset: { images: "" } }, { multi: true })
    done();
};
