const ora = require('ora');
const chalk = require('chalk');
require('dotenv').load();

module.exports.id = ""; // TODO: CHANGE THIS TO MIGRATION NAME

module.exports.up = async function (done) {
    const spinner = ora('Connecting to DBs').start();

    const mysql = require('mysql-promise')();
    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    spinner.succeed("Connected to database");

    const COLLECTION = ''; // TODO: change this 
    this.db.createCollection(COLLECTION).catch(err => console.error(err))

    spinner.start("Querying ..."); //TODO:

    spinner.succeed(`Got ...`); //TODO:

    spinner.info(`... queued up`) // TODO:

    Promise.all([ /* ... TODO: proper queue  */])
        .then(() => {
            spinner.succeed("All Done")
            mysql.end()
            done();
        })
};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};

