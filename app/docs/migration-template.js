const ora = require('ora');
const chalk = require('chalk');
require('dotenv').load();

module.exports.id = "user-details";

module.exports.up = function (done) {
    const mysql = require('mysql-promise')();

    const spinner = ora('Connecting to DBs').start();

    mysql.configure({
        host: process.env.MYSQL_HOST,
        user: 'root',
        password: process.env.MYSQL_PASS,
        database: 'wtf_old'
    });

    const COLLECTION = '';  // TODO: change this
    const collection = this.db.collection(COLLECTION);

    let promiseQueue = [];
    collection.find({}).toArray()
        .catch(err => {
            console.error(err);
            mysql.end();
            done(1);
        })
        .then(entries => {
            if (!entries.length) {
                console.error('No entries from ' + COLLECTION + ' found!')
                mysql.end()
                done(1);
                return;
            }

            spinner.start(`Collecting ${module.exports.id} data and saving to ${entries.length} ${COLLECTION} documents`)

            entries.forEach(async function (entry) {
                //
                // TODO: most edits here
                //

                const query = ``;

                promiseQueue.push(mysql.query(query)
                    .catch((err) => { 
                        console.log(err) 
                        mysql.end()
                        done(1);
                    })
                    .spread(async res => {
                        const row = res[0];

                        return collection.findOneAndUpdate({ _id: entry._id }, { 
                            $set: {
                                // TODO: set data here
                            } 
                        })
                    }))
            })

            Promise.all(promiseQueue)
                .then(function () {
                    spinner.stop("All Done")
                    mysql.end()
                    done();
                })
        })

};

module.exports.down = function (done) {
  // use this.db for MongoDB communication, and this.log() for logging
  done();
};
