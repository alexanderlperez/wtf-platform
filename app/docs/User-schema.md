### Schema
```
  {
      "username": "STRING", 
      "password": "HASH", 
      "created_on": "ISO DATE", 
      "type": "STRING - admin, professional, fashionista", 
      "register_for": "STRING", 
      "subscribed": "BOOLEAN", 
      "profile_url": "STRING", 
      "changed_username": "BOOLEAN", 
      "about": {
          "title": "STRING", 
          "description": "STRING", 
          "upload_resume_path": "STRING", 
          "upload_brochures_path": "STRING", 
          "gender": "STRING", 
          "birthdate": "ISO DATE" 
      },
      "contact": {
          "first_name": "STRING", 
          "last_name": "STRING", 
          "country_code": "INT", 
          "phone": "STRING", 
          "email": "STRING", 
          "personal_detail": "INT", 
          "website_url": "STRING" 
      },
      "language": {
          "name": "STRING", 
          "proficiency": "STRING" 
      }
  }
```

### Annotated w/ MySQL table/field references (+ -> table name)
```
  {
      // + member_login
      "username": "STRING", // username
      "password": "HASH", // password

      // + member_register
      "created_on": "ISO DATE", // creation_date
      "type": "STRING - admin, professional, fashionista", // user_type (need to clarify)
      "register_for": "STRING", // register_for (need to clarify)
      "subscribed": "BOOLEAN", // register_type
      "profile_url": "STRING", // user_profile_url
      "changed_username": "BOOLEAN", // user_name_update

      // + member_about_yourself
      "about": {
          "title": "STRING", // title
          "description": "STRING", // description
          "upload_resume_path": "STRING", // upload_resume_path
          "upload_brochures_path": "STRING", // upload_brochures_path
          "gender": "STRING", // + member_register.user_gender
          "birthdate": "ISO DATE" // + member_register.dob
      },

      // + member_contact_information
      "contact": {
          "first_name": "STRING", // + member_register.first_name
          "last_name": "STRING", // + member_register.last_name
          "country_code": "INT", // country_code
          "phone": "STRING", // phone_number
          "email": "STRING", // email_address
          "personal_detail": "INT", // personal_detail (need to clarify)
          "website_url": "STRING" // + member_register.website_url
      },

      // + member_language_details
      "language": {
          "name": "STRING", // language_name
          "proficiency": "STRING" // proficiency_level
      }
  }
```

