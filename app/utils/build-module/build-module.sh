#!/bin/bash
# build-module.sh: build the component:
# - directory
# - package.json
# - component skeleton


COMP_HOME=$1
TEMPL_PATH=$2
TEMPL_TYPE=$3
COMP_NAME=$4

if [ -z "$COMP_NAME" ]; then
    echo "Name argument missing: component needs a name"
    exit 1
fi

if [ -z "$TEMPL_TYPE" ]; then
    echo "Type argument missing: component needs a type"
    exit 1
fi

mkdir -p ${COMP_HOME}/${COMP_NAME} \
&& jo -p name=${COMP_NAME} private=true main="./${COMP_NAME}.js" > $COMP_HOME/$COMP_NAME/package.json \
&& ./node_modules/.bin/hbs \
    --data $COMP_HOME/$COMP_NAME/package.json \
    --stdout \
    $TEMPL_PATH > /tmp/$TEMPL_TYPE \
&& mv /tmp/$TEMPL_TYPE $COMP_HOME/$COMP_NAME/$COMP_NAME.js


