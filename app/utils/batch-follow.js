require('dotenv').config({ path: '../server/.env' });
const users = require('/tmp/usernames-wtf.json');
const stream = require('getstream');
const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);    

const SPAN = 500;
let actions = [];

for (let i = 0 ; i < users.length ; i += SPAN) {
    const usersPortion = users.slice(0 + i, i + SPAN);
    const follows = usersPortion.map((user) => {
        return { 'source': 'timeline:' +  user._id, 'target': 'user:' + user._id }
    })

    actions.push(client.followMany(follows))
}

Promise.all(actions)
    .then((res) => {
        console.log(res);
    })
