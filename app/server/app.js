require('dotenv').load();

const express = require('express');
const app = express();

const session = require('express-session')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const flash = require('connect-flash');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const mongo = require('mongodb'); 
const MongoClient = mongo.MongoClient;
const ObjectId = mongo.ObjectId;

const graphQLServer = require('apollo-server-express');
const graphqlExpress = graphQLServer.graphqlExpress;
const graphiqlExpress = graphQLServer.graphiqlExpress;
const graphQLTools = require('graphql-tools');
const makeExecutableSchema = graphQLTools.makeExecutableSchema;

const apolloUploadExpress = require('apollo-upload-server').apolloUploadExpress;

const path = require('path');
const md5 = require('md5');
const util = require('util');

const uppy = require('uppy-server');

(async function () {
    try {
        // start up mongodb
        // const client = await MongoClient.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}`)
        // const db = client.db(process.env.WTF_DB);

        const db = require('./lib/db')
        
        let dbOptions = {
            db: { native_parser: true },
            server: {
                poolSize: 5,
                reconnectTries: Number.MAX_VALUE,
                socketOptions: {
                keepAlive: 1000,
                connectTimeoutMS: 30000
            }
        }
        // replset: {
        //   poolSize: 50,
        //   reconnectTries: Number.MAX_VALUE,
        //   socketOptions: {
        //     keepAlive: 1000,
        //     connectTimeoutMS: 30000
        //   }
        // },
        }
        
        if (process.env.NODE_ENV !== 'development-local') {
            dbOptions.user = process.env.MONGODB_USERNAME
            dbOptions.pass = process.env.MONGODB_PASSWORD
            dbOptions.auth = process.env.MONGODB_AUTHDB
        }
        
        db.createConnection(dbOptions)

        const Users = require('./models/Users');
        const UserFollows = require('./models/UserFollows');

        // index.js in gql-resources/ loads the files and constructs typeDefs, resolvers
        const {typeDefs, resolvers} = require('./gql-resources')();

        app.use(cors())
        app.use(session({ secret: 'cats', resave: false, saveUninitialized: false }))
        app.use(cookieParser())
        app.use(bodyParser.urlencoded({ extended: false }));
        //////////PassportJS////////////

        app.use(
            bodyParser.json(),
            uppy.app({
                providerOptions: {
                    google: {
                        key: process.env.GOOGLE_DRIVE_KEY,
                        secret: process.env.GOOGLE_DRIVE_SECRET
                    },
                    dropbox: {
                        key: process.env.DROPBOX_KEY,
                        secret: process.env.DROPBOX_SECRET
                    },
                    instagram: {
                        key: process.env.INSTAGRAM_KEY,
                        secret: process.env.INSTAGRAM_SECRET
                    },
                    s3: {
                        getKey: (req, filename) => filename,
                        key: process.env.AWS_S3_KEY,
                        secret: process.env.AWS_S3_SECRET,
                        bucket: process.env.AWS_S3_BUCKET,
                        region: process.env.AWS_S3_REGION
                    }
                },
                server: {
                    host: 'localhost:3000',
                    protocol: 'http',
                },
                secret: 'knowsecretforyou',
                debug: true,
                filePath: path.resolve(__dirname, 'public/uploads')
            })
        )

        passport.use(new LocalStrategy(
            { passReqToCallback : true },
            async function(req, username, password, done) {
            const user = await Users
                .findOne({username})
                .lean()

            if (!user) {
                console.log('bad username');
                return done(null, false, { message: 'Incorrect username.' });
            }

            if (user.password.toLowerCase() !== md5(password).toLowerCase()) {
                console.log('bad password');
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, user);
        }));

        passport.serializeUser(function(user, done) {
            done(null, user.register_id);
        });

        passport.deserializeUser(async function(register_id, done) {
            const user = await Users
                .findOne({register_id})
                .catch((err) => done(err))

            return done(null, user)
        });

        app.use(passport.initialize());
        app.use(passport.session());
        app.use(flash());

        app.post('/register', 
            function (req, res, next) {
                if(!req.body.terms || req.body.terms=="false") {
                    res.status(400).json({
                        error:'Please agree to terms of service'
                    })
                    return;
                }

                // let firstName = '';
                // let lastName = '';

                // let properName = req.body.fullname.split(',');
                // if(properName.length > 1) {
                    // lastName = properName[0].trim();
                    // firstName = properName[1].trim();
                // } else {
                    // let name = req.body.fullname.split(' ');
                    // lastName = name.length > 1 ? name.pop() : '';
                    // firstName = name.join(' ');
                // }

                let user = new Users({
                    username: req.body.username,
                    password: md5(req.body.password).toLowerCase(),
                    // contact: {
                        // firstName: firstName,
                        // lastName: lastName,
                        // email: req.body.email
                    // },
                    // about: {
                        // profession: req.body.profession
                    // }
                })
                user.save().then(user => {
                    let follow = new UserFollows({
                        user: user._id,
                        target: user._id
                    })
                    follow.save().then(follow => {
                        res.cookie('isLoggedIn', 'true', {httpOnly: false});
                        res.cookie('username', user.username, {httpOnly: false});
                        res.status(200).json(user)
                    })
                });
            }
        );

        app.post('/login', 
            function (req, res, next) {
                passport.authenticate('local', function (err, account) { 
                    if (!account || err) {
                        console.log(err, account);
                        res.status(400);
                        res.end();
                    }

                    // good authentication, set "logged in" cookie for the UI
                    res.cookie('isLoggedIn', 'true', {httpOnly: false});
                    res.cookie('username', account.username, {httpOnly: false});
                    res.end();
                })(req, res, next)
            }
        );

        app.get('/logout', function (req, res) {
            req.logout();
            res.clearCookie('isLoggedIn');
            res.redirect('/');
        })

        app.get('/testcookie', function (req, res) {
            res.send(req.cookies)
        })

        //////////////////////

        app.use('/graphql', 
            graphqlExpress({ 
                schema: makeExecutableSchema({
                    typeDefs,
                    resolvers
                })
            })
        )

        app.use('/graphiql',
            graphiqlExpress({ endpointURL: '/graphql' })
        )


        /////////////////////
        //  Client routes  //
        /////////////////////

        app.use(express.static(__dirname + '/../client/public'));

        app.get(['/uploads/:file', '/uploads/:subdir/:file'], function (req, res) {
            const file = path.resolve(__dirname, 'public/uploads', req.params.subdir || '', req.params.file);
            res.sendFile(file)
        })

        app.get('*', function (req, res) {
            res.sendFile(path.resolve(__dirname, '../client/public', 'index.html'))
        })

        /////////////////////
        //  Start the app  //
        /////////////////////
        
        app.listen(process.env.APP_PORT, function () {
            console.log('Listening in port', process.env.APP_PORT);
        });
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
})();
