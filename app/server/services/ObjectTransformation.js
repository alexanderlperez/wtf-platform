const camelcaseKeys = require('camelcase-keys');

function ccKeys(obj) {
    return camelcaseKeys(obj, { deep: true });
}

module.exports = {
    ccKeys,
}
