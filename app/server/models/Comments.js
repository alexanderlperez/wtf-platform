'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;

let commentSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
        message: { type:String, required: true },
        item: { type: Schema.Types.ObjectId, required: true, refPath: 'col' },
        col: { type: String, required: true }
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Comments'
    }
)

commentSchema.virtual('id').get(function() {
    return this._id.toString();
})

commentSchema.plugin(StreamMongoose.activity);

commentSchema.statics.pathsToPopulate = function() {
    return ['user', 'item'];
};

commentSchema.methods.activityActorFeed = function() {
    return 'timeline';
}

commentSchema.methods.activityVerb = function() {
    return 'comment';
}

commentSchema.methods.activityActor = function() {
    var id = this.item._id || this.item;
    return 'comment:'+id;
}

commentSchema.methods.activityActorProp = function() {
    return 'item';
}

commentSchema.methods.activityObject = function() {
    return 'Comments:'+this._id;
}

commentSchema.methods.activityExtraData = function() {
    return {
        message: this.message
    };
}

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('Comments', commentSchema)
