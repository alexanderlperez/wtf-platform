'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

const Users = require('./Users');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;

let projectImageSchema = new Schema(
  {
    description: String,
    path: String,
    thumbnail: String
  }
)

let projectSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, ref: 'Users' },
    type: {
      type: String,
      enum: ['project', 'lookbook'],
      default: 'project'
    },
    category: String,
    title: String,
    description: String,
    tags: [String],
    images: [projectImageSchema],
    coverImage: String,
    license: String,
    editorChoice: {
      type:Boolean
    },
    projectPrivacy: {
      type:String,
      enum:[
        'PUBLIC',
        'PASSWORD'
      ]
    },
    setName:String,
    createdAt: {
      type:Date,
      default:() => { return new Date(); }
    },

    token: { type:String, default: '' }
  },
  {
    id: false,
    timestamps: true,
    toJSON: {virtuals: true},
    toObject: {virtuals: true},
    collection: 'Projects'
  }
)

projectSchema.virtual('id').get(function() {
  return this._id.toString();
})

projectSchema.plugin(StreamMongoose.activity);

projectSchema.pre('save', function(next) {
	if(!this.token) {
        this.token = FeedManager.getFeed('timeline', this._id.toString()).getReadOnlyToken();
    }
    if(!this.user && (this.registerId || this.register_id)) {
      var userId = this.registerId || this.register_id;
      Users.findOne({
        register_id: userId
      }).then((user) => {
        if(user)
          this.user = user._id;
        next();
      })
    } else {
      next();
    }
});

projectSchema.statics.pathsToPopulate = function() {
    return ['user'];
};

projectSchema.methods.activityActorFeed = function() {
    return 'user';
}

projectSchema.methods.activityVerb = function() {
    return 'post';
}

projectSchema.methods.activityActor = function() {
    var id = this.user._id || this.user;
    return 'Users:'+id;
}

projectSchema.methods.activityActorProp = function() {
    return 'Users';
}

projectSchema.methods.activityObject = function() {
    return 'Projects:'+this._id;
}

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('Projects', projectSchema)