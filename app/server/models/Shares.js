'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;


let shareSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
    item: { type: Schema.Types.ObjectId, required: true, refPath: 'col' },
    col: { type: String, required: true },
    token: { type: String, default:'' }
  },
  {
    id: false,
    timestamps: true,
    toJSON: {virtuals: true},
    toObject: {virtuals: true},
    collection: 'Shares'
  }
)

shareSchema.virtual('id').get(function() {
    return this._id.toString();
})

shareSchema.plugin(StreamMongoose.activity);

shareSchema.statics.pathsToPopulate = function() {
    return ['user', 'owner', 'item'];
};

shareSchema.methods.activityActorFeed = function() {
    return 'user';
}

shareSchema.methods.activityVerb = function() {
    return 'share';
}

shareSchema.methods.activityActor = function() {
    var id = this.user._id || this.user;
    return 'user:'+id;
}

shareSchema.methods.activityActorProp = function() {
    return 'user';
}

shareSchema.methods.activityObject = function() {
    return 'Shares:'+this._id;
}

// timelineShareSchema.methods.activityExtraData = function() {
//     return {
//         message: this.message,
//         record: 'TimelineShares:'+this._id
//     };
// }

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('Shares', shareSchema)