'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;


let userFollowSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
    target: { type: Schema.Types.ObjectId, required: true, ref: 'Users' }
  },
  {
    id: false,
    timestamps: true,
    toJSON: {virtuals: true},
    toObject: {virtuals: true},
    collection: 'UserFollows'
  }
)

userFollowSchema.virtual('id').get(function() {
  return this._id.toString();
})

userFollowSchema.plugin(StreamMongoose.activity);

userFollowSchema.methods.activityNotify = function() {
  if(this.target._id.toString() == this.user._id.toString()) {
    return [];
  }
	var target_feed = FeedManager.getNotificationFeed(this.target._id.toString());
	return [target_feed];
};

userFollowSchema.statics.pathsToPopulate = function() {
	return ['user', 'target'];
};

userFollowSchema.methods.activityForeignId = function() {
	return this.user._id + ':' + this.target._id;
};

userFollowSchema.methods.activityActorFeed = function() {
  return 'notification';
}

userFollowSchema.methods.activityVerb = function() {
  return 'follow';
}

userFollowSchema.post('save', function(doc) {
	if (doc.wasNew) {
		var userId = doc.user._id || doc.user;
        var targetId = doc.target._id || doc.target;
        var feed = FeedManager.client.feed('timeline', userId);

        feed.follow('user', targetId);
	}
});

userFollowSchema.post('remove', function(doc) {
    var userId = doc.user._id || doc.user;
    var targetId = doc.target._id || doc.target;

    var feed = FeedManager.client.feed('timeline', userId);
    
    feed.unfollow('user', targetId);
});

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('UserFollows', userFollowSchema)
