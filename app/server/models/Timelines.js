'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;

let timelineSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
        content: { type: String, required: true },
        cover: { type: String },
        token: { type: String, default: '' }
        // likes:[{
        //     user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
        //     createdAt: { type:Date, default:() => { return new Date() }}
        // }],
        // comments:[{
        //     user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
        //     comment: { type: String, required: true },
        //     createdAt: { type:Date, default:() => { return new Date() }}
        // }]
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Timelines'
    }
)

timelineSchema.plugin(StreamMongoose.activity);
/*
projectFollowSchema.post('save', function(doc) {
	if (doc.wasNew) {
		var userId = doc.user._id || doc.user;
    var projectId = doc.project._id || doc.project;
    var feed = FeedManager.client.feed('notification', userId);

		feed.follow('project', projectId);
	}
});

projectFollowSchema.post('remove', function(doc) {
    var feed = FeedManager.client.feed('notification', userId);
    
    feed.unfollow('project', projectId);
});
*/
// timelineSchema.methods.activityNotify = function() {
// 	var target_feed = FeedManager.getNotificationFeed(this.user._id || this.user);
// 	return [target_feed];
// };

// timelineSchema.statics.addComment = function(timelineId, comment) {
//     return this.findOneAndUpdate(
//         { _id:timelineId },
//         { $push:{ comments:comment } },
//         { new:true, runValidators:true }
//     ).then((timelineWithComment) => {
//         if(timelineWithComment) {
//             var feed = FeedManager.getNotificationFeed(comment.user);
//             feed.follow('timeline', timelineWithComment._id);
//         }
//         return timelineWithComment;
//     });
// }

// timelineSchema.statics.addLike = function(timelineId, userId) {
//     return this.findOne({
//         _id:timelineId,
//         "likes.user":userId
//     }).then((likedTimeline) => {
//         if(likedTimeline) {
//             return likedTimeline;
//         }
//         return this.findOneAndUpdate(
//             { _id:timelineId },
//             { $push:{ likes:{ user:userId } } },
//             { new:true, runValidators:true }
//         );

//     }).then((newLikedTimeline) => {
//         if(newLikedTimeline) {
//             var feed = FeedManager.getNotificationFeed(userId);
//             feed.follow('timeline', timelineWithComment._id);

//             //
//             var ownerFeed = FeedManager.getNewsFeed(timelineWithComment._id);
//             ownerFeed.addActivity({
//                 actor:"user:"+userId,
//                 verb:"like",
//                 object:"Timelines:"+timelineWithComment._id,
//                 foreign_id:"like:"+userId
//             })
//         }
//         return newLikedTimeline;
//     })

// }

// timelineSchema.statics.removeLike = function(timelineId, userId) {
//     return this.findOneAndUpdate(
//         { _id:timelineId },
//         { $pull:{ likes:{ user:userId } } },
//         { new:true, runValidators:true }
//     ).then((newLikedTimeline) => {
//         if(newLikedTimeline) {
//             var feed = FeedManager.client.feed('notification', userId);
//             feed.unfollow('timeline', timelineWithComment._id);

//             var ownerFeed =  FeedManager.getNewsFeed(timelineWithComment._id);
//             ownerFeed.removeActivity({
//                 foreign_id:"like:"+userId
//             })
//         }
//         return newLikedTimeline;
//     });


// }

timelineSchema.virtual('id').get(function() {
    return this._id.toString();
})

timelineSchema.pre('save', function(next) {
	if(!this.token) {
        this.token = FeedManager.getFeed('timeline', this._id).getReadOnlyToken();
    }
    next();
});

timelineSchema.statics.pathsToPopulate = function() {
    return ['user'];
};

timelineSchema.methods.activityActorFeed = function() {
    return 'user';
}

timelineSchema.methods.activityVerb = function() {
    return 'post';
}

timelineSchema.methods.activityActor = function() {
    var id = this.user._id || this.user;
    return 'Users:'+id;
}

timelineSchema.methods.activityActorProp = function() {
    return 'Users';
}

timelineSchema.methods.activityObject = function() {
    return 'Timelines:'+this._id;
}

timelineSchema.methods.activityExtraData = function() {
    return {
        content: this.content,
        cover: this.cover
    };
}

StreamMongoose.setupMongoose(mongoose);

// timelineSchema.virtual('commentsTotal').get(function() {
//     return this.comments.length
// })

// timelineSchema.virtual('likesTotal').get(function() {
//     return this.likes.length
// })

module.exports = mongoose.model('Timelines', timelineSchema)
