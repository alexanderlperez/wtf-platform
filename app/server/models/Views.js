'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

let viewSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, ref: 'Users' },
        userOwner: { type: Schema.Types.ObjectId, ref: 'Users' },
        item: { type: Schema.Types.ObjectId, required: true, refPath: 'col' },
        col: { type: String, required: true },
        section: { type: String } // project, project details, lookbooks, profile, about, design
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Views'
    }
)

module.exports = mongoose.model('Views', viewSchema)
