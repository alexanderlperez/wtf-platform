'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

let conversationSchema = new Schema(
    {
        users: [{ type: Schema.Types.ObjectId, required: true, ref: 'Users' }],
        lastMessage: {
            _id: { type: Schema.Types.ObjectId, ref: 'Comments' },
            user: { type: Schema.Types.ObjectId, ref: 'Users' },
            message: { type:String },
            item: { type: Schema.Types.ObjectId, refPath: 'lastMessage.col' },
            col: { type: String },
            createdAt: { type: Date }
        },
        token: { type: String }
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Conversations'
    }
)

conversationSchema.virtual('id').get(function() {
    return this._id.toString();
})

conversationSchema.virtual('lastMessage.id').get(function() {
    const isEmpty = (obj) => {
        for(var key in obj) {
            if(obj.hasOwnProperty(key)) {
                return false;
            }
        }

        return true;
    }

    const lastMessage = this.lastMessage.toObject();

    if (isEmpty(lastMessage)) {
        return '';
    } else {
        return this.lastMessage._id.toString();
    }
})

module.exports = mongoose.model('Conversations', conversationSchema)
