'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;

let likeSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
        item: { type: Schema.Types.ObjectId, required: true, refPath: 'col' },
        col: { type: String, required: true }
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Likes'
    }
)

likeSchema.virtual('id').get(function() {
    return this._id.toString();
})

likeSchema.plugin(StreamMongoose.activity);

likeSchema.statics.pathsToPopulate = function() {
    return ['user', 'item'];
};

likeSchema.methods.activityActorFeed = function() {
    return 'timeline';
}

likeSchema.methods.activityVerb = function() {
    return 'like';
}

likeSchema.methods.activityActor = function() {
    var id = this.item._id || this.item;
    return 'like:'+id;
}

likeSchema.methods.activityActorProp = function() {
    return 'item';
}

likeSchema.methods.activityObject = function() {
    return 'Likes:'+this._id;
}

// likeSchema.methods.activityExtraData = function() {
//     return {};
// }

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('Likes', likeSchema)
