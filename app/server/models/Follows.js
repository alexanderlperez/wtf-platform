'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const streamNode = require('getstream-node');

var FeedManager = streamNode.FeedManager;
var StreamMongoose = streamNode.mongoose;


let followSchema = new Schema(
  {
    user: { type: Schema.Types.ObjectId, required: true, ref: 'Users' },
    item: { type: Schema.Types.ObjectId, required: true, refPath: 'col' },
    col: { type: String, required: true }
  },
  {
    id: false,
    timestamps: true,
    toJSON: {virtuals: true},
    toObject: {virtuals: true},
    collection: 'Follows'
  }
)

followSchema.virtual('id').get(function() {
    return this._id.toString();
})

followSchema.post('save', function(doc) {
	if (doc.wasNew) {
		var userId = doc.user._id || doc.user;
        var itemId = doc.item._id || doc.item;
        var feed = FeedManager.client.feed('notification', userId);

		feed.follow('timeline', itemId);
	}
});

followSchema.post('remove', function(doc) {
    var userId = doc.user._id || doc.user;
    var itemId = doc.item._id || doc.item;
    var feed = FeedManager.client.feed('notification', userId);
    
    feed.unfollow('timeline', itemId);
});

StreamMongoose.setupMongoose(mongoose);

module.exports = mongoose.model('Follows', followSchema)