'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

let statsSchema = new Schema(
    {
        uploads: {
            type:Number,
            default:0
        },
        following: {
            type:Number,
            default:0
        },
        followers: {
            type:Number,
            default:0
        },
        views: {
            type:Number,
            default:0
        },
        favorites: {
            type:Number,
            default:0
        },
        shares: {
            type:Number,
            default:0
        }
    }
)
let userSchema = new Schema(
    {
        username: {
            type: String,
            required: true
        },
        password:{
            type: String,
            require: true
        },

        profileImages:{
            avatar:String,
            timeline:String
        },

        contact:    {
            firstName: {
                type:String,
                default:''
            },
            lastName: {
                type:String,
                default:''
            },
            address: {
                type:String,
                default:''
            },
            city: {
                type:String,
                default:''
            },
            country: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            email: {
                type:String,
                default:''
            },
            countryCode:{
                type:String,
                default:''
            },
            phone: {
                type:String,
                default:''
            },
            state: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            zipcode: {
                type:String,
                default:''
            }
        },
        accountMeta: {
            changedUsername: {
                type:Boolean, // Y/N
                default: false
            },
            slug: {
                type:String,
                default:''
            },
            registerFor: {
                type: String,
                enum:[
                    'RC', // RC
                    'RB', // RB
                    'NA', // NA
                    'CL' // CL
                ],
                default:'NA'
            },
            registerType: {
                type:String,
                enum:[
                    'student', // S
                    'freelancer', // F
                    'boutique', // W
                    'NU', // NU
                    'NA', // NA
                    'C' // C
                ],
                default:'NA'
            },
            subscription: {
                type:String,
                enum:[
                    'FREE',
                    'PRO'
                ],
                default:'FREE'
            }
        },
        about: {
            title: {
                type:String,
                default:''
            },
            gender: {
                type:String,
                default:''
            },
            description: {
                type:String,
                default:''
            },
            birthdate: {
                type:String,
                default:''
            },
            personalDetail: {
                type:Number,
                default:0
            },
            profession: {
                type: String,
                enum:[
                    'Fashionista',
                    'Makeup',
                    'Jewellery design',
                    'Hair style',
                    'Tattoo design',
                    'Fashion photography',
                    'Accessories',
                    'Embroidery',
                    'Children’s wear',
                    'Costume' ,
                    'Lingeries',
                    'Menswear',
                    'Womenwear',
                    'Sportswear',
                    'Wedding',
                    'Textile',
                    'Footwear',
                    'Sketch',
                    'Henna',
                    'Ethnicwear',
                    'Interior Design',
                    'Leather',
                    'Fashion communication',
                    'Fashion Designers',
                    'Jewellery Designer',
                    'Hair stylist',
                    'Fashion Photographers',
                    'Tattoo Artist',
                    'Fashion model',
                    'Concept Artist',
                    'Interior Designer',
                    'Textile Designer',
                    'Shoe Designer',
                    'Accessories Designer',
                    'Craftworker',
                    'Couture/Boutique',
                    'Fashion',
                    'Nail art',
                    'Costume (stage screen dance)',
                    'Lingeries/Swimwear',
                    'Sketch/CAD',
                    'Inspiration',
                    'DIY',
                    'Wedding wear',
                    'Looks',
                    'Creation &amp; Creative',
                    'NAilArt',
                    'Makeup Artist',
                    'The Look',
                    'Fitness',
                    'Fashion Merchandiser',
                    'Blogger/Writer',
                    'Fashion Stylist',
                    '3D Printing',
                    'Hand Bags',
                    'Fashion Styling',
                    'DIY Decor',
                    'Kids'
                ],
                default:'Makeup'
            },
        },
        resume:{
            resumePath: {
                type:String,
                default:''
            },
            brochuresPath: {
                type:String,
                default:''
            },
        },
        awards:[{
            organization:String,
            url:String,
            title:String,
            year:Number,
            month:Number
        }],
        education:[{
            name:String,
            website:String,
            degree:String,
            city:String,
            state: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            country: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            entryDate:{
                year:Number,
                month:Number
            },
            duration: {
                years:Number,
                months:Number
            },
            graduated: Boolean
        }],
        languages: [{
            language: String,
            proficiency: {
                type:String,
                enum:[
                    'Beginner',
                    'Advanced',
                    'Conversational',
                    'Native',
                    'Fluent'
                ]
            }
        }],
        workExperience:[{
            companyName:String,
            companyWebsite:String,
            city:String,
            state: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            country: {
                name:{
                    type:String,
                    default:''
                },
                short:{
                    type:String,
                    default:''
                }
            },
            title:String,
            details:String,
            startYear:Number,
            startMonth:Number,
            endYear:Number,
            endMonth:Number
        }],

        skills: [String],
        socialLinks:[{
            link:String,
            type:{
                type:String,
                enum:[
                    'facebook', // FB
                    'twitter', // TW
                    'linkedin', // LD
                    'googleplus', // GM
                    'website'
                ]
            }
        }],

        verifications:[{
            token:String,
            expiresAt:Date,
            createdAt:Date,
            verified:Boolean,
            type:{
                type:String,
                enum:[
                    'EMAIL',
                    'NUMBER'
                ]
            }
        }],

        tokens: {
            user:{ type:String, default:'' },
            timeline:{ type:String, default:'' },
            notification:{ type:String, default:'' },
            messages: { type: String, default: '' }
        },

        createdAt:{
            type: Date,
            default: () => new Date()
        }
    },
    {
        id: false,
        timestamps: true,
        toJSON: {virtuals: true},
        toObject: {virtuals: true},
        collection: 'Users'
    }
)

userSchema.pre('validate', function(next) {
    if(this.accountMeta.changedUsername=='Y') {
        this.accountMeta.changedUsername = true;
    }
    if(this.accountMeta.changedUsername=='N') {
        this.accountMeta.changedUsername = false;
    }
    if(this.accountMeta.changedUsername!==true && this.accountMeta.changedUsername!==false) {
        this.accountMeta.changedUsername = false;
    }
    next();
})

userSchema.virtual('id').get(function() {
    return this._id.toString();
})

userSchema.virtual('fullname').get(function() {
  return this.contact.firstName + ' ' + this.contact.lastName
})

module.exports = mongoose.model('Users', userSchema)
