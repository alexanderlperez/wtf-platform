const ccKeys = require('../services/ObjectTransformation').ccKeys;

const stream = require('getstream')
const streamNode = require('getstream-node');

module.exports = (db) => {
    const typeDef= `
        type Conversation {
            id: String
            users: [SmallUser]
            lastMessage: RawComment
            token: String
            createdAt: String
        }

        type RawComment {
            id: String
            user: String
            message: String
            item: String
            col: String
            createdAt: String
        }
    `;

    const queries = `
        getConversations(username: String!): [Conversation]
        getMessages(username: String!, conversationId: String!): [Comments]
    `;
    const mutations = `
        createConversation(username: String!, targets: [String]): Conversation
        addMessage(username: String!, conversationId: String!, message: String!): Comments
        addLikeMessage(username: String!, conversationId: String!, messageId: String!): [Likes]
        removeLikeMessage(username: String!, conversationId: String!, messageId: String!): [Likes]
        removeFollowConversation(username: String!, conversationId: String!): [Conversation]
    `;

    const Users = require('../models/Users');
    const Conversations = require('../models/Conversations');

    const Follows = require('../models/Follows');
    const Comments = require('../models/Comments');
    const Likes = require('../models/Likes');

    const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);
    const streamMongoose = new streamNode.MongooseBackend()
    const FeedManager = streamNode.FeedManager;

    const resolvers = {
        Query: {
            getConversations: async (root, {username}) => {
                const user = await Users.findOne({ username })

                if (!user) {
                    return [];
                }

                const out = await Conversations.find({
                    users: user._id
                }).populate('users', '_id id username fullname profileImages contact accountMeta about createdAt')

                return out;
            },
            getMessages: async (root, {username, conversationId}) => {
                const user = await Users.findOne({ username })

                if(!user) {
                    return [];
                }

                const convo = await Conversations.findOne({
                    _id: conversationId,
                    users: user._id
                })

                if(!convo) {
                    return [];
                }

                const out = await Comments.find({
                    item: convo._id,
                    col: 'Conversations'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return out;
            }
        },
        Mutation: {
            createConversation: async (obj, { username, targets }) => {
                const user = await Users.findOne({
                    username
                })

                if(!user) {
                    return null;
                }

                const t = await Users.find({
                    _id:{$in:targets}
                })

                let users = [user._id].concat(t.map(u => u._id));

                const convo = await Conversations.findOne({
                    users:{ $all: users, $size: users.length }
                }).populate('users', '_id id username fullname profileImages contact accountMeta about createdAt')

                if(convo) {
                    return convo
                }

                const c = new Conversations({
                    users: users,
                    lastMessageAt: null
                })

                c.token = FeedManager.getFeed('timeline', c._id.toString()).getReadOnlyToken();

                await c.save()

                await Promise.all(
                    users.map((u) => {
                        var feed = FeedManager.getFeed('messages', u.toString())
                        return feed.addActivity({
                            actor: 'Users:'+user._id.toString(),
                            verb: 'conversation',
                            object: 'Conversations:'+c._id.toString(),
                            target: 'Users:'+u.toString(),
                            foreign_id: 'Conversations:'+c._id.toString()
                        })
                    })
                )

                // await Promise.all(c.users.map((userId) => {
                //     return new Follows({
                //         user: userId,
                //         item: c._id,
                //         col: 'Conversations'
                //     }).save();
                // }))

                return c.populate('users', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            addMessage: async (obj, { username, conversationId, message }) => {
                const user = await Users.findOne({ username })
                
                if(!user) {
                    return [];
                }

                const convo = await Conversations.findOne({
                    _id: conversationId,
                    users: user._id
                })

                if(!convo) {
                    return [];
                }

                const comment = new Comments({
                    user: user._id,
                    message: message,
                    item: convo._id,
                    col: 'Conversations'
                })

                await comment.save()

                convo.lastMessage = comment;
                await convo.save();

                const follow = await Follows.findOne({
                    user: user._id,
                    item: convo._id,
                    col: 'Conversations'
                })

                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: convo._id,
                        col: 'Conversations'
                    })
                    await newFollow.save()
                }

                return comment.populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            addLikeMessage: async (obj, { username, conversationId, messageId }) => {
                const user = await Users.findOne({ username })
                
                if(!user) {
                    return [];
                }

                const convo = await Conversations.findOne({
                    _id: conversationId,
                    users: user._id
                })

                if(!convo) {
                    return [];
                }

                const comment = await Comments.findOne({
                    _id: messageId,
                    item: convo._id,
                    col: 'Conversations'
                })

                if(!comment) {
                    return [];
                }

                const currentLike = await Likes.findOne({
                    user: user._id,
                    item: comment._id,
                    col: 'Comments'
                })

                if(currentLike) {
                    return currentLike;
                }

                const like = new Likes({
                    user: user._id,
                    item: comment._id,
                    col: 'Comments'
                });

                await like.save()

                return await Likes.find({
                    item: comment._id,
                    col:'Comments'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            removeLikeMessage: async (obj, { username, conversationId, messageId }) => {
                const user = await Users.findOne({ username })
                
                if(!user) {
                    return [];
                }

                const convo = await Conversations.findOne({
                    _id: conversationId,
                    users: user._id
                })

                if(!convo) {
                    return [];
                }

                const comment = await Comments.findOne({
                    _id: messageId,
                    item: convo._id,
                    col: 'Conversations'
                })

                if(!comment) {
                    return [];
                }

                const like = await Likes.findOne({
                    user: user._id,
                    item: comment._id,
                    col: 'Comments'
                })

                if(like) {
                    await like.remove();
                }

                return await Likes.find({
                    item: comment._id,
                    col: 'Comments'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            removeFollowConversation: async (obj, { username, conversationId }) => {
                const user = await Users.findOne({ username })
                
                if(!user) {
                    return [];
                }

                const convo = await Conversations.findOne({
                    _id: conversationId,
                    users: user._id
                })

                if(!convo) {
                    return [];
                }

                const follow = await Follows.findOne({
                    user: user._id,
                    item: convo._id,
                    col: 'Conversations'
                })

                if(follow)
                    await follow.remove()

                const out = await Comments.find({
                    item: convo._id,
                    col: 'Conversations'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return out;
            }
        }
    };

    return {
        typeDef,
        resolvers,
        queries,
        mutations
    };
}
