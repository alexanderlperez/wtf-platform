const ccKeys = require('../services/ObjectTransformation').ccKeys;

const stream = require('getstream')
const streamNode = require('getstream-node');

module.exports = (db) => {
    const typeDef= ``;

    const queries = `
    lookbook(username: String!): [Project]
    getRelatedLookbooks(category: String!, tags: [String]!): [Project]
    `;
    const mutations = ``;

    const Users = require('../models/Users');
    const Projects = require('../models/Projects');

    const Comments = require('../models/Comments');
    const Likes = require('../models/Likes');
    const Shares = require('../models/Shares');
    const Follows = require('../models/Follows');

    const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);
    const streamMongoose = new streamNode.MongooseBackend()
    const FeedManager = streamNode.FeedManager;

    const resolvers = {
        Query: {
            lookbook: async (root, {username}) => {
                const user = await Users.findOne({ username })

                if (!user) {
                    return [];
                }

                const out = await Projects.find({
                    user: user._id,
                    type:'lookbook'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                const res = await Promise.all(out.map((project) => {
                    project = project.toObject();
                    return Likes.find({
                        item: project._id,
                        col: 'Projects'
                    }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt').then(likes => {
                        project.likes = likes;
                        return Shares.find({
                            item: project._id,
                            col: 'Projects'
                        })
                    }).then(shares => {
                        project.shares = shares;
                        return Comments.find({
                            item: project._id,
                            col: 'Projects'
                        }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')
                    }).then(comments => {
                        project.comments = comments;
                        if(!project.token) {
                            project.token = FeedManager.getFeed('timeline', project._id.toString()).getReadOnlyToken();
                            return Projects.update({
                                _id:project._id
                            },{
                                $set:{
                                    token:project.token
                                }
                            }).then(updated => {
                                return project;
                            })
                        }
                        return project;
                    })
                }))

                return res;
            },
            getRelatedLookbooks: async (root, {category, tags}) => {
                const byCategoryOrTags = await Projects
                    .find({
                        $and:[
                            {
                                $or: [
                                    { "category.name": category },
                                    { tags: { $in: tags } }
                                ]
                            },{
                                type:'lookbook'
                            }
                        ]
                    })
                    .limit(15)
                    .populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                const res = await Promise.all(byCategoryOrTags.map((project) => {
                    project = project.toObject();

                    console.log(project);

                    return Likes.find({
                        item: project._id,
                        col: 'Projects'
                    }).then(likes => {
                        project.likes = likes;
                        return Shares.find({
                            item: project._id,
                            col: 'Projects'
                        })
                    }).then(shares => {
                        project.shares = shares;
                        return Comments.find({
                            item: project._id,
                            col: 'Projects'
                        })
                    }).then(comments => {
                        project.comments = comments;
                        if(!project.token) {
                            project.token = FeedManager.getFeed('timeline', project._id.toString()).getReadOnlyToken();
                            return Projects.update({
                                _id:project._id
                            },{
                                $set:{
                                    token:project.token
                                }
                            }).then(updated => {
                                return project;
                            })
                        }
                        return project;
                    })
                }))

                return res;
            }
        },
        Mutation: {
        }
    };

    return {
        typeDef,
        resolvers,
        queries,
        mutations
    };
}
