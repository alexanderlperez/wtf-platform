const ccKeys = require('../services/ObjectTransformation').ccKeys;
const ObjectId = require('mongodb').ObjectID;
const _ = require('lodash');
const request = require('request-promise-native');
const path = require('path');
const fs = require('fs');

const stream = require('getstream')
const streamNode = require('getstream-node');

const GraphQLUpload = require('apollo-upload-server').GraphQLUpload;

require('dotenv').load();

module.exports = () => {
    const typeDef = `
        scalar Upload

        type User {
            id: String
            username: String
            fullname: String
            profileImages: ProfileImages
            contact: Contact
            accountMeta: AccountMeta
            about: About
            resume: Resume
            awards:[Award]
            education:[Education]
            languages:[Language]
            workExperience:[Experience]
            skills:[String]
            socialLinks:[SocialLink]
            verifications:[Verification]
            tokens: UserTokens

            createdAt: String

            isFollowing: Boolean
            isFollower: Boolean

            stats: Stats
        }

        type Location {
            name: String
            short: String
        }

        type ProfileImages {
            avatar: String
            timeline: String
        }

        type Contact {
            firstName: String 
            lastName: String 
            address: String
            city: String
            country: Location
            email: String
            countryCode: String
            phone: String 
            state: Location
            zipcode: String
        }

        type AccountMeta {
            changedUsername: Boolean
            registerFor: String
            registerType: String
            subscription: String
        }

        type About {
            title: String 
            gender: String
            description: String
            birthdate: String
            personalDetail: Int 
            profession: String
        }

        type Resume {
            resumePath: String
            brochuresPath: String
        }

        type Award {
            organization: String
            url: String
            title: String
            year: Int
            month: Int
        }

        type EntryDate {
            year: Int
            month: Int
        }

        type Duration {
            years: Int
            months: Int
        }

        type Education {
            name: String
            website: String
            degree: String
            city: String
            state: Location
            country: Location
            entryDate: EntryDate
            duration: Duration
            graduated: Boolean
        }

        type Language {
            language: String
            proficiency: String
        }

        type Experience {
            companyName: String
            companyWebsite: String
            city: String
            state: Location
            country: Location
            title: String
            details: String
            starYear: Int
            startMonth: Int
            endYear: Int
            endMonth: Int
        }

        type SocialLink {
            link: String
            type: String
        }

        type Verification {
            token: String
            expiresAt: String
            createdAt: String
            verified: Boolean
            type: String
        }

        type UserTokens {
            user: String
            timeline: String
            notification: String
        }

        type Stats {
            uploads: Int
            following: Int
            followers: Int
            views: Int
            favorites: Int
            shares: Int
        }



        type Tokens {
            api: String
            user: String
            notification: String
            timeline: String
            messages: String
            userId: String
            appId: Int
        }

        type Record {
            user: User
            project: Project
            timeline: Timeline
            message: String
        }

        type EnrichedActivities {
            actor: String
            foreign_id: String
            id: String
            message: String
            object: String
            origin: String
            record: Record
            target: String
            time: String
            verb: String
        }

        type Notification {
            activities: [EnrichedActivities]
            activity_count: Int
            actor_count: Int
            created_at: String
            group: String
            id: String
            is_read: Boolean
            is_seen: Boolean
            updated_at: String
            verb: String
        }

        type FollowRaw {
            user: String
            target: String
        }

        type SmallUser {
            id: String
            username: String
            fullname: String
            profileImages: ProfileImages
            contact: Contact
            accountMeta: AccountMeta
            about: About
            createdAt: String
        }

        type FollowFollowing {
            followers: [SmallUser]
            following: [SmallUser]
        }

        type RecommendedUser {
            id: String
            username: String
            contact: Contact
            profileImages: ProfileImages
            accountMeta: AccountMeta
            about: About
            createdAt: String
            totalLengths: Int
            projects: [Project]
        }
    `;

    const queries = `
        user(username: String!, amIFollowing:String): User
        streamTokens(username: String!): Tokens
        notifications(username: String!): [Notification]
        followersFollowing(username: String!): FollowFollowing
        getConnections(username: String!, search:String): [SmallUser]
        followsBack(username: String!, targetname: String!): Boolean
        recommendedUsers(username: String!, profession: String, country: String, state: String): [RecommendedUser]
    `;
    const mutations = `
        createUser(username: String!, firstName: String!, lastName: String!, type: String!): User
        submitBasicInfo(
            username: String!, 
            set: JSON
        ): User
        uploadResume(
            username: String!,
            file: Upload!
        ): User
        followUser(username: String!, targetname:String!): FollowRaw
        unfollowUser(username: String!, targetname:String!): FollowRaw
    `;

    const Users = require('../models/Users');
    const UserFollows = require('../models/UserFollows');
    const Projects = require('../models/Projects');

    const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);    
    const streamMongoose = new streamNode.MongooseBackend()
    const FeedManager = streamNode.FeedManager;

    const storeFS = ({ stream, fullpath }) => {
        return new Promise((resolve, reject) => 
            stream
                .on('error', error => {
                    if (stream.truncated)
                        // Delete the truncated file
                        fs.unlinkSync(fullpath);
                    reject(error);
                })
                .on('end', () =>  resolve({ fullpath }) )
                .pipe(fs.createWriteStream(fullpath))
        );
    };

    const resolvers = {
        Upload: GraphQLUpload,
        Query: {
            user: async (root, {username, amIFollowing}) => {
                const user = await Users.findOne({ username:username });
                user.isFollowing = false;
                user.isFollower = false;
                if(amIFollowing) {
                    const amI = await Users.findOne({ username: amIFollowing });
                    const follow = await UserFollows.findOne({
                        user: amI._id,
                        target: user._id
                    })
                    if(follow) {
                        user.isFollowing = true;
                    }
                    const follower = await UserFollows.findOne({
                        user: user._id,
                        target: amI._id
                    })
                    if(follower) {
                        user.isFollower = true;
                    }
                }
                user.stats = {
                    uploads: 0,
                    following: 0,
                    followers: 0,
                    views: 0,
                    favorites: 0,
                    shares: 0
                }
                return user;
            },
            streamTokens: async (root, {username}) => {
                const user = await Users.findOne({ username });

                if(!user.tokens) {
                    user.tokens = {
                        user:'',
                        timeline:'',
                        notification:'',
                        messages: ''
                    }
                }

                if(!user.tokens.user) {
                    user.tokens.user = FeedManager.getFeed('user', user._id.toString()).getReadOnlyToken();
                }

                if(!user.tokens.timeline) {
                    user.tokens.timeline = FeedManager.getFeed('timeline', user._id.toString()).getReadOnlyToken();
                }

                if(!user.tokens.notification) {
                    user.tokens.notification = FeedManager.getFeed('notification', user._id.toString()).getReadOnlyToken();
                }

                if(!user.tokens.messages) {
                    user.tokens.messages = FeedManager.getFeed('messages', user._id.toString()).getReadOnlyToken();
                }


                if(user.isModified()) {
                    await user.save()
                        .catch((res) => {
                            console.error('error', res);
                            console.error('user', user);
                        })
                }

                return {
                    api: process.env.STREAM_API_KEY,
                    user: user.tokens.user,
                    timeline:  user.tokens.timeline,
                    messages: user.tokens.messages,
                    notification: user.tokens.notification,
                    userId: user._id.toString(),
                    appId: process.env.STREAM_APP_ID
                }
            },
            notifications: async (root, {username}) => {
                const user = await Users.findOne({ username });

                const feed = FeedManager.getNotificationFeed(user._id.toString());

                const activities = await feed.get({
                    mark_read:true,
                    mark_seen:true
                })

                let results = await Promise.all(activities.results.map((r) => {
                    return streamMongoose.enrichActivities(r.activities).then((activities) => {
                        r.activities = activities.map((e) => { // temporary fix
                            if(!e.record && e.object) {
                                e.record = e.object
                            }
                            return e;
                        });
                        return r;
                    });
                }));

                return results;
            },
            followersFollowing: async (root, {username}) => {
                const user = await Users.findOne({ username });

                // Followers
                const followers = await UserFollows.find({
                    target: user._id
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                // Following
                const following = await UserFollows.find({
                    user: user._id
                }).populate('target', '_id id username fullname profileImages contact accountMeta about createdAt')

                return {
                    followers:followers.map((f) => {
                        return f.user
                    }),
                    following:following.map((f) => {
                        return f.target
                    })
                }
            },
            getConnections: async (root, {username, search}) => {
                const user = await Users.findOne({ username });

                // Followers
                const followers = await UserFollows.find({
                    target: user._id
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                // Following
                const following = await UserFollows.find({
                    user: user._id
                }).populate('target', '_id id username fullname profileImages contact accountMeta about createdAt')

                const connections = following.filter((fing) => {
                    if(fing.target._id.toString() == user._id.toString())
                        return false;

                    return followers.find((fer) => {
                        return fer.user._id.toString() == fing.target._id.toString()
                    })
                }).map((fing) => {
                    return fing.target
                })

                if(search) {
                    let regexMatch = new RegExp(search, 'i')
                    return connections.filter((con) => {
                        return con.username.search(regexMatch)!==-1 || con.fullname.search(regexMatch)!==-1
                    })
                }

                return connections;
            },
            followsBack: async (root, {username, targetname}) => {
                const user = await Users.findOne({ username });
                const target = await Users.findOne({ username: targetname });
                const following = await UserFollows.find({
                    user: target._id,
                    target: user._id
                })

                return following.length > 0;
            },
            recommendedUsers: async (root, {username, profession, country, state}) => {
                let match = {
                    "contact.firstName":{$ne:""},
                    "contact.lastName":{$ne:""},
                    "profileImages.avatar":{$ne:""}
                }
                if(profession) {
                    match['about.profession'] = profession;
                }
                if(country) {
                    match['contact.country.short'] = country;
                }
                if(state) {
                    match['contact.state.short'] = state;
                }
                const users = await Users.aggregate()
                        .match(match)
                        .lookup({
                            "from" : "Projects",
                            "localField" : "_id",
                            "foreignField" : "user",
                            "as" : "projects"
                        })
                        .project({
                            _id: 1,
                            username: 1,
                            contact: 1,
                            profileImages: 1,
                            accountMeta: 1,
                            about: 1,
                            createdAt: 1,
                            projects: {$slice:["$projects", 3]},
                            languagesLength:{$size:"$languages"},
                            workExperienceLength:{$size:"$workExperience"},
                            educationLength:{$size:"$education"},
                            skillsLength:{$size:"$skills"},
                            socialLinksLength:{$size:"$socialLinks"},
                            projectsLength:{$size:"$projects"}
                        })
                        .group({
                            _id: "$_id",
                            id: {$first:"$_id"},
                            username: {$first:"$username"},
                            contact: {$first:"$contact"},
                            profileImages: {$first:"$profileImages"},
                            accountMeta: {$first:"$accountMeta"},
                            about: {$first:"$about"},
                            createdAt: {$first:"$createdAt"},
                            projects: {$first:"$projects"},
                            projectsLength: {$first:"$projectsLength"},
                            totalLengths:{ $first:{$add:["$languagesLength","$workExperienceLength","$educationLength","$skillsLength","$socialLinksLength"]} }
                        })
                        .match({
                            "projectsLength":{$gte:3}
                        })
                        .sort({
                            totalLengths: -1,
                            projectsLength: -1
                        })
                        .limit(25)

                return users.map(user => {
                    user.projects = user.projects.map(project => {
                        project.id = project._id;
                        return project;
                    })
                    return user;
                });
            }
        },
        Mutation: {
            createUser: async (root, args) => {
                const res = await Users.insert(args);
                return await Users.findOne({_id: ObjectId(res.insertedIds[0])})
            }, 
            submitBasicInfo: async (root, {username, set}) => {
                const user = await Users.findOne({ username })
                const validFields = [
                    'profileImages',
                    'contact',
                    'about',
                    'resume',
                    'awards',
                    'education',
                    'languages',
                    'workExperience',
                    'skills',
                    'socialLinks'
                ]

                let needsSave = false;

                validFields.forEach(field => {
                    if(set[field]) {
                        if(typeof set[field] == 'object') {
                            for(childField in set[field]) {
                                user[field][childField] = set[field][childField];
                            }
                        } else {
                            user[field] = set[field];
                        }
                        needsSave = true;
                    }
                })

                if(needsSave) {
                    return await user.save();
                }

                return user;
            },
            uploadResume: async (obj, { file, username }) => {
                console.log('file stuff');
                console.log(file);
                console.log(process);

                const user = await Users.findOne({ username });

                const { stream, filename, mimetype, encoding } = await file;

                const resumeFilename = `${user.username}-${filename}`;
                const fullpath = path.join('public/uploads', resumeFilename);

                await storeFS({ stream, fullpath });

                await Users.updateOne({ username }, { $set: { "about.uploadResumePath": resumeFilename } });

                return await Users.findOne({ username });
            },
            followUser: async (obj, { username, targetname }) => {
                const user = await Users.findOne({ username });
                const target = await Users.findOne({ username:targetname });

                const following = new UserFollows({
                    user: user._id,
                    target: target._id
                })

                return await following.save();
            },
            unfollowUser: async (obj, { username, targetname }) => {
                const user = await Users.findOne({ username });
                const target = await Users.findOne({ username:targetname });

                return await UserFollows.remove({
                    user: user._id,
                    target: target._id
                })
            }
        }
    };

    return {
        typeDef,
        resolvers,
        queries,
        mutations
    }
}
