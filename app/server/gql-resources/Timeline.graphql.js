const ccKeys = require('../services/ObjectTransformation').ccKeys;

const stream = require('getstream')
const streamNode = require('getstream-node');

const GraphQLJSON = require('graphql-type-json');

module.exports = (db) => {
    const typeDef= `
        scalar JSON

        type TimelineActivity {
            actor: JSON
            content: String
            cover: String
            foreign_id: String
            id: String
            object: JSON
            origin: String
            target: String
            time: String
            verb: String
            likes: [Likes]
            comments: [Comments]
        }
        type Timeline {
            id: String
            user: SmallUser
            content: String
            cover: String
            token: String
            createdAt: String
        }
        type Shares {
            id: String
            user: String
            owner: String
            item: String
            col: String
            token: String
            createdAt: String
        }
        type Comments {
            id: String
            user: SmallUser
            message: String
            item: String
            col: String
            createdAt: String
        }
        type Likes {
            id: String
            user: SmallUser
            item: String
            col: String
            createdAt: String
        }
    `;

    const queries = `
        timeline(username: String!): [TimelineActivity]
        timelineComments(timelineId: String!): [Comments]
        timelineLikes(timelineId: String!): [Likes]
    `;
    const mutations = `
        addTimeline(username: String!, content: String!, cover:Upload): Timeline
        addTimelineShare(username: String!, timelineId: String!): Shares
        addTimelineComment(username: String!, timelineId: String!, message: String!): [Comments]
        addTimelineLike(username: String!, timelineId: String!): [Likes]
        removeTimelineLike(username: String!, timelineId: String!): [Likes]
        addShareComment(username: String!, shareId: String!, message: String!): [Comments]
        addShareLike(username: String!, shareId: String!): [Likes]
        removeShareLike(username: String!, shareId: String!): [Likes]
    `;

    const Users = require('../models/Users');
    const UserFollows = require('../models/UserFollows');
    const Timelines = require('../models/Timelines');

    const Comments = require('../models/Comments');
    const Likes = require('../models/Likes');
    const Shares = require('../models/Shares');
    const Follows = require('../models/Follows');

    const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);    
    const streamMongoose = new streamNode.MongooseBackend()
    const FeedManager = streamNode.FeedManager;

    const storeFS = ({ stream, fullpath }) => {
        return new Promise((resolve, reject) => 
            stream
                .on('error', error => {
                    if (stream.truncated)
                        // Delete the truncated file
                        fs.unlinkSync(fullpath);
                    reject(error);
                })
                .on('end', () =>  resolve({ fullpath }) )
                .pipe(fs.createWriteStream(fullpath))
        );
    };

    const processUpload = async ({ file, username }) => {
        const { stream, filename, mimetype, encoding } = await file;
        
        const fullFilename = `${username}-${filename}`;
        const fullpath = path.join('public/uploads', fullFilename);

        return storeFS({ stream, fullpath });
    };

    const resolvers = {
        JSON: GraphQLJSON,
        Query: {
            timeline: async (root, {username}) => {

                const user = await Users.findOne({ username });
                
                let enriched = [];

                try {
                    const feeds = FeedManager.getNewsFeeds(user._id.toString());

                    const activities = await feeds.timeline.get({
                        mark_read:true,
                        mark_seen:true
                    });

                    enriched = await streamMongoose.enrichActivities(activities.results);
                } catch(e) {
                    console.log('get timeline error', e);
                    enriched = [];
                }

                const res = await Promise.all(enriched.map((a) => {
                    let type = a.foreign_id.split(':')[0];

                    if(typeof a.object == 'string') {
                        return a;
                    }

                    // if(a.object.item) {
                    //     a.object.item = ccKeys(a.object.item);
                    // }

                    return Likes.find({
                        item: a.object._id,
                        col: type
                    }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt').then(likes => {
                        a.likes = likes;
                        return Comments.find({
                            item: a.object._id,
                            col: type
                        }).sort({
                            createdAt:1
                        }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt').then(comments => {
                            a.comments = comments;
                            if(!a.object.token) {
                                console.log(a, a.object);
                                a.object.token = FeedManager.getFeed('timeline', a.object._id).getReadOnlyToken();
                                return a.object.save().then((doc) => {
                                    a.object = doc;
                                    return a
                                });
                            }
                            return a;
                        })
                    })
                }))

                return res.filter((res) => {
                    return typeof res.object !== 'string';
                });
            },
            timelineComments: async (root, {timelineId}) => {

                const comments = await Comments.find({
                    item: timelineId
                }).sort({
                    createdAt:1
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return comments;
            },
            timelineLikes: async (root, {timelineId}) => {
                const likes = await Likes.find({
                    item: timelineId
                }).sort({
                    createdAt:1
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return likes;
            }
        },
        Mutation: {
            addTimeline: async (obj, { username, content, cover }) => {
                const user = await Users.findOne({ username }).lean();

                // TODO: files
                // if(cover) {
                //     const { fullpath } = await processUpload({ file:cover, username:user.username })
                // } else {
                //     const fullpath = '';
                // }

                const timeline = new Timelines({
                    user: user._id,
                    content: content,
                    cover: ''
                })

                const savedTimeline = await timeline.save()
                const timelineObj = await Timelines.findOne({_id:savedTimeline._id}).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');

                await new Follows({
                    user: user._id,
                    item: timelineObj._id,
                    col: 'Timelines'
                }).save();

                return timelineObj;
            },
            addTimelineShare: async (obj, { username, timelineId }) => {
                const user = await Users.findOne({ username });
                const timeline = await Timelines.findOne({ _id:timelineId });


                // const currentShare = await Shares.findOne({
                //     user: user._id,
                //     item: timeline._id,
                //     col: 'Timelines'
                // })

                // if(currentShare) {
                //     return currentShare;
                // }

                const share = new Shares({
                    user: user._id,
                    owner: timeline.user,
                    item: timeline._id,
                    col: 'Timelines'
                })

                await share.save();

                const follow = await Follows.findOne({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                })
                
                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: timeline._id,
                        col: 'Timelines'
                    })
                    await newFollow.save()
                }

                return share;
            },
            addTimelineComment: async (obj, { username, timelineId, message }) => {
                const user = await Users.findOne({ username });
                const timeline = await Timelines.findOne({_id:timelineId});

                const comment = new Comments({
                    user: user._id,
                    message: message,
                    item: timeline._id,
                    col: 'Timelines'
                });

                await comment.save();

                const follow = await Follows.findOne({
                    user: user._id,
                    item: timeline._id
                })

                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: timeline._id,
                        col: 'Timelines'
                    })
                    await newFollow.save()
                }

                return await Comments.find({
                    item: timeline._id,
                    col: 'Timelines'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            addTimelineLike: async (obj, { username, timelineId }) => {
                const user = await Users.findOne({ username });
                const timeline = await Timelines.findOne({_id:timelineId});

                const currentLike = await Likes.findOne({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                })

                if(currentLike) {
                    return currentLike;
                }

                const like = new Likes({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                });

                await like.save()

                const follow = await Follows.findOne({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                })
                
                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: timeline._id,
                        col: 'Timelines'
                    })
                    await newFollow.save()
                }

                return await Likes.find({
                    item: timeline._id,
                    col:'Timelines'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            removeTimelineLike: async (obj, { username, timelineId }) => {
                const user = await Users.findOne({ username }).lean();
                const timeline = await Timelines.findOne({ _id:timelineId }).lean();

                const like = await Likes.findOne({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                })

                const follow = await Follows.findOne({
                    user: user._id,
                    item: timeline._id,
                    col: 'Timelines'
                })

                if(follow)
                    await follow.remove()

                if(like)
                    await like.remove()

                return await Likes.find({
                    item: timeline._id,
                    col: 'Timelines'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            addShareComment: async (obj, { username, shareId, message }) => {
                const user = await Users.findOne({ username }).lean();
                const share = await Shares.findOne({_id:shareId}).lean();

                const comment = new Comments({
                    user: user._id,
                    message: message,
                    item: share._id,
                    col: 'Shares'
                });

                await comment.save();

                const follow = await Follows.findOne({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                })

                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: share._id,
                        col: 'Shares'
                    })
                    await newFollow.save()
                }

                return await Comments.find({
                    item: share._id,
                    col: 'Shares'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            addShareLike: async (obj, { username, shareId }) => {
                const user = await Users.findOne({ username }).lean();
                const share = await Shares.findOne({_id:shareId}).lean();

                const currentLike = await Likes.findOne({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                })

                if(currentLike) {
                    return currentLike;
                }

                const like = new Likes({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                });

                await like.save()

                const follow = await Follows.findOne({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                })
                
                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: share._id,
                        col: 'Shares'
                    })
                    await newFollow.save()
                }

                return await Likes.find({
                    item: share._id,
                    col:'Shares'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            removeShareLike: async (obj, { username, shareId }) => {
                const user = await Users.findOne({ username }).lean();
                const share = await Shares.findOne({ _id:shareId }).lean();

                const like = await Likes.findOne({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                })

                const follow = await Follows.findOne({
                    user: user._id,
                    item: share._id,
                    col: 'Shares'
                })

                if(follow)
                    await follow.remove()

                if(like)
                    await like.remove()

                return await Likes.find({
                    item: share._id,
                    col: 'Shares'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            }
        }
    };

    return {
        typeDef,
        resolvers,
        queries,
        mutations
    };
}
