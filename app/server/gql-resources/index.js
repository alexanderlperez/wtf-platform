const glob = require('glob');
const path = require('path');
const lodash = require('lodash');

module.exports = () => {
    const schemas = glob
        .sync(__dirname + '**/*.graphql.js')
        .map(file => require(path.resolve(file))());

    const queries = `type Query {
        ${schemas.map(schema => schema.queries).join(`
        `)}
    }`;

    const mutations = `type Mutation {
        ${schemas.map(schema => schema.mutations).join(`
        `)}
    }`;

    return {
        typeDefs: schemas.map(schema => schema.typeDef).concat([queries, mutations]),
        resolvers: lodash.merge.apply(lodash, schemas.map(schema => schema.resolvers)),
    }
}



