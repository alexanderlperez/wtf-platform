const path = require('path');
const fs = require('fs');
const ccKeys = require('../services/ObjectTransformation').ccKeys;

const stream = require('getstream')
const streamNode = require('getstream-node');

const gm = require('gm');
const http = require('http');
const https = require('https');
const streamBuffers = require('stream-buffers');

const GraphQLUpload = require('apollo-upload-server').GraphQLUpload;

module.exports = () => {
    const typeDef= `
        type Project {
            id: String
            user: String
            type: String
            category: String
            title: String
            description: String
            tags: [String]
            images: [ProjectImage]
            coverImage: String
            license: String
            editorChoice: Boolean
            projectPrivacy: String
            setName: String
            createdAt: String
            token: String

            likes: [Likes]
            shares: [Shares]
            comments: [Comments]
        }

        type ProjectImage {
            description: String
            path: String
            thumbnail: String
        }
    `;

    const queries = `
        allProjects(username: String!): [Project]
        getRelatedProjects(category: String!, tags: [String]!): [Project]
        getProjectShares(projectId: String!): [Shares]
        projectComments(projectId: String!): [Comments]
        projectLikes(projectId: String!): [Likes]
    `;

    const mutations = `
        createProject(
            username: String!
            title: String!
            category: String!
            description: String
            projectPrivacy: String
            license: String
            tags: [String]
            images: Upload,
            type: String
        ): Project
        addCoverImage(
            username: String!
            projectId: String!
            coverImage: String!
        ): Project
        addProjectComment(
            username: String!
            projectId: String!
            message: String
        ): [Comments]
        addProjectShare(username: String!, projectId: String!): Shares
        addProjectLike(username: String!, projectId: String!): [Likes]
        removeProjectLike(username: String!, projectId: String!): [Likes]
    `;

    const Users = require('../models/Users');
    const Projects = require('../models/Projects');

    const Comments = require('../models/Comments');
    const Likes = require('../models/Likes');
    const Shares = require('../models/Shares');
    const Follows = require('../models/Follows');

    const client = stream.connect(process.env.STREAM_API_KEY, process.env.STREAM_API_SECRET, process.env.STREAM_APP_ID);
    const streamMongoose = new streamNode.MongooseBackend()
    const FeedManager = streamNode.FeedManager;

    const storeFS = ({ stream, fullpath }) => {
        return new Promise((resolve, reject) => 
            stream
                .on('error', error => {
                    if (stream.truncated)
                        // Delete the truncated file
                        fs.unlinkSync(fullpath);
                    reject(error);
                })
                .on('end', () =>  resolve({ fullpath }) )
                .pipe(fs.createWriteStream(fullpath))
        );
    };

    const processUpload = async ({ file, username }) => {
        const { stream, filename, mimetype, encoding } = await file;
        
        const fullFilename = `${username}-${filename}`;
        const fullpath = path.join('public/uploads', fullFilename);

        return storeFS({ stream, fullpath });
    };

    const resolvers = {
        Upload: GraphQLUpload,
        Query: {
            allProjects: async (root, {username}) => {
                const user = await Users.findOne({ username });

                console.log('user', user)

                if (!user) {
                    return [];
                }

                const out = await Projects
                    .find({
                        user: user._id,
                        type:'project'
                    });

                    console.log('project', out);

                const res = await Promise.all(out.map((project) => {
                    return Likes.find({
                        item: project._id,
                        col: 'Projects'
                    }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt').then(likes => {
                        project.likes = likes;
                        return Shares.find({
                            item: project._id,
                            col: 'Projects'
                        })
                    }).then(shares => {
                        project.shares = shares;
                        return Comments.find({
                            item: project._id,
                            col: 'Projects'
                        }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')
                    }).then(comments => {
                        project.comments = comments;
                        if(!project.token) {
                            project.token = FeedManager.getFeed('timeline', project._id.toString()).getReadOnlyToken();
                            return Projects.update({
                                _id:project._id
                            },{
                                $set:{
                                    token:project.token
                                }
                            }).then(updated => {
                                return project;
                            })
                        }
                        return project;
                    })
                }))

                return res;
            },
            getRelatedProjects: async (root, {category, tags}) => {
                const byCategoryOrTags = await Projects
                    .find({
                        $and:[
                            {
                                $or: [
                                    { "category.name": category },
                                    { tags: { $in: tags } }
                                ]
                            },{
                                type:{$ne:'lookbook'}
                            }
                        ]
                });

                const res = await Promise.all(byCategoryOrTags.map((project) => {
                    return Likes.find({
                        item: project._id,
                        col: 'Projects'
                    }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt').then(likes => {
                        project.likes = likes;
                        return Shares.find({
                            item: project._id,
                            col: 'Projects'
                        })
                    }).then(shares => {
                        project.shares = shares;
                        return Comments.find({
                            item: project._id,
                            col: 'Projects'
                        }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')
                    }).then(comments => {
                        project.comments = comments;
                        if(!project.token) {
                            project.token = FeedManager.getFeed('timeline', project._id.toString()).getReadOnlyToken();
                            return Projects.update({
                                _id:project._id
                            },{
                                $set:{
                                    token:project.token
                                }
                            }).then(updated => {
                                return project;
                            })
                        }
                        return project;
                    })
                }))

                return res;
            },
            getProjectShares: async (root, {projectId}) => {
                const shares = await Shares
                    .find({ item: projectId })

                return shares;
            },
            projectComments: async (root, {projectId}) => {
                const project = await Projects.findOne({
                    _id: projectId
                })
                const comments = await Comments.find({
                    item: project._id
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return comments;

            },
            projectLikes: async (root, {projectId}) => {
                const likes = await Likes.find({
                    item: projectId
                }).sort({
                    createdAt:1
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')

                return likes;
            }
        },
        Mutation: {
            createProject: async (obj, {
                username,
                title,
                category,
                description,
                projectPrivacy,
                tags,
                images,
                license,
                type
            }) => {
                const user = await Users.findOne({ username });

                let projectType = 'lookbook';
                if(user.accountMeta.subscription=='PRO' && type=='project') {
                    projectType = 'project';
                }

                if(!images) {
                    images = [];
                }

                const project = new Projects({
                    user: user._id,

                    type: projectType,
                    category:  category,
                    title: title,
                    description: description,
                    tags: tags,
                    images: images.map((image) => ({
                        description: '',
                        path: image.path,
                        thumbnail: image.path
                    })),

                    coverImage: images.length ? images[0].path : '',

                    license: license,
                    editorChoice: false,
                    projectPrivacy:'PUBLIC',
                    setName:''
                })

                return await project.save();
            },
            addCoverImage: async (obj, {
                username,
                projectId,
                coverImage
            }) => {
                const user = await Users.findOne({ username });
                const project = await Projects.findOne({_id:projectId});

                project.coverImage = coverImage;
                return await project.save();

            },
            addProjectShare: async (obj, { username, projectId }) => {
                const user = await Users.findOne({ username });
                const project = await Projects.findOne({ _id:projectId }).lean();

                let query = {
                    register_id:project.register_id
                }

                if(project.user) {
                    query = {
                        _id:project.user
                    }
                }

                const userOwner = await Users.findOne(query).lean();

                const share = new Shares({
                    user: user._id,
                    owner: userOwner._id,
                    item: project._id,
                    col: 'Projects'
                })

                await share.save();

                const follow = await Follows.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })
                
                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: project._id,
                        col: 'Projects'
                    })
                    await newFollow.save()
                }

                return share;
            },
            addProjectComment: async (obj, { username, projectId, message }) => {
                const user = await Users.findOne({ username })
                const project = await Projects.findOne({ _id: projectId })

                const newComment = await new Comments({
                    user: user._id,
                    message: message,
                    item: project._id,
                    col: 'Projects'
                }).save()

                const followProject = await Follows.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })

                if(!followProject) {
                    const follow = new Follows({
                        user: user._id,
                        item: project._id,
                        col: 'Projects'
                    })
                    await follow.save()
                }

                return await Comments.find({
                    item: project._id,
                    col: 'Projects'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt')
            },
            addProjectLike: async (obj, { username, projectId }) => {
                const user = await Users.findOne({ username });
                const project = await Projects.findOne({_id:projectId});

                const currentLike = await Likes.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })

                if(currentLike) {
                    return [currentLike]; // needs to be array since gQL return type is Array<Like> 
                }

                const like = new Likes({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                });

                await like.save()

                const follow = await Follows.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })
                
                if(!follow) {
                    const newFollow = new Follows({
                        user: user._id,
                        item: project._id,
                        col: 'Projects'
                    })
                    await newFollow.save()
                }

                return await Likes.find({
                    item: project._id,
                    col: 'Projects'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            },
            removeProjectLike: async (obj, { username, projectId }) => {
                const user = await Users.findOne({ username });
                const project = await Projects.findOne({ _id:projectId });

                const like = await Likes.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })

                const follow = await Follows.findOne({
                    user: user._id,
                    item: project._id,
                    col: 'Projects'
                })

                if (follow) {
                    await follow.remove()
                }

                if (like) {
                    await like.remove()
                }

                return await Likes.find({
                    item: project._id,
                    col: 'Projects'
                }).populate('user', '_id id username fullname profileImages contact accountMeta about createdAt');
            }
        }
    };

    return {
        typeDef,
        resolvers,
        queries,
        mutations
    };
}
