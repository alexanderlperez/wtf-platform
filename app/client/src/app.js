import React from 'react';
import ReactDOM from 'react-dom';

import { ApolloProvider  } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createUploadLink } from 'apollo-upload-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
 
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { CookiesProvider, withCookies } from 'react-cookie';
import Media from 'react-media';

import { mobile } from './services/MediaQueries';

import HeaderNavigation from './components/HeaderNavigation';
import MobileToggle from './components/MobileToggle';

import HomePageLayout from './layouts/HomePageLayout';
import UserLayout from './layouts/UserLayout';
import UserEditLayout from './layouts/UserEditLayout';

import LoginView from './views/LoginView';
import SignupView from './views/SignupView';
import NotificationsView from './views/NotificationView';
import MessagesView from './views/MessagesView';

import './scss/app.scss';

const client = new ApolloClient({
    link: createUploadLink({ uri: `http://localhost:3000/graphql` }),
    cache: new InMemoryCache()
});

const HomeByAuthState = withCookies((props) => {
    const isLoggedIn = props.cookies.get('isLoggedIn');
    const username = props.cookies.get('username');

    return (
        <Route exact path="/" render={(props) => { 
            return (
                <div>
                    { isLoggedIn 
                        ? <Redirect to={`/account/${username}`} /> 
                        : <HomePageLayout /> 
                    }
                </div>
            )
        }} />
    );
});

const HeaderViewByAuthState = withCookies((props) => {
    const isLoggedIn = props.cookies.get('isLoggedIn') 

    return (
        <div>
            {(isLoggedIn || isLoggedIn == "true") && <HeaderNavigation />}
        </div>
    )
})

ReactDOM.render(( 
    <ApolloProvider client={client}>
        <CookiesProvider>
            <Router>
                <div className="site-content-wrapper">
                    <div id="outer-container">
                        <HeaderViewByAuthState />

                        <div id="page-wrap">
                            { /* TODO: use the SiteLinks service to set these paths */ }
                            <Switch>
                                <Route path="/login" component={LoginView}></Route>
                                <Route path="/register" component={SignupView}></Route>
                                <Route path="/account/:username/edit" component={UserEditLayout}></Route>
                                <Route path="/account/:username" component={UserLayout}></Route>
								<Route path="/notifications" component={NotificationsView}></Route>
								<Route path="/messages" component={MessagesView}></Route>
                                <HomeByAuthState />
                            </Switch>
                        </div>
                    </div>
                </div>            
            </Router>
        </CookiesProvider>
    </ApolloProvider>
), document.querySelector('#app'));
