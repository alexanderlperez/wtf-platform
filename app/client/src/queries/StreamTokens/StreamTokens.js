import gql from 'graphql-tag';

const StreamTokens = gql`
    query StreamTokens($username: String!) {
        streamTokens(username: $username) {
            api
            user
            notification
            timeline
            messages
            userId
            appId
        }
    }`;

export default StreamTokens;

