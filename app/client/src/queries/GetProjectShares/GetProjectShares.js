import gql from 'graphql-tag';

const GetProjectShares = gql`
    query GetProjectShares($projectId: String!) {
        getProjectShares(projectId: $projectId) {
            id
            user
            owner
            item
            col
            token
            createdAt
        }
    }
    `;

export default GetProjectShares;

