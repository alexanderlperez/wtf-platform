import gql from 'graphql-tag';

const AddComment = gql`
mutation addProjectComment(
    $username: String!
    $projectId: String!
    $message: String
) {
    addProjectComment(
        username: $username
        projectId: $projectId
        message: $message
    ) {
        id
        user {
            id
            username
            fullname
            profileImages {
                avatar
                timeline
            }
            contact {
                firstName
                lastName
                address
                city
                country {
                    name
                    short
                }
                email
                countryCode
                phone
                state {
                    name
                    short
                }
                zipcode
            }
            accountMeta {
                changedUsername
                registerFor
                registerType
                subscription
            }
            about {
                title
                gender
                description
                birthdate
                personalDetail 
                profession
            }
            createdAt
        }
        message
        item
        col
        createdAt
    }
}
`;

export default AddComment;

