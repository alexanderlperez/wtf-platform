import gql from 'graphql-tag';

const RemoveFollowConversations = gql`
    query RemoveFollowConversations($username: String!, $conversationId: String!) {
        removeFollowConversations(username: $username, conversationId: $conversationId) {
            id
            users {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            lastMessage {
                id
                user
                message
                item
                col
                createdAt
            }
            createdAt
        }
    }
    `;


export default RemoveFollowConversations;

