import gql from 'graphql-tag';

const Notifications = gql`
    query Notifications($username: String!) {
        notifications(username: $username) {
            activities {
                actor
                foreign_id
                id
                message
                object
                origin
                record {
                    user {
                        avatar
                        username
                        fullname
                        subscribed
                        profession {
                            name
                        }
                        contact {
                            firstName
                            lastName
                            email
                            websiteUrl
                            city
                            state
                            country
                            phone
                            address
                            zipcode
                        }
                        accountMeta {
                            registerType
                        }
                        about {
                            uploadResumePath
                            uploadBrochuresPath
                            gender
                        }
                        stats {
                            shares
                            uploads
                            following
                            followers
                            views
                            favorites
                        }
                    }
                    project {
                        id
                        projectId
                        createdOn
                        title
                        description
                        coverImage
                        tags
                        projectViewCounter
                        stats {
                            favorites {
                                date
                            }
                            likes {
                                date
                            }
                            shares {
                                date
                            }
                        }
                        images {
                            id
                            description
                            filename
                        }
                        comments {
                            text
                            registerId
                            date
                            user {
                                contact {
                                    firstName
                                    lastName
                                }
                            }
                        }
                        category {
                            categoryName
                        }
                    }
                    timeline {
                        id
                        user {
                            avatar
                            username
                            fullname
                            subscribed
                            profession {
                                name
                            }
                            contact {
                                firstName
                                lastName
                                email
                                websiteUrl
                                city
                                state
                                country
                                phone
                                address
                                zipcode
                            }
                            accountMeta {
                                registerType
                            }
                            about {
                                uploadResumePath
                                uploadBrochuresPath
                                gender
                            }
                            stats {
                                shares
                                uploads
                                following
                                followers
                                views
                                favorites
                            }
                        }
                        content
                        cover
                        likes {
                            user {
                                avatar
                                username
                                fullname
                                subscribed
                                profession {
                                    name
                                }
                                contact {
                                    firstName
                                    lastName
                                    email
                                    websiteUrl
                                    city
                                    state
                                    country
                                    phone
                                    address
                                    zipcode
                                }
                                accountMeta {
                                    registerType
                                }
                                about {
                                    uploadResumePath
                                    uploadBrochuresPath
                                    gender
                                }
                                stats {
                                    shares
                                    uploads
                                    following
                                    followers
                                    views
                                    favorites
                                }
                            }
                            createdAt
                        }
                        comments {
                            user {
                                avatar
                                username
                                fullname
                                subscribed
                                profession {
                                    name
                                }
                                contact {
                                    firstName
                                    lastName
                                    email
                                    websiteUrl
                                    city
                                    state
                                    country
                                    phone
                                    address
                                    zipcode
                                }
                                accountMeta {
                                    registerType
                                }
                                about {
                                    uploadResumePath
                                    uploadBrochuresPath
                                    gender
                                }
                                stats {
                                    shares
                                    uploads
                                    following
                                    followers
                                    views
                                    favorites
                                }
                            }
                            comment
                            createdAt
                        }
                        likesTotal
                        commentsTotal
                        createdAt
                    }
                    message
                }
                target
                time
                verb
            }
            activity_count
            actor_count
            created_at
            group
            id
            is_read
            is_seen
            updated_at
            verb
        }
    }`;


export default Notifications;

