import gql from 'graphql-tag';

const AddTimelineComment = gql`
mutation addTimelineComment($username: String!, $timelineId: String!, $message: String!) {
    addTimelineComment(username: $username, timelineId: $timelineId, message: $message) {
        id
        user {
            id
            username
            fullname
            profileImages {
                avatar
                timeline
            }
            contact {
                firstName
                lastName
                address
                city
                country {
                    name
                    short
                }
                email
                countryCode
                phone
                state {
                    name
                    short
                }
                zipcode
            }
            accountMeta {
                changedUsername
                registerFor
                registerType
                subscription
            }
            about {
                title
                gender
                description
                birthdate
                personalDetail 
                profession
            }
            createdAt
        }
        message
        item
        col
        createdAt
    }
  }
`;

export default AddTimelineComment;



  
