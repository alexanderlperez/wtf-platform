import gql from 'graphql-tag';

const AddProjectShare = gql`
    mutation AddProjectShare($username: String!, $projectId: String!) {
        addProjectShare(username: $username, projectId: $projectId) {
            id
            user
            owner
            item
            col
            token
            createdAt
        }
    }
    `;

export default AddProjectShare;

