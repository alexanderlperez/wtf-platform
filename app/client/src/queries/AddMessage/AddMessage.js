import gql from 'graphql-tag';

const AddMessages = gql`
    mutation AddMessages($username: String!, $conversationId: String!, $message: String!) {
        addMessage(username: $username, conversationId: $conversationId, message: $message) {
            id
            user {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            message
            item
            col
            createdAt
        }
    }
    `;


export default AddMessages;

