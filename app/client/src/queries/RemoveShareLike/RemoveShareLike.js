import gql from 'graphql-tag';

const RemoveShareLike = gql`
    mutation RemoveShareLike($username: String!, $shareId: String!) {
        removeShareLike(username: $username, shareId: $shareId) {
            id
            user {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            item
            col
            createdAt
        }
    }
    `;

export default RemoveShareLike;

