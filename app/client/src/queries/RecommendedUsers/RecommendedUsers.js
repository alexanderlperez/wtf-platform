import gql from 'graphql-tag';

const RecommendedUsers = gql` query RecommendedUsers($username: String!, $profession: String, $country: String, $state: String) {
        recommendedUsers(username: $username, profession: $profession, country: $country, state: $state) {
            id
            username
            contact {
                firstName
                lastName
                address
                city
                country {
                    name
                    short
                }
                email
                countryCode
                phone
                state {
                    name
                    short
                }
                zipcode
            }
            profileImages {
                avatar
                timeline
            }
            accountMeta {
                changedUsername
                registerFor
                registerType
                subscription
            }
            about {
                title
                gender
                description
                birthdate
                personalDetail
                profession
            }
            createdAt
            totalLengths
            projects {
                id
                user
                type
                category
                title
                description
                tags
                images {
                    description
                    path
                    thumbnail
                }
                coverImage
                license
                editorChoice
                projectPrivacy
                setName
                createdAt
                token
            }
        }
    }`;

export default RecommendedUsers;

