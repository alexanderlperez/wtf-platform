import gql from 'graphql-tag';

const CreateProject = gql`
mutation CreateProject(
    $username: String!
    $title: String!
    $category: String!
    $description: String
    $projectPrivacy: String
    $tags: [String]
    $images: Upload
    $license: String
    $type: String
) {
    createProject(
        username: $username
        title: $title
        category: $category
        description: $description
        projectPrivacy: $projectPrivacy
        tags: $tags
        images: $images
        license: $license
        type: $type
    ) {
        id
        user
        type
        category
        title
        description
        tags
        images {
            description
            path
            thumbnail
        }
        coverImage
        license
        editorChoice
        projectPrivacy
        setName
        createdAt
        token

        likes {
            id
            user {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            item
            col
            createdAt
        }
        shares {
            id
            user
            owner
            item
            col
            token
            createdAt
        }
        comments {
            id
            user {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            message
            item
            col
            createdAt
        }
    }
}
`;


export default CreateProject;
