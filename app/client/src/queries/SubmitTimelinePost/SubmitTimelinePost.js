import gql from 'graphql-tag';

const SubmitTimelinePost = gql`
        mutation addTimeline($username: String!, $content: String!, $cover: Upload) {
            addTimeline(username: $username, content: $content, cover: $cover) {
                id
                user {
                    id
                    username
                    fullname
                    profileImages {
                        avatar
                        timeline
                    }
                    contact {
                        firstName
                        lastName
                        address
                        city
                        country {
                            name
                            short
                        }
                        email
                        countryCode
                        phone
                        state {
                            name
                            short
                        }
                        zipcode
                    }
                    accountMeta {
                        changedUsername
                        registerFor
                        registerType
                        subscription
                    }
                    about {
                        title
                        gender
                        description
                        birthdate
                        personalDetail 
                        profession
                    }
                    createdAt
                }
                content
                cover
                createdAt
            }
        }
    `;

export default SubmitTimelinePost;

