import gql from 'graphql-tag';

const UnfollowUser = gql`
    mutation unfollowUser($username: String!, $targetname: String!) {
        unfollowUser(username: $username, targetname: $targetname) {
            user
            target
        }
    }
    `;

export default UnfollowUser;

