import gql from 'graphql-tag';

const AllProjects = gql`
    query UserProjects($username: String!) {
        allProjects(username: $username) {
            id
            user
            type
            category
            title
            description
            tags
            images {
                description
                path
                thumbnail
            }
            coverImage
            license
            editorChoice
            projectPrivacy
            setName
            createdAt
            token
    
            likes {
                id
                user {
                    id
                    username
                    fullname
                    profileImages {
                        avatar
                        timeline
                    }
                    contact {
                        firstName
                        lastName
                        address
                        city
                        country {
                            name
                            short
                        }
                        email
                        countryCode
                        phone
                        state {
                            name
                            short
                        }
                        zipcode
                    }
                    accountMeta {
                        changedUsername
                        registerFor
                        registerType
                        subscription
                    }
                    about {
                        title
                        gender
                        description
                        birthdate
                        personalDetail 
                        profession
                    }
                    createdAt
                }
                item
                col
                createdAt
            }
            shares {
                id
                user
                owner
                item
                col
                token
                createdAt
            }
            comments {
                id
                user {
                    id
                    username
                    fullname
                    profileImages {
                        avatar
                        timeline
                    }
                    contact {
                        firstName
                        lastName
                        address
                        city
                        country {
                            name
                            short
                        }
                        email
                        countryCode
                        phone
                        state {
                            name
                            short
                        }
                        zipcode
                    }
                    accountMeta {
                        changedUsername
                        registerFor
                        registerType
                        subscription
                    }
                    about {
                        title
                        gender
                        description
                        birthdate
                        personalDetail 
                        profession
                    }
                    createdAt
                }
                message
                item
                col
                createdAt
            }
        }
    } `;

export default AllProjects;
