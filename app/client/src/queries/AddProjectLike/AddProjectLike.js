import gql from 'graphql-tag';

const AddProjectLike = gql`
    mutation AddProjectLike($username: String!, $projectId: String!) {
        addProjectLike(username: $username, projectId: $projectId) {
            id
            user {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            item
            col
            createdAt
        }
    }
    `;

export default AddProjectLike;

