import gql from 'graphql-tag';

const CreateConversation = gql`
    mutation CreateConversation($username: String!, $targets: [String]) {
        createConversation(username: $username, targets: $targets) {
            id
            users {
                id
                username
                fullname
                profileImages {
                    avatar
                    timeline
                }
                contact {
                    firstName
                    lastName
                    address
                    city
                    country {
                        name
                        short
                    }
                    email
                    countryCode
                    phone
                    state {
                        name
                        short
                    }
                    zipcode
                }
                accountMeta {
                    changedUsername
                    registerFor
                    registerType
                    subscription
                }
                about {
                    title
                    gender
                    description
                    birthdate
                    personalDetail 
                    profession
                }
                createdAt
            }
            lastMessage {
                id
                user
                message
                item
                col
                createdAt
            }
            createdAt
        }
    }
    `;


export default CreateConversation;

