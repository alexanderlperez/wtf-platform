import gql from 'graphql-tag';

const GetConnections = gql`
    query GetConnections($username: String!, $search: String) {
        getConnections(username: $username, search: $search) {
            id
            username
            fullname
            profileImages {
                avatar
                timeline
            }
            contact {
                firstName
                lastName
                address
                city
                country {
                    name
                    short
                }
                email
                countryCode
                phone
                state {
                    name
                    short
                }
                zipcode
            }
            accountMeta {
                changedUsername
                registerFor
                registerType
                subscription
            }
            about {
                title
                gender
                description
                birthdate
                personalDetail 
                profession
            }
            createdAt
        }
    }
    `;

export default GetConnections;

