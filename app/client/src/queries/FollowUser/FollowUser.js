import gql from 'graphql-tag';

const FollowUser = gql`
    mutation followUser($username: String!, $targetname: String!) {
        followUser(username: $username, targetname: $targetname) {
            user
            target
        }
    }
    `;

export default FollowUser;

