import gql from 'graphql-tag';

const AddTimelineShare = gql`
    mutation AddTimelineShare($username: String!, $timelineId: String!) {
        addTimelineShare(username: $username, timelineId: $timelineId) {
            id
            user
            owner
            item
            col
            token
            createdAt
        }
    }
    `;

export default AddTimelineShare;

