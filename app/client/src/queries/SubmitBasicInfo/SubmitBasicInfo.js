import gql from 'graphql-tag';

const SubmitBasicInfo = gql`
mutation SubmitBasicInfo(
        $username: String!,
        $set: JSON!
) {
    submitBasicInfo(
        username: $username,
        set: $set
    ) {
        id
        username
        fullname
        profileImages {
            avatar
            timeline
        }
        contact {
            firstName
            lastName
            address
            city
            country {
                name
                short
            }
            email
            countryCode
            phone
            state {
                name
                short
            }
            zipcode
        }
        accountMeta {
            changedUsername
            registerFor
            registerType
            subscription
        }
        about {
            title
            gender
            description
            birthdate
            personalDetail
            profession
        }
        resume {
            resumePath
            brochuresPath
        }
        awards {
            organization
            url
            title
            year
            month
        }
        education {
            name
            website
            degree
            city
            state {
                name
                short
            }
            country {
                name
                short
            }
            entryDate {
                year
                month
            }
            duration {
                years
                months
            }
            graduated
        }
        languages {
            language
            proficiency
        }
        workExperience {
            companyName
            companyWebsite
            city
            state {
                name
                short
            }
            country {
                name
                short
            }
            title
            details
            starYear
            startMonth
            endYear
            endMonth
        }
        skills
        socialLinks {
            link
            type
        }
        verifications {
            token
            expiresAt
            createdAt
            verified
            type
        }

        createdAt
    }
}
`;

export default SubmitBasicInfo;