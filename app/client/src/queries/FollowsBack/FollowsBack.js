import gql from 'graphql-tag';

const FollowsBack = gql`
    query FollowsBack($username: String!, $targetname: String!) {
        followsBack(username: $username, targetname: $targetname)
    }
    `;

export default FollowsBack;

