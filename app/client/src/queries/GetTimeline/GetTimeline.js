import gql from 'graphql-tag';

const GetTimeline = gql`
    query Timeline ($username: String!) {
        timeline(username: $username) {
            actor
            content
            cover
            foreign_id
            id
            object
            origin
            target
            time
            verb
            likes {
                id
                user {
                    id
                    username
                    fullname
                    profileImages {
                        avatar
                        timeline
                    }
                    contact {
                        firstName
                        lastName
                        address
                        city
                        country {
                            name
                            short
                        }
                        email
                        countryCode
                        phone
                        state {
                            name
                            short
                        }
                        zipcode
                    }
                    accountMeta {
                        changedUsername
                        registerFor
                        registerType
                        subscription
                    }
                    about {
                        title
                        gender
                        description
                        birthdate
                        personalDetail 
                        profession
                    }
                    createdAt
                }
                item
                col
                createdAt
            }
            comments {
                id
                user {
                    id
                    username
                    fullname
                    profileImages {
                        avatar
                        timeline
                    }
                    contact {
                        firstName
                        lastName
                        address
                        city
                        country {
                            name
                            short
                        }
                        email
                        countryCode
                        phone
                        state {
                            name
                            short
                        }
                        zipcode
                    }
                    accountMeta {
                        changedUsername
                        registerFor
                        registerType
                        subscription
                    }
                    about {
                        title
                        gender
                        description
                        birthdate
                        personalDetail 
                        profession
                    }
                    createdAt
                }
                message
                item
                col
                createdAt
            }
        }
    }
    `;


export default GetTimeline;

