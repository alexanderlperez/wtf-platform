import React from 'react';
import { withCookies } from 'react-cookie';
import { Redirect } from 'react-router';
import { Button, Form, FormGroup, Label, Input, FormText  } from 'reactstrap';
import { graphql, compose  } from 'react-apollo';
import gql from 'graphql-tag';
import Dropzone from 'react-dropzone';
import TagsInput from 'react-tagsinput';

import { uploadCategories, supAsterisk } from '../../services/Constants';

import User from '../../queries/User';
import CreateProject from '../../queries/CreateProject';
import AddCoverImage from '../../queries/AddCoverImage';

import Uploader from '../../components/Uploader';
import LicenseDrop from '../../components/LicenseDrop';

import WrapWithModal from '../../components/WrapWithModal';
import Cropper from '../../components/Cropper';
import { X_OK } from 'constants';

const DO_PREVIEW = true;

class LookbookUploadsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId:'',
            category: "-1",
            name:'',
            title:'',
            description:'',
            tags:[],
            projectMetadata: '',
            images: [],
            coverImage: '',
            license:'',
            saveFullDetails:'',
            editorChoice:'',

            url: this.props.match.url,
            redirect: false,
            openCrop: false,
            uploadProgressIds: []
        };

        this.uppy = null;

        this.onDrop = this.onDrop.bind(this);
        this.onRemove = this.onRemove.bind(this);
        this.submitProject = this.submitProject.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onCropImageSave = this.onCropImageSave.bind(this);
        this.onUppyLoad = this.onUppyLoad.bind(this);
        this.uploadStart = this.uploadStart.bind(this);
        this.uploadSuccess = this.uploadSuccess.bind(this);
        this.uploadFails = this.uploadFails.bind(this);
        this.handleTagsChange = this.handleTagsChange.bind(this);
        this.onLicenseChange = this.onLicenseChange.bind(this);
    }

    onUppyLoad(uppy) {
        this.uppy = uppy;
        this.goBack = this.goBack.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;


        this.setState({
            [name]: value
        });
    }

    handleTagsChange(tags) {
        this.setState({tags})
    }

    onLicenseChange(value) {
        console.log('license value', value);
        this.setState({
            license: value.value
        })
    }

    onDrop(results) {
        event.preventDefault();
        let images = results.map((res) => ({
            id: res.id,
            filename: res.data.name,
            path: res.response.uploadURL,
            thumbnail: res.response.uploadURL
        }))
        this.setState({
            images: images,
            uploadProgressIds: []
        })
    }

    onRemove(file) {
        let images = this.state.images.filter((image) => {
            return image.id !== file.id
        })
        this.setState({
            images: images
        })
    }

    uploadStart(data) {
        this.setState({
            uploadProgressIds: this.state.uploadProgressIds.concat(data.fileIDs)
        })
    }

    uploadSuccess(file) {
        let index = this.state.uploadProgressIds.indexOf(file.id);
        if(index > -1) {
            this.setState({
                uploadProgressIds: this.state.uploadProgressIds.splice(index, 1)
            })
        }
    }

    uploadFails(failures) {
        failures.forEach(fail => {
            let index = this.state.uploadProgressIds.indexOf(fail.id);
            if(index > -1) {
                this.setState({
                    uploadProgressIds: this.state.uploadProgressIds.splice(index, 1)
                })
            }
        })
    }

    async submitProject(e, doPreview, history) {
        console.log('submit project', e, this.state, this.props);
        e.preventDefault();
        const { mutate } = this.props;

        let uppyResults = await this.uppy.upload();
        console.log('after upload', uppyResults);

        let images = uppyResults.successful.map((res) => ({
            id: res.id,
            filename: res.name,
            path: res.uploadURL,
            thumbnail: res.uploadURL
        }))

        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.props.cookies.get('username'),
                    title: this.state.title,
                    category: this.state.category,
                    description: this.state.description,
                    projectPrivacy: 'NA',
                    tags: this.state.tags,
                    images: images,
                    license: this.state.license,
                    type: 'project'
                }
            })
            this.setState({
                redirect: true
            })
        } catch (err) {
            console.log(err);
            this.setState({ 
                alerts: { 
                    success: false, 
                    failure: true 
                }
            })

            return;
        }
    }

    async onCropImageSave(data) {
        const mutate = this.props.coverImageMutate;
        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.props.cookies.get('username'),
                    projectId: this.state.projectId,
                    imageUrl: data.imageUrl,
                    width: data.width,
                    height: data.height,
                    x: data.x,
                    y: data.y,
                    rotate: data.rotate
                }
            })
            this.setState({
                openCrop: false,
                redirect: true
            })
        } catch (err) {
            console.log(err);
            this.setState({ 
                alerts: { 
                    success: false, 
                    failure: true 
                }
            })

            return;
        }
        // {
        //     imageUrl:
        //     width:
        //     height:
        //     x:
        //     y:
        //     rotate:
        // }
    }

    goBack() {
        this.setState({
            redirect: true
        })
    }

    render() {
        const categoryOptions = uploadCategories.map((cat, i) => <option key={i} value={cat}>{cat}</option>)

        const { url } = this.state;

        const imageItems = this.state.images.map((image, i) => (
            <div key={i} style={{display:'inline-block'}}>
                <img src={image.path}  width="200" />
            </div>
        ));

        if(this.state.redirect) {
            return <Redirect push to={`/account/${this.props.cookies.get('username')}/lookbook`} />;
        }

        let cropper = <Cropper images={this.state.images} onSave={this.onCropImageSave} />

        const isSubmitDisabled = () => {
            const isValid = !!this.state.title.length && this.state.category != "-1";

            if (!isValid) {
                return true;
            }

            return false;
        }

        return (
            <div className="ProjectUploadsView">
                <Form onSubmit={this.submitProject}>

                    <div className="upload-wrapper">
                        <Uploader
                            maxFileLimit={1}
                            note={'1 Photo, 5MB per photo max'}
                            handleUploadStart={this.uploadStart}
                            handleUploadSuccess={this.uploadSuccess}
                            handleUploadComplete={this.onDrop}
                            handleUploadFails={this.uploadFails}
                            handleRemoval={this.onRemove} 
                            onLoad={this.onUppyLoad} />
                    </div>

                    <FormGroup>
                        <Label>Name{supAsterisk}</Label>
                        <Input placeholder="Name" name="title" value={this.state.title} onChange={this.handleInputChange}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label>Description</Label>
                        <Input rows={10} type="textarea" placeholder="Description" name="description" value={this.state.description} onChange={this.handleInputChange}></Input>
                    </FormGroup>

                    <div className="row">
                        <div className="col-md-6">
                            <FormGroup>
                                <Label>Category{supAsterisk}</Label>
                                <Input className="dropdown-border" type="select" name="category" value={this.state.category} onChange={this.handleInputChange}>
                                    <option value={-1}>Select a category</option>
                                    {categoryOptions}
                                </Input>
                            </FormGroup>
                        </div>

                        <div className="col-md-6">
                            <FormGroup>
                                <Label>Tags</Label>
                                <TagsInput value={this.state.tags} onChange={this.handleTagsChange} />
                            </FormGroup>
                        </div>
                    </div>

                    <div className="controls-wrapper">
                        <div>
                            {
                                /*
                                *<Button onClick={() => this.submitProject(null, DO_PREVIEW)} color="outline-danger">Preview</Button>
                                */
                                }
                            <Button color="outline-danger" onClick={this.goBack}>Cancel</Button>
                            <Button color="outline-danger">Preview</Button>
                            <Button type="submit" color="danger" disabled={isSubmitDisabled()}>Publish Project &gt;</Button>
                        </div>
                    </div>
                </Form>

                <WrapWithModal isOpen={this.state.openCrop} toggle={() => {}} children={cropper} />
            </div>
        );
    }
}


export default compose(
    withCookies,
    graphql(CreateProject),
    graphql(AddCoverImage, { name:'coverImageMutate' })
)(LookbookUploadsView);
