import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert, Table } from 'reactstrap';


class AwardsView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          alerts: { success: false, failure: false },
          awardOrganization: '',
          awardName: '',
          website: '',
          startDate: '',
          awards: [],
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.submitForm = this.submitForm.bind(this);
      this.addAward = this.addAward.bind(this);
      this.renderAwards = this.renderAwards.bind(this);
      this.removeAward = this.removeAward.bind(this);
      this.editAward = this.editAward.bind(this);
  }

  addAward() {
    const { awards, awardOrganization, awardName, website, startDate } = this.state;
    const newAward = {
      awardOrganization,
      awardName,
      website,
      startDate
    }

    this.setState({
      awardOrganization: '',
      awardName: '',
      website: '',
      startDate: '',
      awards: [...awards, newAward],
    })
  }

  editAward(event) {
    const target = event.target.parentNode.parentNode;
    const award_id = target.dataset.id;
    const { awards } = this.state;
    const awardToEdit = awards.reduce((editObj, award) => {
      if (award.awardName === award_id) {
        editObj = Object.assign(award)
      }
      return editObj
    }, {})

    this.setState({
      awardOrganization: awardToEdit.awardOrganization,
      awardName: awardToEdit.awardName,
      startDate: awardToEdit.startDate,
      website: awardToEdit.website,
      awards: [...this.state.awards.filter((award) => award.awardName !== award_id)],
    })
  }

  removeAward(event) {
    const target = event.target.parentNode.parentNode;
    const award_id = target.dataset.id;
    const { awards } = this.state;

    this.setState({
      awards: [...awards.filter((award) => award.awardName !== award_id)]
    })
  }

  renderAwards() {
    const { awards } = this.state;

    if (!awards.length) {
      return (
        <tr>
          <td>Fashion Award</td>
          <td>RedDot</td>
          <td>https://reddot.com</td>
          <td>2016</td>
          <td className="actions"><div className="fas fa-edit"></div> <div className="fas fa-trash-alt"></div></td>
        </tr>
      )
    }

    return awards.map((award, i) => {
      let { awardOrganization, awardName, website, startDate } = award;

      return (
        <tr key={awardName} data-id={awardName}>
          <td>{awardName}</td>
          <td>{awardOrganization}</td>
          <td>{website}</td>
          <td>{startDate}</td>
          <td className="actions">
            <div className="fas fa-edit" onClick={(e) => this.editAward(e)}></div>
            <div className="fas fa-trash-alt" onClick={(e) => this.removeAward(e)}></div>
          </td>
        </tr>
      )
    })
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  submitForm(e) {
    e.preventDefault();

    this.setState({
        alerts: {
            success: false,
            failure: false
        }
    }, () => console.log(this.state));
  }

  render(){
    return (
      <div className="AwardsView UserEditView">
          <div className="heading-wrapper">
              <h1 className="heading">Add/Edit Awards Details</h1>
              <hr />
          </div>

        <Form className="container" onSubmit={this.submitForm}>
          <FormGroup className="row">
            <div className="col-sm-6">
              <FormGroup>
                <Label for="awardOrganization">Award Organization</Label>
                <Input type="text" name="awardOrganization" placeholder="enter" value={this.state.awardOrganization} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="awardName">Award Name</Label>
                <Input type="text" name="awardName" placeholder="enter" value={this.state.awardName} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
            <div className="col-sm-6">
              <FormGroup>
                <Label for="website">Website</Label>
                <Input type="url" name="website" placeholder="https://" value={this.state.website} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="startDate">Year</Label>
                <Input type="date" name="startDate" placeholder="date placeholder" value={this.state.startDate} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
          </FormGroup>
        </Form>

        <FormGroup className="submission-wrapper">
          <div className="alert-wrapper">
            <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
            <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
          </div>
          <Button type="submit" className="add-experience-button" onClick={() => this.addAward()}>Add Award</Button>
        </FormGroup>


        <Table bordered className="work-table">
          <thead>
            <tr>
              <th>Award Name</th>
              <th>Organization Name</th>
              <th>Website</th>
              <th>Year</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.renderAwards()}
          </tbody>
        </Table>

        <FormGroup className="submission-wrapper">
            <div className="alert-wrapper">
                <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
            </div>
            <Button type="submit" >Save</Button>
        </FormGroup>
      </div>
    );
  }
}

export default AwardsView;
