import React from 'react';
import { Link } from 'react-router-dom';
import { Alert, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import classNames from 'classnames';
import formEncoded from 'form-urlencoded';

import SiteLinks from '../../services/SiteLinks';
import Globals from '../../services/Globals';

import LogoFullBlack from '../../assets/images/wtf-logo-full-black.png';
import twitter from '../../assets/images/twitter.png';
import facebook from '../../assets/images/facebook.png';
import google from '../../assets/images/google.png';


class LoginView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isLoginError: false,
            isNetworkError: false,
            isServerError: false, 
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submitForm(e) {
        e.preventDefault();

        const { username } = this.state;

        const options = {
            method: 'POST',
            body: formEncoded(this.state),
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            credentials: "same-origin"
        }

        fetch('/login', options)
            .catch((err) => {
                this.setState({ isNetworkError: true })
                return Promise.reject(); 
            })
            .then(res => {
                // reset the messages
                this.setState({
                    isLoginError: false,
                    isNetworkError: false,
                    isServerError: false, 
                })

                if (res.status === 500) {
                    this.setState({
                        isServerError: true
                    })
                    return Promise.reject(); 
                }

                if (res.status !== 200) {
                    this.setState({
                        isLoginError: true
                    })
                    return Promise.reject();
                }
            })
            .then(res => {
                // things went fine
                Globals.set('username', username);

                setTimeout(() => {
                    window.location.href = `${SiteLinks.account}/${username}`
                }, 1000)
            })
    }

    render() {
        return (
            <div className="LoginView">
                <div className="left">
                    <div className="content-wrapper">
                        <div className="form-wrapper">
                            <div className="title">
                                <div className="logo">
                                    <img src={LogoFullBlack} alt="" />
                                </div>
                                <div className="tagline">World’s Largest Fashion Network </div>
                            </div>
                        
                            <Alert color="danger" isOpen={ this.state.isLoginError }>The username or password is incorrect</Alert>
                            <Alert color="danger" isOpen={ this.state.isNetworkError }>There was an error with the network.  Please try again later.</Alert>
                            <Alert color="danger" isOpen={ this.state.isServerError }>There was an error with the server.  Please try again later.</Alert>

                            <Form className="login-form" onSubmit={this.submitForm}>
                                <FormGroup>
                                    <Input type="text" value={this.state.username} onChange={this.handleInputChange} name="username" placeholder="Email/Username" />
                                </FormGroup>
                                <FormGroup>
                                    <Input value={this.state.password} onChange={this.handleInputChange} type="password" name="password" placeholder="Password"/>
                                </FormGroup>
                                <FormGroup>
                                    <Link className="forgot" to="/resetpassword">Forgot password?</Link>
                                    <Input className="submit" type="submit" value="Log In"/>
                                </FormGroup>
                            </Form>
                        </div>
                        
                        <div className="alternate-options">
                            <div className="divider"><span className="line"></span><span className="text">or log in with</span><span className="line"></span></div>
                            <div className="social-options">
                                <img src={twitter} alt="" />
                                <img src={facebook} alt="" />
                                <img src={google} alt="" />
                            </div>
                            <div className="register">
                                <Link to="/register">I don't have an account</Link>
                            </div>
                        </div>
                    </div>

                    <div className="copyright">
                        &copy; WTF 2016, All Rights Reserved
                    </div>
                </div>

                <div className="right"></div>
            </div>        
        );
    }
}

export default LoginView;
