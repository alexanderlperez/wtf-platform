import React from 'react';
import { withCookies } from 'react-cookie';
import { graphql, compose, withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import { Media, Button } from 'reactstrap';
import * as stream from 'getstream';

import DetailsHeader from '../../components/DetailsHeader';
import DetailsImages from '../../components/DetailsImages';
import DetailsActions from '../../components/DetailsActions';
import DetailsMeta from '../../components/DetailsMeta';
import ProjectsSlider from '../../components/ProjectsSlider';
import CommentsView from '../../components/CommentsView';
import RelatedProjects from '../../components/RelatedProjects';

import SiteLinks from '../../services/SiteLinks';

import AllProjects from '../../queries/AllProjects';
import GetRelatedProjects from '../../queries/GetRelatedProjects';
import GetProjectComments from '../../queries/GetProjectComments';
import StreamTokens from '../../queries/StreamTokens';
import GetTimelineLikes from '../../queries/GetTimelineLikes';
import GetProjectShares from '../../queries/GetProjectShares';

class ProjectDetailsView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            likes:[],
            comments:[],
            shares: 0,
            apiKey:'',
            appId:0,
            hasLiked: false
        }

        this.client = null;

        this.AllProjectsSlider = graphql(AllProjects, {options: {variables: {username: this.props.user.username}}})(ProjectsSlider)
        this.SampleRelatedProjects = graphql(
            GetRelatedProjects, 
            {options: {variables: { 
                category: this.props.project.category, 
                tags: this.props.project.tags 
        }}})(RelatedProjects);
    }

    componentWillReceiveProps(newProps) {
        if(!newProps.loadingTokens && !newProps.errorTokens && !this.client) {
            // Use these methods to listen for notification updates
            // Use the counter to display on notification icon
            // There is a separate endpoint in NotificationsView for retrieving list of notifications
            // This endpoint will trigger a mark_seen, mark_read event
            this.setState({
                apiKey: newProps.tokens.api,
                appId: newProps.tokens.appId
            })
            this.client = stream.connect(
                newProps.tokens.api,
                null,
                newProps.tokens.appId
            );
            var feed = this.client.feed(
                "timeline",
                newProps.project.id,
                newProps.project.token
            );
            feed.subscribe((response) => {
                this.props.client.query({
                    query: GetProjectComments,
                    variables: {
                        projectId: newProps.project.id
                    },
                    fetchPolicy:'network-only'
                }).then(res => {
                    this.setState({
                        comments: res.data.projectComments
                    })
                });

                this.props.client.query({
                    query: GetTimelineLikes,
                    variables: {
                        timelineId: newProps.project.id
                    },
                    fetchPolicy:'network-only'
                }).then(res => {
                    this.setState({
                        likes: res.data.timelineLikes
                    })
                    let hasLiked = !!this.state.likes.filter((like) => {
                        return like.user.username == this.props.cookies.get('username')
                    }).length;
                    this.setState({
                        hasLiked
                    });
                });

            }).then((success) => {
                console.log('success connection on comment view', success)
            }, (error) => {
                console.log('error connection on comment view', error);
            })
            // on feed.get() opperation trigger {mark_seen:true} {mark_read:true} to clear log
        }

        if(!newProps.loadingShares && !newProps.errorShares) {
            this.setState({
                shares: newProps.shares
            })
        }

        if(!newProps.loadingComments && !newProps.errorComments) {
            this.setState({
                comments: newProps.comments
            })
        }

        if(!newProps.loadingLikes && !newProps.errorLikes) {
            this.setState({
                likes: newProps.likes
            })
            let username = newProps.cookies.get('username');
            let hasLiked = !!this.state.likes.filter((like) => {
                return like.user.username == username
            }).length;
            this.setState({
                hasLiked
            });
        }
    }

    render() {
        const { project, user, handler } = this.props;
        const { stats, description, title, images, category, id, tags } = project;
        const { username, profileImages, fullname, about, contact } = user;
        const { likes, comments, shares } = this.state;

        return (
            <div className="ProjectDetailsView">
                <DetailsHeader 
                    isFollowing={this.props.user.isFollowing}
                    username={username}
                    title={title}
                    profileImages={profileImages} 
                    fullname={fullname}
                    about={about}
                    contact={contact}
                    comments={comments.length}
                    shares={shares.length}
                    likes={likes.length} />
                <DetailsImages images={images} />
                <DetailsActions projectId={id} hasLiked={this.state.hasLiked} />
                <this.AllProjectsSlider handler={handler} user={user}/>
                <DetailsMeta 
                    description={description}
                    tags={tags}
                    category={category}/>
                <div className="heading underline">Comments</div> 
                <CommentsView project={project} comments={comments} />
                <this.SampleRelatedProjects handler={handler} username={user.username} />
            </div>
        );
    }
}

export default compose(
    withCookies,
    withApollo,
    graphql(GetProjectShares, { 
        props: ({ownProps, data: { getProjectShares, loading, error }}) => ({
            ...ownProps,
            shares: getProjectShares,
            loadingShares: loading,
            errorShares: error
        }),
        options: (props) => ({variables: { projectId:props.project.id }})
    }),
    graphql(GetTimelineLikes, { 
        props: ({ownProps, data: { timelineLikes, loading, error }}) => ({
            ...ownProps,
                likes: timelineLikes,
                loadingLikes: loading,
                errorLikes: error
        }),
        options: (props) => ({variables: { timelineId:props.project.id }})
    }),
    graphql(GetProjectComments, { 
        props: ({ownProps, data: { projectComments, loading, error }}) => ({
            ...ownProps,
                comments: projectComments,
                loadingComments: loading,
                errorComments: error
        }),
        options: (props) => ({variables: { projectId:props.project.id }})
    }),
    graphql(StreamTokens, { 
        props: ({ownProps, data: { streamTokens, loading, error }}) => ({
            ...ownProps,
                tokens: streamTokens,
                loadingTokens: loading,
                errorTokens: error
        }),
        options: (props) => ({variables: { username: props.cookies.get('username') }})
    })
)(ProjectDetailsView);
