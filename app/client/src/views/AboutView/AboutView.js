import React from 'react';
import {Button} from 'reactstrap';
import { withCookies } from 'react-cookie';
import UnauthenticatedModal from '../../components/UnauthenticatedModal';
import ExtendedBio from './ExtendedBio';

class AboutView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: this.props.cookies.get('isLoggedIn')
        };
    }

    render() {
        const fullBioOnAuth = this.state.isLoggedIn && (<ExtendedBio />);
        const unauthModal = !this.state.isLoggedIn && (<UnauthenticatedModal />)

        return (
            <div className="AboutView">
                {unauthModal}

                <Button className="resume-download">Download Resume</Button>
                <div className="content-wrapper">
                    <table className="entry-group border">
                        <tbody>
                            <tr>
                                <td className="left">Full Name</td>
                                <td className="right">
                                    Adipisicing adipisci
                                </td>
                            </tr>
                            <tr>
                                <td className="left">Title</td>
                                <td className="right">Amet</td>
                            </tr>
                            <tr>
                                <td className="left">Location</td>
                                <td className="right">US, California</td>
                            </tr>
                            <tr>
                                <td className="left">Gender</td>
                                <td className="right">Male</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <hr />

                {fullBioOnAuth}
        </div>
        );
    }
}

export default withCookies(AboutView);
