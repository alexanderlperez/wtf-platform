import React from 'react';

const ExtendedBio = () => {
    return (
        <div>
            <div className="content-wrapper">
                <table className="entry-group border">
                    <tbody>
                        <tr>
                            <td className="left top">Biography</td>
                            <td className="right">
                                <p>Elit molestiae aliquam expedita quam dicta? Ratione explicabo incidunt impedit numquam minus rem consequatur? Ullam minus impedit necessitatibus doloribus rerum Recusandae assumenda deserunt ea necessitatibus.</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr />

            <div className="content-wrapper">
                <table className="entry-group border">
                    <tbody>
                        <tr>
                            <td className="left top">Experience</td>
                            <td className="right">
                                <p>Elit delectus</p>
                                <p><span className="highlight">Amet necessitatibus eos</span></p>
                                <p>Dolor necessitatibus magnam</p>
                            </td>
                        </tr>
                        <tr>
                            <td className="left top">Education</td>
                            <td className="right">
                                <p>Elit delectus</p>
                                <p><span className="highlight">Amet necessitatibus eos</span></p>
                                <p>Dolor necessitatibus magnam</p>
                            </td>
                        </tr>
                        <tr>
                            <td className="left top"></td>
                            <td className="right">
                                <p>Elit delectus</p>
                                <p><span className="highlight">Amet necessitatibus eos</span></p>
                                <p>Dolor necessitatibus magnam</p>
                            </td>
                        </tr>
                        <tr>
                            <td className="left top"></td>
                            <td className="right">
                                <p>Elit delectus</p>
                                <p><span className="highlight">Amet necessitatibus eos</span></p>
                                <p>Dolor necessitatibus magnam</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr />

            <div className="content-wrapper">
                <table className="entry-group">
                    <tbody>
                        <tr>
                            <td className="left top">Social</td>
                            <td className="right">
                                <i className="social fab fa-facebook"></i>
                                <i className="social fab fa-twitter"></i>
                                <i className="social fab fa-google-plus-square"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>    
    ) 
};

export default ExtendedBio;
