import React from 'react';
import { withCookies } from 'react-cookie';
import { graphql, compose, withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import { Media, Button } from 'reactstrap';
import * as stream from 'getstream';

import DetailsHeader from '../../components/DetailsHeader';
import DetailsImages from '../../components/DetailsImages';
import DetailsActions from '../../components/DetailsActions';
import LookbookDetailsMeta from '../../components/LookbookDetailsMeta';
import CommentsView from '../../components/CommentsView';
import RelatedLookbooks from '../../components/RelatedLookbooks';
import LookbookCommentsSwitcher from '../../components/LookbookCommentsSwitcher';

import { toUploadUrl } from '../../services/SiteLinks';
import { showChangedProps } from '../../services/Debugging';

import GetRelatedLookbooks from '../../queries/GetRelatedLookbooks';
import GetProjectComments from '../../queries/GetProjectComments';
import StreamTokens from '../../queries/StreamTokens';
import GetTimelineLikes from '../../queries/GetTimelineLikes';
import GetProjectShares from '../../queries/GetProjectShares';

class LookbookDetailsView extends React.Component {
    constructor(props) {
        super(props);

        console.log(props);

        this.state = {
            likes:[],
            comments:[],
            shares: 0,
            apiKey:'',
            appId:0,
            hasLiked: false
        }

        this.client = null;

        this.SampleRelatedLookbooks = graphql(
            GetRelatedLookbooks, 
            {options: {variables: { 
                category: this.props.project.category, 
                tags: this.props.project.tags 
        }}})(RelatedLookbooks);
    }

    componentWillReceiveProps(newProps) {
        // TODO: remove this when existing comments are debugged
        console.log('-------------------- LookbookDetailsView');
        showChangedProps(this.props, newProps);

        if(!newProps.loadingTokens && !newProps.errorTokens && !this.client) {
            // Use these methods to listen for notification updates
            // Use the counter to display on notification icon
            // There is a separate endpoint in NotificationsView for retrieving list of notifications
            // This endpoint will trigger a mark_seen, mark_read event
            this.setState({
                apiKey: newProps.tokens.api,
                appId: newProps.tokens.appId
            })

            this.client = stream.connect(
                newProps.tokens.api,
                null,
                newProps.tokens.appId
            );

            var feed = this.client.feed(
                "timeline",
                newProps.project.id,
                newProps.project.token
            );

            feed.subscribe((response) => {
                this.props.client.query({
                    query: GetProjectComments,
                    variables: {
                        projectId: newProps.project.id
                    },
                    fetchPolicy:'network-only'
                }).then(res => {
                    this.setState({
                        comments: res.data.projectComments
                    })
                });

                this.props.client.query({
                    query: GetTimelineLikes,
                    variables: {
                        timelineId: newProps.project.id
                    },
                    fetchPolicy:'network-only'
                }).then(res => {
                    this.setState({
                        likes: res.data.timelineLikes
                    })
                    let hasLiked = !!this.state.likes.filter((like) => {
                        return like.user.username == this.props.cookies.get('username')
                    }).length;
                    this.setState({
                        hasLiked
                    });
                });

            }).then((success) => {
                console.log('success connection on comment view', success)
            }, (error) => {
                console.log('error connection on comment view', error);
            })
            // on feed.get() opperation trigger {mark_seen:true} {mark_read:true} to clear log
        }

        if(!newProps.loadingShares && !newProps.errorShares) {
            this.setState({
                shares: newProps.shares
            })
        }

        if(!newProps.loadingComments && !newProps.errorComments) {
            // TODO: figure out why comments are being mangled here, after initial query
            this.setState({
                comments: newProps.comments
            })
        }

        if(!newProps.loadingLikes && !newProps.errorLikes) {
            this.setState({
                likes: newProps.likes
            })
            let username = newProps.cookies.get('username');
            let hasLiked = !!this.state.likes.filter((like) => {
                return like.user.username == username
            }).length;
            this.setState({
                hasLiked
            });
        }
    }

    render() {
        const { project, user, handler } = this.props;
        const { stats, description, title, images, category, id, tags } = project;
        const { username, profileImages, fullname, about, contact } = user;
        const { likes, comments, shares } = this.state;

        return (
            <div className="LookbookDetailsView">
                <div className="row">
                    <div className="col-md-8">
                        <DetailsHeader 
                            isFollowing={this.props.user.isFollowing}
                            username={username}
                            title={title}
                            profileImages={profileImages}
                            fullname={fullname}
                            about={about}
                            contact={contact}
                            comments={comments.length}
                            shares={shares.length}
                            likes={likes.length} />

                        <div className="LookbookImage">
                            <img src={toUploadUrl(project.coverImage)} width={'98%'} />
                        </div>

                        <DetailsActions projectId={id} hasLiked={this.state.hasLiked} />
                    </div>
                    <div className="col-md-4">
                        <LookbookDetailsMeta 
                            description={description}
                            tags={tags}
                            category={category}/>

                        <LookbookCommentsSwitcher project={project} comments={comments} />
                    </div>
                </div>

                <this.SampleRelatedLookbooks handler={handler} username={user.username} />
            </div>
        );
    }
}

export default compose(
    withCookies,
    withApollo,
    graphql(GetProjectShares, { 
        props: ({ownProps, data: { getProjectShares, loading, error }}) => ({
            ...ownProps,
            shares: getProjectShares,
            loadingShares: loading,
            errorShares: error
        }),
        options: (props) => ({variables: { projectId:props.project.id }})
    }),
    graphql(GetTimelineLikes, { 
        props: ({ownProps, data: { timelineLikes, loading, error }}) => ({
            ...ownProps,
                likes: timelineLikes,
                loadingLikes: loading,
                errorLikes: error
        }),
        options: (props) => ({variables: { timelineId:props.project.id }})
    }),
    graphql(GetProjectComments, { 
        props: ({ownProps, data: { projectComments, loading, error }}) => ({
            ...ownProps,
                comments: projectComments,
                loadingComments: loading,
                errorComments: error
        }),
        options: (props) => ({variables: { projectId:props.project.id }})
    }),
    graphql(StreamTokens, { 
        props: ({ownProps, data: { streamTokens, loading, error }}) => ({
            ...ownProps,
                tokens: streamTokens,
                loadingTokens: loading,
                errorTokens: error
        }),
        options: (props) => ({variables: { username: props.cookies.get('username') }})
    })
)(LookbookDetailsView);
