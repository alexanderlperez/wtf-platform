import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import SiteLinks from '../../services/SiteLinks';
import { withCookies } from 'react-cookie';
import { NavLink as Link } from 'react-router-dom';

import ProjectGrid from '../../components/ProjectGrid';
import WrapWithModal from '../../components/WrapWithModal';
import ProjectDetailsView from '../ProjectDetailsView';
import UnauthenticatedModal from '../../components/UnauthenticatedModal';

class PortfolioView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // this handles whether the modal will be opened with a portfolio project 
            isDetailsOpen: !!this.props.match.params.projectId,
            isLoggedIn: this.props.cookies.get('isLoggedIn'),
            username: this.props.cookies.get('username'),
        };

        this.toggleDetailsModal = this.toggleDetailsModal.bind(this);
        this.singleProject = this.singleProject.bind(this);
        this.setCurrentProject = this.setCurrentProject.bind(this);
    }

    singleProject(project, projectsList, projId) {
        if (project) {
            return project
        } else {
            return projectsList.find(proj => proj.projectId == parseInt(projId))
        }
    }

    toggleDetailsModal(curProj) {
        console.log('toggling details modal');
        if (curProj) {
            this.setState({
                isDetailsOpen: !this.state.isDetailsOpen,
                project: curProj
            })
        } else {
            this.setState({
                isDetailsOpen: !this.state.isDetailsOpen,
            })
        }
    }

    setCurrentProject(proj) {
        this.setState({
            project: proj
        })
    }

    render() {
        const { match: { params: { projectId } }, AllProjects, User } = this.props;

        if (AllProjects.loading || User.loading) {
            return <div></div>
        } else if (AllProjects.error || User.error) {
            return (
                <p>
                    {User.error}{AllProjects.error}
                </p>
            )
        }

        const { allProjects } = AllProjects;
        const { user } = User;

        // Get the project either from what's been passed to PortfolioView through: 
        // A. this.state.project <- toggleDetailsModal <- *ProjectEntry* <- ProjectGrid
        // B. the 'match' prop from React Router

        const currentProject = this.singleProject(this.state.project, allProjects, projectId);
        const FullView = (props) => {
            return (
                <div className="PortfolioView">
                    <Link to={`portfolio/upload`} className="upload-button btn btn-danger">Upload Project</Link>

                    <ProjectGrid projects={allProjects} handler={this.toggleDetailsModal} />

                    <WrapWithModal className="ProjectDetailsModal" toggle={this.toggleDetailsModal} isOpen={this.state.isDetailsOpen}>
                        <ProjectDetailsView handler={this.setCurrentProject} project={currentProject} user={user} />
                    </WrapWithModal>
                </div>
            );
        };

        const { isLoggedIn } = this.state;
        const unauthModal = !isLoggedIn && (<UnauthenticatedModal />)
        const fullViewOnAuthj = isLoggedIn && (<FullView />)

        return (
            <div>
                {unauthModal}
                {fullViewOnAuthj}
            </div>
        )
    }
}

export default withCookies(PortfolioView);
