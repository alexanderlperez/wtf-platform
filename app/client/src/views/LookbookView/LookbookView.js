import React from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import SiteLinks from '../../services/SiteLinks';
import { CSSGrid, layout, measureItems, makeResponsive } from 'react-stonecutter';
import { NavLink as Link } from 'react-router-dom';

import LookbookDetailsView from '../LookbookDetailsView';
import WrapWithModal from '../../components/WrapWithModal';
import LookbookEntry from '../../components/LookbookEntry';

const Grid = makeResponsive(measureItems(CSSGrid, { measureImages: true }), {
    maxWidth: 1170,
    minPadding: 0
});

class LookbookView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isDetailsOpen: false,
            lookbook: null
        };

        this.gridProps = {
            component: "ul",
            columns: 4,
            columnWidth: 270,
            gutterWidth: 30,
            gutterHeight: 30,
            layout: layout.pinterest,
            duration: 0,
        };

        this.openDetailsModal = this.openDetailsModal.bind(this);
        this.closeDetailsModal = this.closeDetailsModal.bind(this);
        this.toggleDetailsModal = this.toggleDetailsModal.bind(this);
    }

    openDetailsModal(image) {
        this.setState({
            isDetailsOpen: true,
            image
        })
    }

    closeDetailsModal() {
        this.setState({
            isDetailsOpen: false
        })
    }

    toggleDetailsModal(entry) {
        if (entry) {
            this.setState({
                isDetailsOpen: !this.state.isDetailsOpen,
                lookbook: entry
            })
        } else {
            this.setState({
                isDetailsOpen: !this.state.isDetailsOpen,
            })
        }
    }

    setCurrentLookbook(entry) {
        this.setState({
            lookbook: entry
        })
    }

    render() {
        const { User, Lookbook } = this.props;

        if (User.loading || Lookbook.loading) {
            return <h1>Loading</h1>
        } else if (User.error || Lookbook.error) {
            return <h1>{`${User.error} ${Lookbook.error}`}</h1>
        }

        const { user } = User;
        const {fullname, avatar, profession } = user;
        const lookbook = this.props.Lookbook.lookbook;

        // Each ProjectEntry's modal overlay will deal with toggling WrapWithModal
        const gridItems = lookbook.map((entry, i) => {
            return (
                <li key={i}>
                    <LookbookEntry 
                        clickHandler={() => this.toggleDetailsModal(entry)} 
                        likes={entry.likes.length} 
                        profession={user.about.profession || ''} 
                        fullname={user.fullname} 
                        avatar={user.profileImages.avatar} 
                        coverImage={entry.coverImage} />
                </li>
            )
        });

        return (
            <div className="LookbookView">
                <Link to={`lookbook/upload`} className="upload-button btn btn-danger">Upload Lookbook</Link>

                <Grid {...this.gridProps}>{gridItems}</Grid>

                <WrapWithModal className="LookbookViewModal" toggle={this.toggleDetailsModal} isOpen={this.state.isDetailsOpen}>
                    <LookbookDetailsView handler={this.setCurrentLookbook} project={this.state.lookbook} user={user} />
                </WrapWithModal>
            </div>
        )
    }
}

export default LookbookView;
