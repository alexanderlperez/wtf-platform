import React from 'react';
import { graphql, compose  } from 'react-apollo';
import gql from 'graphql-tag';
import Dropzone from 'react-dropzone';
import { Form, FormGroup, Label, Input, Button, Alert, Table } from 'reactstrap';

import User from '../../queries/User';

class ResumeUploadView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alerts: { success: false, failure: false },
            username: this.props.username,
            uploadResumePath: ''
        };

        this.handleUpload = this.handleUpload.bind(this)
    }
    async handleUpload([file]) {
        const { mutate } = this.props;

        let res;
        try {
            res = await mutate({
                variables: { username: this.state.username, file: file }
            })
        } catch (err) {
            this.setState({
                alerts: {
                    success: false,
                    failure: true
                }
            })

            return;
        }
        this.setState({
            alerts: {
                success: true,
                failure: false
            },
            uploadResumePath: res.data.uploadResume.about.uploadResumePath
        })
    }

    componentWillReceiveProps(newProps) {
        if (!newProps.loading) {
            this.setState({
                alerts: { success: false, failure: false },
                uploadResumePath: newProps.user.about.uploadResumePath
            });
        }
    }

    render() {
        const { loading, error } = this.props;

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
          <div className="ResumeUploadView UserEditView">
              <div className="heading-wrapper">
                  <h1 className="heading">Upload Resume</h1>
                  <hr />
              </div>
              <div className="dropzone">
                <Dropzone onDrop={this.handleUpload} style="none"/>
                  <div className="fas fa-cloud-upload-alt"></div>
                  <div className="upload-project-text">UPLOAD PROJECT</div>
                {this.state.uploadResumePath}
              </div>

                <Table bordered className="work-table">
                  <thead>
                    <tr>
                      <th className="main-header">Document</th>
                      <th className="second-header"></th>
                      <th className="action">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Resume</td>
                      <td>pdf</td>
                      <td className="actions"> <div className="fas fa-trash-alt"></div></td>
                    </tr>
                  </tbody>
                </Table>

                <FormGroup className="submission-wrapper">
                    <div className="alert-wrapper">
                        <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                        <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                    </div>
                    <Button type="submit">Save</Button>
                </FormGroup>
          </div>
        );
    }
}

const uploadResume = gql`
mutation uploadResume(
    $username: String!,
    $file: Upload!
) {
    uploadResume(
        username: $username,
        file: $file
    ) {
        avatar
        username
        fullname
        profession {
            name
        }
        contact {
            firstName
            lastName
            email
            websiteUrl
            city
            state
            country
            phone
            address
            zipcode
        }
        accountMeta {
            registerType
        }
        about {
            uploadResumePath
            gender
        }
        stats {
            shares
            uploads
            following
            followers
            views
            favorites
        }
    }
}
`;

export default compose(
    graphql(uploadResume),
    graphql(User, { props: ({ ownProps, data: {user, loading, error} }) => ({
        ...ownProps,
        loading,
        error,
        user
    })})
)(ResumeUploadView);
