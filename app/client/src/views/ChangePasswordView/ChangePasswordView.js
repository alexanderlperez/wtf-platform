import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert, Table } from 'reactstrap';


class ChangePasswordView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          alerts: { success: false, failure: false },
          password: '',
          newPassword: '',
          confirmNewPassword: '',
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.submitForm = this.submitForm.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  submitForm(e) {
    e.preventDefault();

    this.setState({
        alerts: {
            success: false,
            failure: false
        }
    }, () => console.log(this.state));
  }

  render(){
    return (
      <div className="ChangePasswordView UserEditView">
          <div className="heading-wrapper">
              <h1 className="heading">Change Password</h1>
              <hr />
          </div>

        <Form className="container" onSubmit={this.submitForm}>
          <FormGroup className="row">
            <div className="col-sm-4">
              <FormGroup className="form-field">
                <Label for="password">Password</Label>
                <Input type="password" name="password" placeholder="enter password" value={this.state.password} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
            <div className="col-sm-4">
              <FormGroup className="form-field">
                <Label for="newPassword">New Password</Label>
                <Input type="password" name="newPassword" placeholder="enter new password" value={this.state.newPassword} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
            <div className="col-sm-4">
              <FormGroup className="form-field">
                <Label for="confirmNewPassword">Confirm New Password</Label>
                <Input type="password" name="confirmNewPassword" placeholder="enter new password" value={this.state.confirmNewPassword} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
          </FormGroup>

          <FormGroup className="submission-wrapper">
            <div className="alert-wrapper">
              <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
              <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
            </div>
            <Button type="submit">Save</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default ChangePasswordView;
