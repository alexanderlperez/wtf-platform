import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert } from 'reactstrap';
import { professions } from '../../services/Constants';

class AboutMeView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          alerts: { success: false, failure: false },
          title: '',
          description: '',
          facebook: '',
          twitter: '',
          google: '',
          linkedin: '',
          about:{
            title: '',
            gender: '',
            description: '',
            birthdate: '',
            personalDetail: 0,
            profession: ''
          }
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.submitForm = this.submitForm.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  submitForm(e) {
    e.preventDefault();
    this.setState({
        alerts: {
            success: false,
            failure: false
        }
    }, () => console.log(this.state));
  }

  render() {
    return (

      <div className="AboutMeView UserEditView">
          <div className="heading-wrapper">
              <h1 className="heading">Edit about details</h1>
              <hr />
          </div>

        <Form className="container" onSubmit={this.submitForm}>
          <FormGroup className="row">
            <div className="col-sm-6">
              <FormGroup>
                <Label for="exampleEmail">Section Title</Label>
                <Input type="text" name="title" placeholder="title" value={this.state.title} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleText">Description</Label>
                <Input type="textarea" name="description" className="input text-area" placeholder="tell us about yourself" value={this.state.description} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
            <div className="col-sm-6">
              <FormGroup>
                  <Label>Profession</Label>
                  <Input type="select" name="profession" value={this.state.about.profession} onChange={this.handleAboutInputChange}>
                      { professions.map((pro, i) => (<option key={i} value={pro}>{pro}</option>)) }
                  </Input>
              </FormGroup>
              <FormGroup>
                  <Label>Gender</Label>
                  <Input type="select" name="gender" value={this.state.about.gender} onChange={this.handleAboutInputChange}>
                      <option value="">Select Gender</option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                  </Input>
              </FormGroup>
              <FormGroup>
                <Label for="birthdate">Date of Birth</Label>
                <Input type="date" name="birthdate" placeholder="date placeholder" value={this.state.about.birthdate} onChange={this.handleInputChange} />
              </FormGroup>
              {/*
              <FormGroup>
                <Label for="examplePassword">Facebook Link</Label>
                <Input type="url" name="facebook" placeholder="https://" value={this.state.facebook} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword">Twitter Link</Label>
                <Input type="url" name="twitter" placeholder="https://" value={this.state.twitter} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword">Google+ Link</Label>
                <Input type="url" name="google" placeholder="https://" value={this.state.google} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="examplePassword">LinkedIn Link</Label>
                <Input type="url" name="linkedin" placeholder="https://" value={this.state.linkedin} onChange={this.handleInputChange} />
              </FormGroup>
              */}
            </div>
          </FormGroup>

          <FormGroup className="submission-wrapper">
              <div className="alert-wrapper">
                  <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                  <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
              </div>
              <Button type="submit">Save</Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default AboutMeView;
