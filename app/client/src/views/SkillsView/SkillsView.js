import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert } from 'reactstrap';


class SkillsView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          alerts: { success: false, failure: false },
          skills: [],
          currentInput: ''
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.submitForm = this.submitForm.bind(this);
      this.addSkill = this.addSkill.bind(this);
  }

  addSkill() {
    const newSkill = 'Marketing'
    const { skills } = this.state;

    this.setState({
      skills: [...skills, newSkill]
    })
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;

    this.setState({
        currentInput: value
    });
  }

  renderSkills() {
    const { skills } = this.state;

    if (!skills.length) {
      return (
        <div>
          <Input type="text" name="skills" placeholder="Good evey for color, texture and shape"></Input>
          <Input type="text" name="skills" placeholder="Commercial awareness"></Input>
          <Input type="text" name="skills" placeholder="Drawing Skills"></Input>
        </div>
      )
    }

    return skills.map((skill, i) => {
      return (
        <Input
          key={i}
          type="text"
          name="skills"
          value={this.state.currentInput}
          placeholder={skill}
          onChange={this.handleInputChange}>
        </Input>
      )
    })
  }

  submitForm(e) {
    e.preventDefault();

    this.setState({
        alerts: {
            success: false,
            failure: false
        }
    }, () => console.log(this.state));
  }

  render() {
    return(
      <div className="SkillsView UserEditView">
          <div className="heading-wrapper">
              <h1 className="heading">Add Skills</h1>
              <hr />
          </div>

        <div className="skills-form">
          <Form className="container" onSubmit={this.submitForm}>
            <FormGroup>
              <Label for="skills">Enter Your Skills</Label>
              <div className="fas fa-plus-circle" onClick={(e) => this.addSkill(e)}> Add Skill</div>
              {this.renderSkills()}
            </FormGroup>

            <FormGroup className="submission-wrapper">
              <div className="alert-wrapper">
                <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
              </div>
              <Button type="submit">Save</Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    )
  }
}




export default SkillsView;
