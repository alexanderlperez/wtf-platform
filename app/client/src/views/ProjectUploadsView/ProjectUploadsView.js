import React from 'react';
import { withCookies } from 'react-cookie';
import { Redirect } from 'react-router';
import { Button, Form, FormGroup, Label, Input, FormText  } from 'reactstrap';
import { graphql, compose  } from 'react-apollo';
import gql from 'graphql-tag';
import Dropzone from 'react-dropzone';
import TagsInput from 'react-tagsinput';
import watermark from 'watermarkjs';

import { uploadCategories, supAsterisk } from '../../services/Constants';

import User from '../../queries/User';
import CreateProject from '../../queries/CreateProject';
import AddCoverImage from '../../queries/AddCoverImage';

import Uploader from '../../components/Uploader';
import LicenseDrop from '../../components/LicenseDrop';

import WrapWithModal from '../../components/WrapWithModal';
import Cropper from '../../components/Cropper';
import Watermark from '../../components/Watermark';
import { X_OK } from 'constants';

const DO_PREVIEW = true;

class ProjectUploadsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId:'',
            category: "-1",
            name:'',
            title:'',
            description:'',
            tags:[],
            projectMetadata: '',
            images: [],
            coverImage: '',
            license:'',
            saveFullDetails:'',
            editorChoice:'',

            url: this.props.match.url,
            redirect: false,
            openCrop: false,
            openWatermark: false,
            uploadProgressIds: [],
            watermark: "no",
            position: 'center',
            uploadProgressIds: [],
        };

        this.uppy = null;

        this.onDrop = this.onDrop.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onRemove = this.onRemove.bind(this);
        this.submitProject = this.submitProject.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onCropImageSave = this.onCropImageSave.bind(this);
        this.uploadStart = this.uploadStart.bind(this);
        this.uploadSuccess = this.uploadSuccess.bind(this);
        this.uploadFails = this.uploadFails.bind(this);
        this.handleTagsChange = this.handleTagsChange.bind(this);
        this.onLicenseChange = this.onLicenseChange.bind(this);
        this.onUppyLoad = this.onUppyLoad.bind(this);
        this.onPublish = this.onPublish.bind(this);
        this.submitWatermark = this.submitWatermark.bind(this);
        this.onChangePosition = this.onChangePosition.bind(this);
    }

    onUppyLoad(uppy) {
        this.uppy = uppy;
        this.goBack = this.goBack.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;


        this.setState({
            [name]: value
        });
    }

    handleTagsChange(tags) {
        this.setState({tags})
    }

    onLicenseChange(value) {
        console.log('license value', value);
        this.setState({
            license: value.value
        })
    }

    onDrop(results) {
        // event.preventDefault();
        // let images = results.map((res) => ({
        //     id: res.id,
        //     filename: res.data.name,
        //     path: res.response.uploadURL,
        //     thumbnail: res.response.uploadURL
        // }))
        // this.setState({
        //     images: images,
        //     uploadProgressIds: []
        // })
    }

    onAdd(file) {
        // this.setState({
        //     images: [...this.state.images, file]
        // })
    }

    onRemove(file) {
        // let images = this.state.images.filter((image) => {
        //     return image.id !== file.id
        // })
        // this.setState({
        //     images: images
        // })
    }

    uploadStart(data) {
        // this.setState({
        //     uploadProgressIds: this.state.uploadProgressIds.concat(data.fileIDs)
        // })
    }
    uploadSuccess(file) {
        // let index = this.state.uploadProgressIds.indexOf(file.id);
        // if(index > -1) {
        //     this.setState({
        //         uploadProgressIds: this.state.uploadProgressIds.splice(index, 1)
        //     })
        // }
    }
    uploadFails(failures) {
        // failures.forEach(fail => {
        //     let index = this.state.uploadProgressIds.indexOf(fail.id);
        //     if(index > -1) {
        //         this.setState({
        //             uploadProgressIds: this.state.uploadProgressIds.splice(index, 1)
        //         })
        //     }
        // })
    }

    onPublish(e) {
        e.preventDefault();
        const { files } = this.uppy.getState();
        let images = [];

        for(var key in files) {
            images.push(files[key])
        }

        this.setState({
            images
        })

        console.log('what is watermark', this.state.watermark);

        if(this.state.watermark=='yes') {
            this.setState({
                openWatermark: true
            })
        } else {
            this.submitProject(e)
        }
    }

    async submitProject(e) {
        if(e) {
            e.preventDefault();
        }

        const { mutate } = this.props;
        let uppyResults = await this.uppy.upload();
        console.log('after upload', uppyResults);

        let images = uppyResults.successful.map((res) => ({
            id: res.id,
            filename: res.name,
            path: res.uploadURL,
            thumbnail: res.uploadURL
        }))

        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.props.cookies.get('username'),
                    title: this.state.title,
                    category: this.state.category,
                    description: this.state.description,
                    projectPrivacy: 'NA',
                    tags: this.state.tags,
                    images: images,
                    license: this.state.license,
                    type: 'project'
                }
            })
            this.setState({
                openCrop: true,
                projectId: res.data.createProject.id,
                images: uppyResults.successful
            })
        } catch (err) {
            console.log(err);
            this.setState({
                alerts: {
                    success: false,
                    failure: true
                }
            })

            return;
        }
    }

    goBack() {
        this.setState({
            redirect: true
        })
    }

    textManipulation(username, position) {
        
        let translationRatios = {
            center: {
                x:1/2,
                y:1/2
            },
            topleft: {
                x:1/3,
                y:1/3
            },
            topright: {
                x:2/3,
                y:1/3
            },
            bottomleft:{
                x:1/3,
                y:2/3
            },
            bottomright:{
                x:2/3,
                y:2/3
            }
        }

        return (target) => {
            let context = target.getContext('2d');

            let fontSize = Math.round(target.width*0.06);

            context.globalAlpha = 0.6;
            context.fillStyle = '#fff';
            context.font = fontSize+'px Josefin Slab';

            let metrics = context.measureText(username);

            let x = Math.round(target.width * translationRatios[position].x) - Math.round(metrics.width/2);
            let y = Math.round(target.height * translationRatios[position].y) - Math.round(fontSize/2);
            
            console.log(target, translationRatios[position], metrics, x, y);

            context.translate(x, y);
            context.fillText(username, 0, 0)

            return target;
        }
    }

    async compressImage(image, quality, type) {
        let canvas = document.createElement('canvas');

        canvas.width = image.width;
        canvas.height = image.height;

        let ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0);
        return canvas.toDataURL(type, quality)
    }

    async onCropImageSave(data) {
        const mutate = this.props.coverImageMutate;
        let coverImage = data.coverImage;
        if(this.state.watermark=='yes') {
            coverImage = await watermark([data.coverImage])
                .image(this.textManipulation(this.props.cookies.get('username'), this.state.position))
                .then(img => {
                    return this.compressImage(img, 0.8, data.type)
                })
        }
        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.props.cookies.get('username'),
                    projectId: this.state.projectId,
                    coverImage: coverImage
                }
            })
            this.setState({
                openCrop: false,
                redirect: true
            })
        } catch (err) {
            console.log(err);
            this.setState({
                alerts: {
                    success: false,
                    failure: true
                }
            })

            return;
        }
        // {
        //     imageUrl:
        //     width:
        //     height:
        //     x:
        //     y:
        //     rotate:
        // }
    }
    onChangePosition(position) {
        this.setState({
            position: position
        })
    }
    submitWatermark(position) {
        this.setState({
            openWatermark: false
        })
        this.submitProject();
    }

    render() {
        const categoryOptions = uploadCategories.map((cat, i) => <option key={i} value={cat}>{cat}</option>)

        const { url } = this.state;

        if(this.state.redirect) {
            return <Redirect push to={`/account/${this.props.cookies.get('username')}/portfolio`} />;
        }

        let cropper = <Cropper images={this.state.images} onSave={this.onCropImageSave} />
        let watermark = <Watermark username={this.props.cookies.get('username')} images={this.state.images} onChangePosition={this.onChangePosition} onSave={this.submitWatermark} />

        const isSubmitDisabled = () => {
            const isValid = !!this.state.title.length && this.state.category != "-1";

            if (!isValid) {
                return true;
            }

            return false;
        }

        return (
            <div className="ProjectUploadsView">
                <Form onSubmit={this.onPublish}>
                    <FormGroup>
                        <Label>Project Name{supAsterisk}</Label>
                        <Input placeholder="Name" name="title" value={this.state.title} onChange={this.handleInputChange}></Input>
                    </FormGroup>
                    <FormGroup>
                        <Label>Project Description</Label>
                        <Input rows={10} type="textarea" placeholder="Description" name="description" value={this.state.description} onChange={this.handleInputChange}></Input>
                    </FormGroup>

                    <div className="row">
                        <div className="col-md-6">
                            <FormGroup>
                                <Label>Category{supAsterisk}</Label>
                                <Input className="dropdown-border" type="select" name="category" value={this.state.category} onChange={this.handleInputChange}>
                                    <option value={-1}>Select a category</option>
                                    {categoryOptions}
                                </Input>
                            </FormGroup>
                        </div>

                        <div className="col-md-6">
                            <FormGroup>
                                <Label className="dropdown-border">License</Label>
                                <LicenseDrop onChange={this.onLicenseChange} />
                            </FormGroup>
                        </div>

                        <div className="col-md-6">
                            <FormGroup>
                                <Label>Tags</Label>
                                <TagsInput value={this.state.tags} onChange={this.handleTagsChange} />
                            </FormGroup>
                        </div>

                        <div className="col-md-6">
                            <div className="row">
                                <div className="col-md-6">
                                    <FormGroup tag="fieldset" className="radio-select">
                                        <Label className="underline">Add Watermark</Label>

                                        <div className="row">
                                            <div className="col-md-4">
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="radio" name="watermark" value="yes" checked={this.state.watermark=="yes"} onChange={this.handleInputChange}></Input>
                                                        Yes
                                                    </Label>
                                                </FormGroup>
                                            </div>
                                            <div className="col-md-4">
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="radio" name="watermark" value="no" checked={this.state.watermark=="no"} onChange={this.handleInputChange}></Input>
                                                        No
                                                    </Label>
                                                </FormGroup>
                                            </div>
                                        </div>
                                    </FormGroup>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="upload-wrapper">
                        <Uploader
                            watermark={this.state.watermark}
                            position={this.state.position}
                            username={this.props.cookies.get('username')}
                            handleUploadStart={this.uploadStart}
                            handleUploadSuccess={this.uploadSuccess}
                            handleUploadComplete={this.onDrop}
                            handleUploadFails={this.uploadFails}
                            handleRemoval={this.onRemove}
                            handleAdd={this.onAdd}
                            onLoad={this.onUppyLoad} />
                    </div>

                    <div className="controls-wrapper">
                        <div>
                            {
                                /*
                                *<Button onClick={() => this.submitProject(null, DO_PREVIEW)} color="outline-danger">Preview</Button>
                                */
                                }
                            <Button color="outline-danger" onClick={this.goBack}>Cancel</Button>
                            <Button color="outline-danger">Preview</Button>
                            <Button type="submit" color="danger" disabled={isSubmitDisabled()}>Publish Project &gt;</Button>
                        </div>
                    </div>
                </Form>

                <WrapWithModal isOpen={this.state.openWatermark} toggle={() => {}} children={watermark} />
                <WrapWithModal isOpen={this.state.openCrop} toggle={() => {}} children={cropper} />
            </div>
        );
    }
}


export default compose(
    withCookies,
    graphql(CreateProject),
    graphql(AddCoverImage, { name:'coverImageMutate' })
)(ProjectUploadsView);
