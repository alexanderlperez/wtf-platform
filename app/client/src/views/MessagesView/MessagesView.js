import React from 'react';
import { withCookies } from 'react-cookie';
import { graphql, compose, withApollo } from 'react-apollo';
import * as stream from 'getstream';

import { Media, Input, Alert, Button, Form, FormGroup, Label } from 'reactstrap';
import Autosuggest from 'react-autosuggest';
import Textarea from "react-textarea-autosize";
import classNames from 'classnames';

import Avatar from '../../components/Avatar';
import CommentHeading from '../../components/CommentHeading';
import CommentBody from '../../components/CommentBody';
import ProBadge from '../../components/ProBadge';
import WithInputTimeout from '../../components/WithInputTimeout';
import withUser from '../../hocs/withUser'

import { prefixHash } from '../../services/Text';
import { isSubscribed } from '../../services/UserMeta';

import AddMessage from '../../queries/AddMessage';
import GetMessages from '../../queries/GetMessages';
import StreamTokens from '../../queries/StreamTokens';
import GetConnections from '../../queries/GetConnections';
import GetConversations from '../../queries/GetConversations';
import CreateConversation from '../../queries/CreateConversation';


const ConversationEntry = ({ username, conversation, handleClick }) => {
    const user = conversation.users.find((u) => {
        return u.username !== username
    })
    return (
        <Media className="ConversationEntry" onClick={() => handleClick(conversation)}>
            <Media left href="#">
                <Avatar filename={user.profileImages.avatar} />  
            </Media>

            <Media body>
                <Media heading className="fullname-wrapper">
                    {user.fullname}
                    <div className="online-wrapper"></div>
                    <ProBadge size="medium" isVisible={user.accountMeta.subscription=='PRO'} />
                </Media>
                <div className="meta">
                    <span className="profession">{conversation.lastMessage ? conversation.lastMessage.message : ''}</span>
                </div>
            </Media>
        </Media>
    );
};

const UserEntry = ({ user, handleClick }) => {
    return (
        <Media className="UserEntry" onClick={() => handleClick(user)}>
            <Media left href="#">
                <Avatar  filename={user.profileImages.avatar} />  
            </Media>

            <Media body>
                <Media heading className="fullname-wrapper">
                    {user.fullname}
                    <div className="online-wrapper"></div>
                    <ProBadge size="medium" isVisible={user.accountMeta.subscription=='PRO'} />
                </Media>
                <div className="meta">
                    <span className="profession">{prefixHash([user.about.profession])}</span>
                </div>
            </Media>
        </Media>
    );
};

const MessageEntry = ({ message, username }) => {
    let avatar;

    if(message.user.username==username) {
        return (
            <Media className="MessageEntry text-right light">
                <Media body>
                    <div className="bubble">
                        {message.message}
                    </div>
                </Media>
                <Media right href="#">
                    <Avatar filename={message.user.profileImages.avatar} />  
                </Media>
            </Media>
        );
    } else {
        return (
            <Media className="MessageEntry text-left dark">
                <Media left href="#">
                    <Avatar filename={message.user.profileImages.avatar} />  
                </Media>
    
                <Media body>
                    <div className="bubble">
                        {message.message}
                    </div>
                </Media>
            </Media>
        );
    }

};

class ConversationMessagesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}

        this.messagesList;
    }

    render() {
        const list = this.props.messages.map((message, i) => (
            <MessageEntry key={i} username={this.props.username} message={message} />
        ))

        return (
            <div className="ConversationMessagesList">
                <div className="scroll-wrapper" ref={(elem) => { this.messagesList = elem; }}>
                    {list}
                </div>
            </div>
        );
    }
};



class MessageSubmission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alerts: { failure: false },
            message: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    handleInputChange(event) {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        });
    }

    async submitForm(e) {
        e && e.preventDefault();

        const { createConversation, addMessage } = this.props;

        this.setState({ 
            alerts: { 
                failure: false 
            } 
        })

        let conversationId = this.props.conversationId;

        if(this.props.newConvo && this.props.target) {
            let convoRes;
            try {
                convoRes = await createConversation({ variables: {
                    username: this.props.username,
                    targets: [this.props.target]
                } })
            } catch (err) {
                this.setState({ 
                    alerts: { 
                        failure: true 
                    } 
                })
    
                return;
            }
            conversationId = convoRes.data.createConversation.id
            this.props.handleNewConvo(convoRes.data.createConversation);
        }
        if(!conversationId) {
            this.setState({ 
                alerts: { 
                    failure: true
                } 
            })
            return;
        }

        let res;
        try {
            res = await addMessage({ variables: {
                username: this.props.username,
                conversationId: conversationId,
                message: this.state.message
            } })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    failure: true 
                } 
            })

            return;
        }

        this.setState({ 
            alerts: { 
                failure: false 
            }
        })

        this.props.handleNewMessage(res.data.addMessage);
    }

    render() {
        if(!this.props.conversationId && !this.props.newConvo) {
            return 'Select a Conversation, or create a new one';
        }
        return (
            <div className="MessageSubmission">
                <Form onSubmit={this.submitForm}>
                    <Input type="textarea" rows={2} placeholder="message" name="message" value={this.state.message} onKeyPress={this.handleEnterSubmission} onChange={this.handleInputChange} />
                    
                    <div className="alert-wrapper">
                        <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                    </div>

                    <Button type="submit">Save</Button>
                </Form>
            </div>
        );
    }
}

class ComposeMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alerts: { failure: false },
            suggestions: [],
            user: '',
            userValue: '',
            message: ''
        };

        this.timeoutHandler = null;

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
        this.getConnections = this.getConnections.bind(this);
        this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
        this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    handleInputChange(event) {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        });
    }

    handleToChange(event) {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        });
    }

    async submitForm(e) {
        e && e.preventDefault();

        const { createConversation, addMessage } = this.props;

        this.setState({ 
            alerts: { 
                failure: false 
            } 
        })

        let convoRes;
        try {
            convoRes = await createConversation({ variables: {
                username: this.props.username,
                targets: [this.state.user.id]
            } })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    failure: true 
                } 
            })

            return;
        }
        let conversationId = convoRes.data.createConversation.id
        this.props.handleNewConvo(convoRes.data.createConversation);

        if(!conversationId) {
            this.setState({ 
                alerts: { 
                    failure: true
                } 
            })
            return;
        }

        let res;
        try {
            res = await addMessage({ variables: {
                username: this.props.username,
                conversationId: conversationId,
                message: this.state.message
            } })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    failure: true 
                } 
            })

            return;
        }

        this.setState({ 
            alerts: { 
                failure: false 
            }
        })

        this.props.handleNewMessage(res.data.addMessage);
    }

    async getConnections(search) {
        const connections = await this.props.client.query({
            query: GetConnections,
            fetchPolicy:'network-only',
            variables: {
                username: this.props.username,
                search: search
            }
        })
        this.setState({
            suggestions: connections.data.getConnections
        })
    }

    onChange(event, { newValue, method }) {
        this.setState({
            userValue: newValue
        })
    };

    onSuggestionsFetchRequested({ value }) {
        if(this.timeoutHandler) {
            clearTimeout(this.timeoutHandler);
            this.timeoutHandler = null;
        }
        this.timeoutHandler = setTimeout(() => {
            this.getConnections(value);
        }, 1000)
    };

    onSuggestionsClearRequested() {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) {
        this.setState({
            user: suggestion
        })
    }

    renderSuggestion(suggestion) {
        return (
            <span>{suggestion.fullname} - {suggestion.username}</span>
        );
    }

    getSuggestionValue(suggestion) {
        return suggestion.username;
    }

    render() {
        const inputProps = {
            placeholder: "Search Username",
            value: this.state.userValue,
            onChange: this.onChange
        };

        return (
            <div className="MessageSubmission">
                <Form onSubmit={this.submitForm}>
                    <FormGroup>
                        <Label>To</Label>
                        <Autosuggest
                            suggestions={this.state.suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            onSuggestionSelected={this.onSuggestionSelected}
                            getSuggestionValue={this.getSuggestionValue}
                            renderSuggestion={this.renderSuggestion}
                            inputProps={inputProps} />
                    </FormGroup>
                    <FormGroup>
                        <Label>Message</Label>
                        <Input type="textarea" rows={2} placeholder="message" name="message" value={this.state.message} onKeyPress={this.handleEnterSubmission} onChange={this.handleInputChange} />
                    </FormGroup>
                    <FormGroup>
                        <div className="alert-wrapper">
                            <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                        </div>

                        <Button type="submit">Send</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

class MessagesView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            curConvo: null,
            targetUser: '',
            active: 'conversations',
            messages: [],
            conversations: [],
            connections: [],
            newConvo: false,
            compose: false
        }

        this.MessageSubmission = compose(
            graphql(AddMessage, { name: 'addMessage' }),
            graphql(CreateConversation, { name: 'createConversation' })
        )(MessageSubmission);

        this.ComposeMessage = compose(
            withApollo,
            graphql(AddMessage, { name: 'addMessage' }),
            graphql(CreateConversation, { name: 'createConversation' })
        )(ComposeMessage);

        this.toggleNav = this.toggleNav.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.refreshConvos = this.refreshConvos.bind(this);
        this.selectConvo = this.selectConvo.bind(this);
        this.getConnections = this.getConnections.bind(this);
        this.selectNewConvo = this.selectNewConvo.bind(this);
        this.updateConvo = this.updateConvo.bind(this);
        this.updateMessages = this.updateMessages.bind(this);


        this.username = this.props.cookies.get('username');
        this.tokens = null;
        this.client = null;

        this.subscriptions = [];
    }

    toggleNav(active) {
        this.setState({
            active
        })
    }

    componentDidMount() {
        this.props.client.query({
            query: StreamTokens,
            fetchPolicy:'network-only',
            variables: {
                username: this.username
            }
        }).then(tokens => {
            this.tokens = tokens.data.streamTokens
            
            this.client = stream.connect(
                this.tokens.api,
                null,
                this.tokens.appId
            );
    
            var feed = this.client.feed(
                "messages",
                this.tokens.userId,
                this.tokens.messages
            );
    
            feed.subscribe((response) => {
                    this.refreshConvos();
                    console.log('conversation update', response);
                }).then((success) => {
                    console.log('success connection', success)
                }, (error) => {
                    console.log('error connection', error);
                })

            this.refreshConvos();
            this.getConnections();
        })
    }

    async refreshConvos() {
        this.subscriptions.forEach(s => {
            if(s)
                s.cancel();
        })
        this.subscriptions = [];

        const conversations = await this.props.client.query({
            query: GetConversations,
            fetchPolicy:'network-only',
            variables:{
                username: this.username
            }
        })

        this.subscriptions = conversations.data.getConversations.map(c => {
            var feed = this.client.feed(
                "timeline",
                c.id,
                c.token
            );
            return feed.subscribe((res) => {
                if(this.state.curConvo && this.state.curConvo.id == c.id) {
                    this.selectConvo(c);
                }
                console.log('new message', res)
            })
        })

        this.setState({
            conversations: conversations.data.getConversations
        })
    }

    async selectConvo(convo) {
        const messages = await this.props.client.query({
            query: GetMessages,
            fetchPolicy:'network-only',
            variables: {
                username: this.username,
                conversationId: convo.id
            }
        })
        let target = convo.users.find((u) => {
            return u.username !== this.username
        })
        this.setState({
            messages: messages.data.getMessages,
            newConvo: false,
            curConvo: convo,
            targetUser: target,
            compose: false
        })
    }

    async selectNewConvo(user) {
        const convo = this.state.conversations.find((c) => {
            return c.users.find((u) => {
                return u.username == user.username;
            })
        })
        if(convo) {
            return this.selectConvo(convo);
        }
        this.setState({
            newConvo: true,
            messages: [],
            curConvo: null,
            targetUser: user,
            compose: false
        })
    }

    async getConnections() {
        const connections = await this.props.client.query({
            query: GetConnections,
            fetchPolicy:'network-only',
            variables: {
                username: this.username
            }
        })
        this.setState({
            connections: connections.data.getConnections
        })
    }

    async updateConvo(convo) {
        this.setState({
            curConvo: convo,
            active: 'conversation',
            compose: false
        });
        this.refreshConvos();
    }

    async updateMessages(message) {
        this.selectConvo(this.state.curConvo)
    }

    render() {
        const { loading, error, comments } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { active, conversations, connections, messages, newConvo, curConvo, targetUser } = this.state;
        const isToggled = (itemName) => active == itemName ? 'active' : '';

        const converationsList = conversations.map((c, i) => (
            <ConversationEntry key={i} username={this.username} conversation={c} handleClick={this.selectConvo} />
        ))
        const connectionsList = connections.map((u, i) => (
            <UserEntry key={i} user={u} handleClick={this.selectNewConvo} />
        ))
        const listContent = active == 'conversations' ? converationsList : connectionsList;

        return (
            <div className="MessagesView">
                <div className="row">
                    <div className="left col-sm-5">
                        <div className="Conversations">
                            <ul className="nav">
                                <li className={isToggled('conversations')} onClick={() => this.toggleNav('conversations')}>
                                    Inbox
                                </li>
                                {/*
                                <li className={isToggled('connections')} onClick={() => this.toggleNav('connections')}>
                                    Connections
                                </li>
                                */}
                                <li className="pull-right">
                                    <Button onClick={() => {this.setState({compose:!this.state.compose})}}>Compose</Button>
                                </li>
                            </ul>

                            <hr />

                            <div className="users-list">
                                <div className="followers">
                                    {listContent}
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    {this.state.compose ? (
                            <div className="right col-sm-7">
                                <h3>New Message</h3>
                                <hr />
                                <this.ComposeMessage
                                    username={this.username}
                                    handleNewConvo={this.updateConvo}
                                    handleNewMessage={this.updateMessages}
                                    />
                            </div>
                        ) : (
                            <div className="right col-sm-7">
                                {targetUser ? (
                                    <div>
                                        <h3>Message with {targetUser.fullname}</h3>
                                        <hr />
                                    </div>
                                ) : ''}
                                <ConversationMessagesList messages={messages} username={this.username} />
                                <this.MessageSubmission 
                                    username={this.username}
                                    conversationId={curConvo ? curConvo.id : ''}
                                    target={targetUser.id}
                                    newConvo={newConvo}
                                    handleNewConvo={this.updateConvo}
                                    handleNewMessage={this.updateMessages} />
                            </div>
                        )}


                </div>
            </div>
        );
    }
}



export default compose(
    withCookies,
    withApollo
)(MessagesView)
