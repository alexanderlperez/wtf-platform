import React from 'react';
import { 
    InputGroup, 
    InputGroupAddon, 
    InputGroupDropdown,
    InputGroupButtonDropdown, 
    Alert, 
    Button, 
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText, 
    Dropdown, 
    DropdownToggle, 
    DropdownMenu, 
    DropdownItem
} from 'reactstrap';
import ReactTelInput from 'react-telephone-input';
import MaskedInput from 'react-text-mask';
import { graphql, compose  } from 'react-apollo';
import gql from 'graphql-tag';
import User from '../../queries/User';
import { addCountryCode } from '../../services/Text';
import tinyFlags from '../../assets/images/tiny-flags.png';
import SubmitBasicInfo from '../../queries/SubmitBasicInfo';

class BasicInformationView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alerts: { success: false, failure: false },
            username: this.props.username,
            profileImages: {
                avatar: '',
                timeline: ''
            },
            contact: {
                firstName:'',
                lastName:'',
                address:'',
                city:'',
                country: {
                    name:'',
                    short:''
                },
                email:'',
                countryCode:'',
                phone:'',
                state:'',
                zipcode:''
            },
            accountMeta: {
                changedUsername: false, // Y/N
                slug:'',
                registerFor:'',
                registerType:'',
                subscription:''
            },
            about: {
                title: '',
                gender: '',
                description: '',
                birthdate: '',
                personalDetail: 0,
                profession: ''
                /*
                'Fashionista',
                'Makeup',
                'Jewellery design',
                'Hair style',
                'Tattoo design',
                'Fashion photography',
                'Accessories',
                'Embroidery',
                'Children’s wear',
                'Costume' ,
                'Lingeries',
                'Menswear',
                'Womenwear',
                'Sportswear',
                'Wedding',
                'Textile',
                'Footwear',
                'Sketch',
                'Henna',
                'Ethnicwear',
                'Interior Design',
                'Leather',
                'Fashion communication',
                'Fashion Designers',
                'Jewellery Designer',
                'Hair stylist',
                'Fashion Photographers',
                'Tattoo Artist',
                'Fashion model',
                'Concept Artist',
                'Interior Designer',
                'Textile Designer',
                'Shoe Designer',
                'Accessories Designer',
                'Craftworker',
                'Couture/Boutique',
                'Fashion',
                'Nail art',
                'Costume (stage screen dance)',
                'Lingeries/Swimwear',
                'Sketch/CAD',
                'Inspiration',
                'DIY',
                'Wedding wear',
                'Looks',
                'Creation &amp; Creative',
                'NAilArt',
                'Makeup Artist',
                'The Look',
                'Fitness',
                'Fashion Merchandiser',
                'Blogger/Writer',
                'Fashion Stylist',
                '3D Printing',
                'Hand Bags',
                'Fashion Styling',
                'DIY Decor',
                'Kids'
                */
            },
            resume: {
                resumePath:'',
                brochuresPath:''
            },
            awards:[/*
            {
                organization:String,
                url:String,
                title:String,
                year:Number,
                month:Number
            }
            */],
            education:[/*
            {
                name:String,
                website:String,
                degree:String,
                city:String,
                state: {
                    name:{
                        type:String,
                        default:''
                    },
                    short:{
                        type:String,
                        default:''
                    }
                },
                country: {
                    name:{
                        type:String,
                        default:''
                    },
                    short:{
                        type:String,
                        default:''
                    }
                },
                entryDate:{
                    year:Number,
                    month:Number
                },
                duration: {
                    years:Number,
                    months:Number
                },
                graduated: Boolean
            }
            */],
            languages:[
                /*
                    {
                        language: String,
                        proficiency: {
                            type:String,
                            enum:[
                                'Beginner',
                                'Advanced',
                                'Conversational',
                                'Native',
                                'Fluent'
                            ]
                        }
                    }
                */
            ],
            workExperience:[/*
            {
                companyName:String,
                companyWebsite:String,
                city:String,
                state: {
                    name:{
                        type:String,
                        default:''
                    },
                    short:{
                        type:String,
                        default:''
                    }
                },
                country: {
                    name:{
                        type:String,
                        default:''
                    },
                    short:{
                        type:String,
                        default:''
                    }
                },
                title:String,
                details:String,
                startYear:Number,
                startMonth:Number,
                endYear:Number,
                endMonth:Number
            }
            */],

            skills:[/* String */],

            socialLinks:[/*
                {
                    link:String,
                    type:{
                        type:String,
                        enum:[
                            'facebook',
                            'twitter',
                            'linkedin',
                            'googleplus',
                            'website'
                        ]
                    }
                }
            */]
        };

        this.changedFields = []

        this.handleAboutInputChange = this.handleAboutInputChange.bind(this);
        this.handleContactInputChange = this.handleContactInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.toggleDropdownState = this.toggleDropdownState.bind(this);
    }

    toggleDropdownState() {
        this.setState({
            isCountryCodeOpen: !this.state.isCountryCodeOpen
        })
    }

    handleAboutInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(!this.changedFields.find(a => (a=='about'))) {
            this.changedFields.push('about');
        }

        let about = Object.assign({}, this.state.about, { [name]:value })
        
        this.setState({
            about
        });
    }

    handleContactInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(!this.changedFields.find(a => (a=='contact'))) {
            this.changedFields.push('contact');
        }

        let contact = Object.assign({}, this.state.contact, { [name]:value })

        this.setState({
            contact
        });
    }

    async submitForm(e) {
        e.preventDefault();

        if(!this.changedFields.length) {
            return;
        }

        this.setState({ 
            alerts: { 
                success: false, 
                failure: false 
            } 
        })

        const { mutate } = this.props;

        let body = {}

        this.changedFields.forEach(f => {
            if(this.state[f])
                body[f] = this.state[f];
        })

        let res;
        try {
            res = await mutate({ variables: {
                username: this.props.username,
                set: body
            } })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    success: false, 
                    failure: true 
                } 
            })

            return;
        }

        const { data: { submitBasicInfo: { about, contact } } } = res;

        this.setState({ 
            alerts: { 
                success: true, 
                failure: false 
            },
            about,
            contact
        })
    }

    componentWillReceiveProps(newProps) {
        if (!newProps.loading) {
            const { 
                user: {
                    contact,
                    about
                }
            } = newProps;

            this.setState({
                contact,
                about
            });
        }
    }

    render() {
        const { error, loading } = this.props;

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <div className="BasicInformationView UserEditView">
                <div className="heading-wrapper">
                    <h1 className="heading">Edit your personal info</h1>
                    <hr />
                </div>

                <Form onSubmit={this.submitForm}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-md-12">
                                <FormGroup>
                                    <Label>First Name</Label>
                                    <Input placeholder="Enter your first name" name="firstName" value={this.state.contact.firstName} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>Last Name</Label>
                                    <Input placeholder="Enter your last name" name="lastName" value={this.state.contact.lastName} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>Email</Label>
                                    <Input type="email" placeholder="Enter your email" name="email" value={this.state.contact.email} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>Mobile</Label>

                                    <div className="inline">
                                        <ReactTelInput
                                            defaultCountry="in"
                                            flagsImagePath={tinyFlags}
                                            value={this.state.contact.phone}
                                            onChange={(num, country) => {
                                                this.handleContactInputChange({ 
                                                    target: {
                                                        type: 'select',
                                                        name: 'countryCode',
                                                        value: country.dialCode,
                                                        
                                                    }
                                                })
                                                this.handleContactInputChange({ 
                                                    target: {
                                                        type: 'select',
                                                        name: 'phone',
                                                        value: num,
                                                        
                                                    }
                                                })
                                            }}
                                            onBlur={(num, country) => {
                                                this.handleContactInputChange({ 
                                                    target: {
                                                        type: 'select',
                                                        name: 'countryCode',
                                                        value: country.dialCode,
                                                        
                                                    }
                                                })
                                                this.handleContactInputChange({ 
                                                    target: {
                                                        type: 'select',
                                                        name: 'phone',
                                                        value: num,
                                                        
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                </FormGroup>

                                {/*<FormGroup>
                                    <Label>Web Url</Label>
                                    <Input type="url" placeholder="Include http:// or https://" name="websiteUrl" value={this.state.websiteUrl} onChange={this.handleInputChange}/>
                                </FormGroup>*/}


                            </div>

                            <div className="col-lg-6 col-md-12">
                                <FormGroup>
                                    <Label>Address</Label>
                                    <Input placeholder="Enter your address" name="address" value={this.state.contact.address} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>Country</Label>
                                    { /* this will be filled out automatically */}
                                    <Input placeholder="" name="country.name" value={this.state.contact.country.name} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>State</Label>
                                    { /* this will be filled out automatically */}
                                    <Input placeholder="" name="state" value={this.state.contact.state.name} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>City</Label>
                                    { /* this will be filled out automatically */ }
                                    <Input placeholder="" name="city" value={this.state.contact.city} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                                <FormGroup>
                                    <Label>Zip Code</Label>
                                    { /* this will be filled out automatically */ }
                                    <Input placeholder="" name="zipcode" value={this.state.contact.zipcode} onChange={this.handleContactInputChange}/>
                                </FormGroup>

                            </div>
                        </div>
                    </div>

                    <FormGroup className="submission-wrapper">
                        <div className="alert-wrapper">
                            <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                            <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                        </div>
                        <Button type="submit">Save</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default compose(
    graphql(SubmitBasicInfo),
    graphql(User, { props: ({ ownProps, data: {user, loading, error} }) => ({ 
        ...ownProps,
        loading,
        error,
        user
    })})
)(BasicInformationView);
