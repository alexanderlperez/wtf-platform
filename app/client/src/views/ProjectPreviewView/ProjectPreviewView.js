import React from 'react';
import { Button  } from 'reactstrap';

import DetailsHeader from '../../components/DetailsHeader';
import DetailsImages from '../../components/DetailsImages';
import DetailsActions from '../../components/DetailsActions';
import DetailsMeta from '../../components/DetailsMeta';

import Globals from '../../services/Globals';
import { exampleProject } from '../../services/DummyDocs';
import withUser from '../../hocs/withUser'

class ProjectPreviewView extends React.Component {
    constructor(props) {
        super(props);

        this.UserProjectPreview = withUser(Globals.get('username'))
    }

    render() {

        return (
            <div className="ProjectPreviewView">
                <div className="button-wrapper container-fluid row">
                    <Button>&lt; Back to Edit</Button>
                </div>
                <div className="content-wrapper container-fluid row">
                    <this.UserProjectPreview render= {(user) => {
                        const { avatar = '', fullname, about: { profession = '' }, contact } = user;
                        const project = exampleProject;
                        const { stats, description, comments, title, images, category: { categoryName = '' } } = project;

                        const shares = (stats) => stats ? stats.shares.length : 0;
                        const favorites = (stats) => stats ? stats.favorites.length : 0;
                        const likes = (stats) => stats ? stats.likes.length : 0;

                        return ( 
                            <div className="ProjectPreview">
                                <DetailsHeader 
                                    title={title}
                                    avatar={avatar} 
                                    fullname={fullname}
                                    profession={name}
                                    contact={contact}
                                    isPreview={true}
                                    />
                                <DetailsImages images={images} />
                                <DetailsMeta 
                                    description={description}
                                    tags={project.tags}
                                    category={categoryName}/>
                            </div>
                        )
                    }} />
                </div>
            </div>
        );
    }
}

export default ProjectPreviewView;
