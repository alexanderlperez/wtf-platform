import React from 'react';
import { withCookies } from 'react-cookie';
import { graphql, compose } from 'react-apollo';
import { NavLink } from 'react-router-dom';
import * as stream from 'getstream';

import StreamTokens from '../../queries/StreamTokens';
import Notifications from '../../queries/Notifications';
import NotificationList from '../../components/NotificationList';
import NotificationTest from '../../components/NotificationTest';
import Notification from '../../components/Notification';
import FollowingFollowers from '../../components/FollowingFollowers';
import TopUsers from '../../components/TopUsers';
import gear from '../../assets/images/gear.png';

class NotificationView extends React.Component {
    constructor(props) {
        super(props);

        this.NotificationList = compose(
                graphql(StreamTokens, { 
                    props: ({ownProps, data: { streamTokens, loading, error }}) => ({
                        ...ownProps,
                            tokens: streamTokens,
                            loading,
                            error
                    }),
                    options: {variables: { username:this.props.cookies.get('username') }}
                }),
                graphql(Notifications, { 
                    props: ({ownProps, data: { notifications, loading, error }}) => ({
                        ...ownProps,
                            notifications,
                            loading,
                            error
                    }),
                    options: {variables: { username:this.props.cookies.get('username') }}
                })
            )
        (NotificationList)
    }

    render() {
        const { loading, error, comments, cookies } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <div className="NotificationView">
                <div className="sidebar">
                    <FollowingFollowers />
                    <TopUsers />
                </div>

                <div className="page-content">
                    <div className="content-wrapper">
                        <h3 className="heading">Notifications</h3>

                        <NavLink to="#">
                            <img src={gear} alt="" />
                        </NavLink>
                    </div>

                    <hr />

                    <this.NotificationList 
                        render={(activities) => activities.map((activity, i) => <Notification key={i} activity={activity} />)}
                    />
                </div>
            </div>
        );
    }

}

export default withCookies(NotificationView);
