import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert, Table } from 'reactstrap';


class WorkExperienceView extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          alerts: { success: false, failure: false },
          company: '',
          country: '',
          state: '',
          city: '',
          website: '',
          position: '',
          startDate: '',
          endDate: '',
          experiences: [],
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.submitForm = this.submitForm.bind(this);
      this.addExperience = this.addExperience.bind(this);
      this.removeExperience = this.removeExperience.bind(this);
      this.renderExperiences = this.renderExperiences.bind(this);
      this.editExperience = this.editExperience.bind(this);
  }

  addExperience() {
    const { experiences, company, country, state, city, website, position, startDate, endDate } = this.state;
    const experience = {
      company,
      country,
      state,
      city,
      website,
      position,
      startDate,
      endDate
    }

    this.setState({
      alerts: { success: false, failure: false },
      company: '',
      country: '',
      state: '',
      city: '',
      website: '',
      position: '',
      startDate: '',
      endDate: '',
      experiences: [...experiences, experience]
    })
  }

  editExperience(event) {
    const target = event.target.parentNode.parentNode;
    const experience_id = target.dataset.id; //company
    const { experiences } = this.state;
    const experienceToEdit = experiences.reduce((expToEdit, experience) => {
      if (experience.company === experience_id) {
        expToEdit = Object.assign(experience)
      }
      return expToEdit
    }, {})

    this.setState({
      company: experienceToEdit.company,
      country: experienceToEdit.country,
      state: experienceToEdit.state,
      city: experienceToEdit.city,
      website: experienceToEdit.website,
      position: experienceToEdit.position,
      startDate: experienceToEdit.startDate,
      endDate: experienceToEdit.endDate,
      experiences: [...experiences.filter((experience) => experience.company !== experience_id)],
    })
  }

  removeExperience(event) {
    const target = event.target.parentNode.parentNode;
    const experience_id = target.dataset.id;
    const { experiences } = this.state;

    this.setState({
      experiences: [...experiences.filter((experience) => experience.company !== experience_id)]
    })
  }

  renderExperiences() {
    const { experiences } = this.state;

    if (!experiences.length) {
      return (
        <tr>
          <td>Fashion Studio</td>
          <td>https://wethefashion.com</td>
          <td>US, Colorado</td>
          <td>2016-Present</td>
          <td className="actions"> <div className="fas fa-edit"></div> <div className="fas fa-trash-alt"></div></td>
        </tr>
      )
    }

    return experiences.map((experience, i) => {
      let { company, country, state, city, website, position, startDate, endDate } = experience;

      return (
        <tr key={company} data-id={company}>
          <td>{company}</td>
          <td>{website}</td>
          <td>{city}, {state}, {country}</td>
          <td>{startDate || 'start'} - {endDate || 'end'}</td>
          <td className="actions">
            <div className="fas fa-edit" onClick={(e) => this.editExperience(e)}></div>
            <div className="fas fa-trash-alt" onClick={(e) => this.removeExperience(e)}></div>
          </td>
        </tr>
      )
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });
  }

  submitForm(e) {
    e.preventDefault();

    this.setState({
        alerts: {
            success: false,
            failure: false
        }
    }, () => console.log(this.state));
  }

  render(){
    return (
      <div className="WorkExperienceView UserEditView">
          <div className="heading-wrapper">
              <h1 className="heading">Add work experience details</h1>
              <hr />
          </div>

        <Form className="container" onSubmit={this.submitForm}>
          <FormGroup className="row">
            <div className="col-sm-6">
              <FormGroup>
                <Label for="company">Company Name/Organization</Label>
                <Input type="text" name="company" placeholder="company" value={this.state.company} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="country">Country</Label>
                <Input type="text" name="country" placeholder="United States" value={this.state.country} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="state">State</Label>
                <Input type="text" name="state" placeholder="Colorado" value={this.state.state} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="city">City</Label>
                <Input type="text" name="city" placeholder="Denver" value={this.state.city} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
            <div className="col-sm-6">
              <FormGroup>
                <Label for="website">Website</Label>
                <Input type="url" name="website" placeholder="https://" value={this.state.website} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="position">Position</Label>
                <Input type="text" name="position" placeholder="enter position" value={this.state.position} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="startDate">Starting from </Label>
                <Input type="date" name="startDate" placeholder="date placeholder" value={this.state.startDate} onChange={this.handleInputChange} />
              </FormGroup>
              <FormGroup>
                <Label for="endDate">Ending on</Label>
                <Input type="date" name="endDate" placeholder="date placeholder" value={this.state.endDate} onChange={this.handleInputChange} />
              </FormGroup>
            </div>
          </FormGroup>

          <FormGroup className="submission-wrapper">
            <div className="alert-wrapper">
              <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
              <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
            </div>
            <Button type="submit" className="add-experience-button" onClick={() => this.addExperience()}>Add Experience</Button>
          </FormGroup>
        </Form>


        <Table bordered className="work-table">
          <thead>
            <tr>
              <th>Company Name</th>
              <th>Website</th>
              <th>Country/City/State</th>
              <th>Starting/Ending</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.renderExperiences()}
          </tbody>
        </Table>

        <FormGroup className="submission-wrapper">
            <div className="alert-wrapper">
                <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
            </div>
            <Button type="submit">Save</Button>
        </FormGroup>
      </div>
    );
  }
}

export default WorkExperienceView;
