import React from 'react';
import { withCookies } from 'react-cookie';
import { graphql, compose, withApollo } from 'react-apollo';
import * as stream from 'getstream';

import TimelineWritePost from '../../components/TimelineWritePost';
import FollowingFollowers from '../../components/FollowingFollowers';
import TopUsers from '../../components/TopUsers';
import SponsoredAds from '../../components/SponsoredAds';
import TimelinePost from '../../components/TimelinePost';
import GetTimeline from '../../queries/GetTimeline';
import StreamTokens from '../../queries/StreamTokens';
import FollowersFollowing from '../../queries/FollowersFollowing';

class RecommendedPosts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            timelinePosts:  this.props.timelinePosts,
            apiKey: '',
            appId: 0
        };

        this.refreshTimeline = this.refreshTimeline.bind(this);
        this.posts = null;
        this.client = null;
        this.selfUser = null;
    }

    componentWillReceiveProps(newProps) {
        console.log('set state', this.setState, this.state)

        if(!newProps.timelineLoading && !newProps.timelineError && !newProps.tokenLoading && !newProps.tokenError && !this.client) {
            console.log(newProps)
            // Use these methods to listen for notification updates
            // Use the counter to display on notification icon
            // There is a separate endpoint in NotificationsView for retrieving list of notifications
            // This endpoint will trigger a mark_seen, mark_read event
            this.setState({
                apiKey: newProps.tokens.api,
                appId: newProps.tokens.appId
            })
            this.client = stream.connect(
                newProps.tokens.api,
                null,
                newProps.tokens.appId
            );
            var feed = this.client.feed(
                "timeline",
                newProps.tokens.userId,
                newProps.tokens.timeline
            );
            feed.subscribe((response) => {
                if(response.new.length) {
                    this.refreshTimeline()
                }
                console.log('feed update', response);
            }).then((success) => {
                console.log('success connection timeline notification', success)
            }, (error) => {
                console.log('error connection', error);
            })
            // on feed.get() opperation trigger {mark_seen:true} {mark_read:true} to clear log

        }

        if(!newProps.timelineLoading && !newProps.timelineError && !newProps.tokenLoading && !newProps.tokenError) {
            this.posts = newProps.timelinePosts.map((post, i) => <TimelinePost getstreamClient={this.client} post={post} key={i} />)
            this.forceUpdate()
        }
    }

    async refreshTimeline() {
        let res = await this.props.client.query({
            query:GetTimeline,
            variables: {
                username: this.props.username
            },
            fetchPolicy:'network-only'
        });
        this.posts = res.data.timeline.map((post, i) => <TimelinePost getstreamClient={this.client} post={post} key={i} />)
        this.forceUpdate()
    }

    render() {
        const { timelineLoading, timelineError, tokenLoading, tokenError, userLoading, userError } = this.props;
        const { timelinePosts } = this.state;
        if (timelineLoading || tokenLoading || userLoading) {
            return <h1>Loading</h1>
        } else if (timelineError || tokenError || userError) {
            return <h1>{timelineError || ''} {tokenError || ''} {userError || ''}</h1>
        }
        let posts = null;
        if(timelinePosts) {
            
        }

        return (
            <div className="TimelinePostsContainer">
                <div className="TimelinePosts">
                    {this.posts}
                </div>
            </div>
        );
    }
}

class TimelineView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.username = this.props.cookies.get('username')
        this.isLoggedIn = this.props.cookies.get('isLoggedIn')

        this.GetTimeline = compose(
            withApollo,
            graphql(GetTimeline, { 
                props: ({ownProps, data: { timeline, loading, error }}) => ({
                    ...ownProps,
                        timelinePosts: timeline,
                        timelineLoading: loading,
                        timelineError: error
                }),
                options: {variables: { username: props.cookies.get('username') }}
            }),
            graphql(StreamTokens, { 
                props: ({ownProps, data: { streamTokens, loading, error }}) => ({
                    ...ownProps,
                        tokens: streamTokens,
                        tokenLoading: loading,
                        tokenError: error
                }),
                options: {variables: { username: props.cookies.get('username') }}
            })
        )(RecommendedPosts);

        this.FollowingFollowers = graphql(FollowersFollowing, {
            props: ({ownProps, data: { followersFollowing, loading, error }}) => ({
                ...ownProps,
                    followersFollowing,
                    loading,
                    error
            }),
            options:{
                variables: {
                    username: props.cookies.get('username')
                }
            }
        })(FollowingFollowers);
    }

    render() {
        return (
            <div className="TimelineView">
                <div className="sidebar">
                    <this.FollowingFollowers />
                    <TopUsers />
                    <SponsoredAds />
                </div>

                <div className="page-content">
                    <TimelineWritePost />

                    <div className="recommended-wrapper">
                        <h3 className="heading">Recommended For You</h3>
                        <div className="recommended-posts">
                            <this.GetTimeline username={this.props.cookies.get('username')} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withCookies(TimelineView);
