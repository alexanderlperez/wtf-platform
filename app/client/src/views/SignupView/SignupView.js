import React from 'react';
import { Link } from 'react-router-dom';
import { 
    InputGroup, 
    InputGroupAddon, 
    InputGroupDropdown,
    InputGroupButtonDropdown, 
    Alert, 
    Button, 
    Form, 
    FormGroup, 
    Label, 
    Input, 
    FormText, 
    Dropdown, 
    DropdownToggle, 
    DropdownMenu, 
    DropdownItem
} from 'reactstrap';
import classNames from 'classnames';
import formEncoded from 'form-urlencoded';
import { graphql, compose, withApollo } from 'react-apollo';

import SiteLinks from '../../services/SiteLinks';
import Globals from '../../services/Globals';
import { professions } from '../../services/Constants';
import { countries, states } from '../../services/Locations';

import LogoFullBlack from '../../assets/images/wtf-logo-full-black.png';
import twitter from '../../assets/images/twitter.png';
import facebook from '../../assets/images/facebook.png';
import google from '../../assets/images/google.png';

import RecommendedUsers from '../../queries/RecommendedUsers';
import SubmitBasicInfo from '../../queries/SubmitBasicInfo';

import WrapWithModal from '../../components/WrapWithModal';
import Avatar from '../../components/Avatar';
import FollowButton from '../../components/FollowButton';

import TagsInput from 'react-tagsinput';



import FollowUser from '../../queries/FollowUser';
import UnfollowUser from '../../queries/UnfollowUser';


class RecommendedUsersView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recommendedUsers: [],
            username: this.props.username,
            state: this.props.state,
            country: this.props.country,
            profession: this.props.profession
        }

        this.client = this.props.client;

        this.FollowButton = compose(
            graphql(FollowUser, { name: 'followMutate' }),
            graphql(UnfollowUser, { name: 'unfollowMutate' })
        )(FollowButton);

        this.handleClick = this.handleClick.bind(this);
        this.refreshUsers = this.refreshUsers.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
    }

    async refreshUsers() {
        let users = await this.client.query({
            query: RecommendedUsers,
            fetchPolicy:'network-only',
            variables: {
                username: 'stuff',
                country: this.state.country.short,
                state: this.state.state.short,
                profession: this.state.profession
            }
        })
        console.log('what is users', users);
        this.setState({
            recommendedUsers: users.data.recommendedUsers
        })
    }

    componentWillReceiveProps(props) {
        console.log('this is props', props);
        const { loading, error, recommendedUsers } = props.data;

        if (loading) {
            return; 
        } else if (error) {
            return;
        }

        this.setState({
            recommendedUsers
        })
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        }, (prevState, props) => {
            this.refreshUsers();
        });
    }

    handleLocationChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(name=='country') {
            let country = countries.find(c => {
                return value == c.short;
            });
            this.setState({
                country: {
                    short: country.short,
                    name: country.name
                }
            }, (prevState, props) => {
                this.refreshUsers();
            });
        }
        if(name=='state') {
            let state = states.find(s => {
                return value == s.short && s.countryCode == this.state.country.short;
            });
            this.setState({
                state: {
                    short: state.short,
                    name: state.name
                }
            }, (prevState, props) => {
                this.refreshUsers();
            });
        }


    }

    handleClick() {
        this.props.onNext()
    }

    render() {
        const { loading, error } = this.props.data;
        
        if (loading) {
            return <div>Loading</div>
        } else if (error) {
            return <div>{error}</div>
        }

        let users = this.state.recommendedUsers.map((user, u) => {
            let projects = user.projects.map((project, p) => {
                return (
                    <img src={`${SiteLinks.uploads}/${project.coverImage}`} key={'p'+p} />
                )
            })

            return (
                <div className="user" key={'u'+u}>
                    <div className="user-details">
                        <div className="left">
                            <Avatar filename={user.profileImages.avatar}/>
                        </div>
                        <div className="right">
                            <h4 className="name">{user.contact.firstName} {user.contact.lastName}</h4>
                            <h5 className="meta">{user.contact.country ? user.contact.country.name : ''}, {user.contact.state ? user.contact.state.name : ''}</h5>
                            <this.FollowButton username={this.state.username} targetname={user.username} />
                        </div>
                    </div>
                    <div className="projects">
                        {projects}
                    </div>
                </div>
            )
        })

        let renderStates = this.state.country.short ?
            states.filter(s => {
                return s.countryCode == this.state.country.short
            }).map((s, i) => (<option key={s.short} value={s.short}>{s.name}</option>)) : ''

        return (
            <div className="RecommendedUsers">
                <h3 className="heading">Follow any 5 users to continue</h3>
                <hr />

                <div className="users">
                    {users}
                </div>
                <button className="next-button" onClick={this.handleClick}>Next</button>
            </div>
        )
    }
}

class MoreInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.username,
            city: '',
            state: {
                name:'',
                short:''
            },
            country: {
                name:'',
                short:''
            },
            zipcode:'',
            skills:[]
        }

        this.submitInfo = this.submitInfo.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSkillsChange = this.handleSkillsChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleLocationChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(name=='country') {
            let country = countries.find(c => {
                return value == c.short;
            });
            this.setState({
                country: {
                    short: country.short,
                    name: country.name
                }
            });
        }
        if(name=='state') {
            let state = states.find(s => {
                return value == s.short && s.countryCode == this.state.country.short;
            });
            this.setState({
                state: {
                    short: state.short,
                    name: state.name
                }
            });
        }


    }

    handleClick() {
        this.props.onNext(this.state.country, this.state.state)
    }

    handleSkillsChange(skills) {
        this.setState({skills})
    }

    async submitInfo() {
        const { mutate } = this.props;

        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.state.username,
                    set: {
                        contact: {
                            city:this.state.city,
                            state:this.state.state,
                            country: this.state.country,
                            zipcode: this.state.zipcode
                        },
                        skills: this.state.skills
                    }
                }
            })
            this.handleClick();
        } catch (err) {
            console.log(err);

            return;
        }
    }

    render() {

        let renderStates = this.state.country.short ?
                states.filter(s => {
                    return s.countryCode == this.state.country.short
                }).map((s, i) => (<option key={s.short} value={s.short}>{s.name}</option>)) : ''

        return (
            <div className="RecommendedUsers">
                <h3>Please tell us about yourself</h3>
                <div className="row">
                    <div className="col-md-12">
                        <FormGroup>
                            <Input type="select" name="country" value={this.state.country.short} onChange={this.handleLocationChange}>
                                <option value="">Select Country</option>
                                {countries.map((c, i) => (<option key={c.short} value={c.short}>{c.name}</option>))}
                            </Input>
                        </FormGroup>
                    </div>
                    <div className="col-md-12">
                        <FormGroup>
                            <Input type="select" name="state" value={this.state.state.short} onChange={this.handleLocationChange}>
                                <option value="">Select State</option>
                                {renderStates}
                            </Input>
                        </FormGroup>
                    </div>
                    <div className="col-md-12">
                        <FormGroup>
                            <Input type="text" value={this.state.city} onChange={this.handleInputChange} name="city" placeholder="City" />
                        </FormGroup>
                    </div>
                    <div className="col-md-12">
                        <FormGroup>
                            <Input type="text" value={this.state.zipcode} onChange={this.handleInputChange} name="zipcode" placeholder="Postal Code" />
                        </FormGroup>
                    </div>
                    <div className="col-md-12">
                        <FormGroup>
                            <Label>Skills</Label>
                            <TagsInput value={this.state.skills} onChange={this.handleSkillsChange} />
                        </FormGroup>
                    </div>
                </div>
                <button className="next-button" onClick={this.submitInfo}>Next</button>
            </div>
        )
    }
}

class SignupView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            email: '',
            username: '',
            password: '',
            profession: '',
            state: {
                name:'',
                short:''
            },
            country: {
                name:'',
                short:''
            },
            terms: false,
            isRegisterError: false,
            isNetworkError: false,
            isServerError: false, 
            error:'',
            openRecommended: false,
            openMoreInfo: false
        }

        this.RecommendedUsersView = compose(
            withApollo,
            graphql(RecommendedUsers, {
                options: {
                    variables: {
                        username: 'stuff',
                        profession: this.state.profession,
                        country: this.state.country.short,
                        state: this.state.state.short
                    }
                }
            })
        )(RecommendedUsersView)

        this.MoreInfo = graphql(SubmitBasicInfo)(MoreInfo);
        

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.onRecommendedUsersNext = this.onRecommendedUsersNext.bind(this);
        this.onMoreInfoNext = this.onMoreInfoNext.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submitForm(e) {
        e.preventDefault();
        const { fullname, email, username, password, profession, terms } = this.state;

        const options = {
            method: 'POST',
            body: formEncoded({
                fullname,
                email,
                username,
                password,
                profession,
                terms
            }),
            headers: {
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            credentials: "same-origin"
        }

        fetch('/register', options)
            .catch((err) => {
                this.setState({ isNetworkError: true })
                return Promise.reject(); 
            })
            .then(res => {
                // reset the messages
                this.setState({
                    isRegisterError: false,
                    isNetworkError: false,
                    isServerError: false, 
                })

                if (res.status === 500) {
                    this.setState({
                        isServerError: true
                    })
                    return Promise.reject(); 
                }

                if (res.status !== 200) {
                    this.setState({
                        isRegisterError: true
                    })
                    if(res.error) {
                        this.setState({
                            error: res.error
                        })
                    }
                    return Promise.reject();
                }

                return Promise.resolve(res);
            })
            .then(res => {
                // things went fine
                Globals.set('username', username);

                this.setState({
                    // openMoreInfo: true
                    openRecommended: true,
                })

                /*setTimeout(() => {
                    window.location.href = `${SiteLinks.account}/${username}`
                }, 1000)*/
            })
    }

    onRecommendedUsersNext() {
        setTimeout(() => {
            window.location.href = `${SiteLinks.account}/${this.state.username}`
        }, 1000)
    }

    onMoreInfoNext(country, state) {
        this.setState({
            openMoreInfo: false,
            openRecommended: true,
            country: country,
            state: state
        })
    }

    render() {

        const recUsers = <this.RecommendedUsersView username={this.state.username} country={this.state.country} state={this.state.state} profession={this.state.profession} onNext={this.onRecommendedUsersNext} />
        const moreInfo = <this.MoreInfo username={this.state.username} onNext={this.onMoreInfoNext} />

        return (
            <div className="LoginView SignupView">
                <div className="left">
                    <div className="content-wrapper">
                        <div className="form-wrapper">
                            <div className="title">
                                <div className="logo">
                                    <img src={LogoFullBlack} alt="" />
                                </div>
                                <div className="tagline">World’s Largest Fashion Network </div>
                            </div>
                        
                            <Alert color="danger" isOpen={ this.state.isRegisterError }>Make sure all the fields are filled out and checked.</Alert>
                            <Alert color="danger" isOpen={ this.state.isNetworkError }>There was an error with the network.  Please try again later.</Alert>
                            <Alert color="danger" isOpen={ this.state.isServerError }>There was an error with the server.  Please try again later.</Alert>

                            <Form className="login-form" onSubmit={this.submitForm}>
                                <FormGroup>
                                    <Input type="text" value={this.state.username} onChange={this.handleInputChange} name="username" placeholder="Username" />
                                </FormGroup>

                                <FormGroup>
                                    <Input value={this.state.password} onChange={this.handleInputChange} type="password" name="password" placeholder="Password"/>
                                </FormGroup>

                                <FormGroup>
                                    <Input value={this.state.terms} onChange={this.handleInputChange} type="checkbox" name="terms" /> I have read and agree to the <Link to="/toc">T&amp;C</Link> and <Link to="/privacy">Privacy Policy</Link>
                                </FormGroup>

                                <FormGroup>
                                    <Input className="submit" type="submit" value="Sign Up"/>
                                </FormGroup>
                            </Form>
                        </div>
                        
                        <div className="alternate-options">
                            <div className="divider"><span className="line"></span><span className="text">or log in with</span><span className="line"></span></div>
                            <div className="social-options">
                                <img src={twitter} alt="" />
                                <img src={facebook} alt="" />
                                <img src={google} alt="" />
                            </div>
                            <div className="register">
                                <Link to="/login">I already have an account</Link>
                            </div>
                        </div>
                    </div>

                    <div className="copyright">
                        &copy; WTF 2016, All Rights Reserved
                    </div>
                </div>

                <div className="right"></div>

                <WrapWithModal className="registrationModal" isOpen={this.state.openMoreInfo} toggle={() => {}} children={moreInfo} />
                <WrapWithModal className="registrationModal" isOpen={this.state.openRecommended} toggle={() => {}} children={recUsers} />
            </div>        
        );
    }
}

export default SignupView;
