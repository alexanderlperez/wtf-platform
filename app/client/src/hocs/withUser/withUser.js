import React from 'react';
import { graphql } from 'react-apollo';
import User from '../../queries/User';

const withUser = (username, isFollowingUser) => {
    const gqlHoc = graphql(User, {options: {variables: { username, amIFollowing: isFollowingUser }}});

    const Render = ({ render, data, ...props }) => {
        const { loading, error } = data;

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { user } = data;

        return render(user)
    }


    return gqlHoc(Render);
}

export default withUser;
