import React from 'react';
import { Button, FormGroup, Label, Input } from 'reactstrap'
import { Fullpage, Slide } from 'fullpage-react';
import searchGlass from '../../assets/images/searchglass.svg';
import wtfLogoBlack from '../../assets/images/wtf-logo-full-black.png';
import wtfLogoWhite from '../../assets/images/wtf-logo-full-white.png';
const { changeFullpageSlide, changeHorizontalSlide } = Fullpage;

const SearchBar = (props) => {
    return(
        <div className="SearchBar">
            <FormGroup className="search-bar-container">
                <img className={(props.activeSlide === 0 ? 'white' : 'black') + " logo"} src={( props.activeSlide === 0 ? wtfLogoWhite : wtfLogoBlack)} />

                <div className="input-wrapper">
                    <Input className={(props.activeSlide === 0 ? 'search-bar' : 'search-bar-black')} type="search" name="search" placeholder="Search the industry" />
                    <img className="search-glass-icon" src={searchGlass} alt="" />
                </div>
            </FormGroup>

            <div className="login">
                <a href="/login">Login</a>
            </div>
        </div>
    )
}


const NavigationDots = (props) => {
    return(
        <div className="dot-navigation-bar">

            <div onClick={ changeSlide(0) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 0 ? 'active-white' : '')}/>
            <div onClick={ changeSlide(1) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 1 ? 'active' : '')}/>
            <div onClick={ changeSlide(2) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 2 ? 'active' : '')}/>
            <div onClick={ changeSlide(3) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 3 ? 'active' : '')}/>
            <div onClick={ changeSlide(4) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 4 ? 'active' : '')}/>
            <div onClick={ changeSlide(5) } className={"dot " + (props.activeSlide === 0 ? 'dot-color-white' : 'dot-color-black') + " " + (props.activeSlide === 5 ? 'active' : '')}/>
        </div>
    )
};


const changeSlide = (argument) => changeFullpageSlide.bind(null, argument);

const FixedFooter = (props) => {
    return (
        <div className="footer-image" onClick={ changeSlide(props.nextSlide) }>
            <div className="footer-container">
                <div className="footer-text">How do I work this?</div>
            </div>
        </div>
    );
};

const FirstSlide = (props) => {
    return(
        <div className="first-slide-component">
            <div className="wrapper">
                <div className="title-text">
                    <span className="hashtag">#</span>
                    <span className="we-the-fashion">WeTheFashion</span>
                </div>
                <div className="second-text-container">
                    <h2 className="get-closer-text">Get closer to your industry</h2>
                </div>
                <Button>Join now</Button>
            </div>
        </div>
    )
}

const SecondSlide = (props) => {
    return(
        <div className="landing-page-component">
            <div className="page-header-text">
                Want to build your portfolio?
            </div>
            <div className="description-text">
                Build your professional profile at WeTheFashion by uploading
                best of  your designs and most current and accurate information
                to put your best professional foot forward globally.
            </div>
            <FixedFooter nextSlide={ 2 } />
        </div>
    )
}

const ThirdSlide = (props) => {
    return(
        <div className="profile-landing-page">
            <div className="profile-landing-page-header-text">
                Looking for some inspiration on trends?
            </div>
            <div className="profile-landing-page-description-text">
                Fill your beans of creativity by discovering latest trends and technology and further upgrade your knowledge.
            </div>
            <FixedFooter nextSlide={ 3 } />
        </div>
    )
}

const FourthSlide = (props) => {
    return(
        <div className="profile-landing-page">
            <div className="profile-landing-page-header-text">
                Looking for some relevant job opportunities?
            </div>
            <div className="profile-landing-page-description-text">
                WeTheFashion provides filtered jobs to the
                students/professionals relevant to their skills and also allow employers to post jobs and hire the right candidate for FREE.
            </div>
            <FixedFooter nextSlide={ 4 } />
        </div>
    )
}

const FifthSlide = (props) => {
    return(
        <div className="profile-landing-page">
            <div className="profile-landing-page-header-text">
                Looking for networking?
            </div>
            <div className="profile-landing-page-description-text">
                Enhance your Social Influence. Connect with fellow designer from all over the world, learn & collaborate with each other. Follow and stay in touch with prospective employers, colleagues, buyers & more.
            </div>
            <FixedFooter nextSlide={ 5 } />
        </div>
    )
}

const SixthSlide = (props) => {
    return(
        <div className="profile-landing-page">
            <div className="profile-landing-page-header-text">
                Show what you can do
            </div>
            <div className="profile-landing-page-description-text">
                Your profile is where you can shine
            </div>
        </div>
    )
}

class HomePageLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            slide: 0,
            activeSlide: 0,
        };

        this.fullPageOptions = {
            scrollSensitivity: 7,
            touchSensitivity: 7,
            scrollSpeed: 500,
            hideScrollBars: true,
            easing: 'easeInExpo',// TODO: update fullpage-react for pluggable easing and submit pull-request
            enableArrowKeys: true,
            onSlideChangeEnd: this.setCurrentSlide,

        };

        const slides = [
            <Slide className="first"> <FirstSlide /> </Slide>,
            <Slide className="second"> <SecondSlide /> </Slide>,
            <Slide className="third"> <ThirdSlide /> </Slide>,
            <Slide className="fourth"> <FourthSlide /> </Slide>,
            <Slide className="fifth"> <FifthSlide /> </Slide>,
            <Slide className="sixth"> <SixthSlide /> </Slide>
        ]

        this.fullPageOptions.slides = slides;

        this.onSlideChangeStart = this.onSlideChangeStart.bind(this);
        this.onSlideChangeEnd = this.onSlideChangeEnd.bind(this);
    }

    onSlideChangeStart(name, props, state, newState)  {
        console.log("Slide Change Start")
        console.log(newState.activeSlide)
        this.setState({activeSlide: newState.activeSlide})
    }

    onSlideChangeEnd(name, props, state, newState) {
        // console.log("ggggg")
        // const oldActive = this.state.slide;
        // const sliderState = {
        //     [name]: newState.activeSlide
        // };

        // const updatedState = Object.assign(oldActive, sliderState);
        // this.setState(updatedState);
        // console.log("-----------")
    }
    //

    setCurrentSlide() {
        // TODO: 
        // - track active slide to handle correct animations
    }

    render() {
        return (
            <div className="HomePageLayout">
                <NavigationDots activeSlide={this.state.activeSlide} />
                <SearchBar activeSlide={this.state.activeSlide} />
                <Fullpage onSlideChangeStart={this.onSlideChangeStart} onSlideChangeEnd={this.onSlideChangeEnd} {...this.fullPageOptions} />
            </div>
        );
    }
}

export default HomePageLayout;
