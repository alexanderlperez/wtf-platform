import React from 'react';
import PropTypes from 'prop-types';
import {Route, Switch} from 'react-router-dom';

import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';
import { withCookies } from 'react-cookie';

import ProjectUploadsLayout from '../../layouts/ProjectUploadsLayout';
import LookbookUploadsLayout from '../../layouts/LookbookUploadsLayout';

import AboutView from '../../views/AboutView';
import TimelineView from '../../views/TimelineView';
import LookbookView from '../../views/LookbookView';
import VideosView from '../../views/VideosView';
import FavoritesView from '../../views/FavoritesView';
import PortfolioView from '../../views/PortfolioView';

import ProfileBanner from '../../components/ProfileBanner';
import ProfileNav from '../../components/ProfileNav';
import ProfileContent from '../../components/ProfileContent';

import AllProjects from '../../queries/AllProjects';
import User from '../../queries/User';
import UserLookbook from '../../queries/UserLookbook';

class UserLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: props.match.params.username
        };

        this.UserProfileBanner = graphql(User, {options: {variables: {username: this.state.username, amIFollowing: this.props.cookies.get('username') }}})(ProfileBanner);
        this.UserProfileNav = graphql(User, {options: {variables: {username: this.state.username, amIFollowing: this.props.cookies.get('username')}}})(ProfileNav);

        this.UserLookbook = compose(
            graphql(UserLookbook, {options: {variables: {username: this.state.username}, fetchPolicy: 'network-only'}, name: 'Lookbook' }),
            graphql(User, {options: {variables: {username: this.state.username, amIFollowing: this.props.cookies.get('username')}}, name: 'User'}),
        )(LookbookView);

        this.UserPortfolio = compose(
            graphql(AllProjects, { options: {variables: {username: this.state.username}, fetchPolicy: 'network-only'}, name: 'AllProjects' }),
            graphql(User, { options: {variables: {username: this.state.username, amIFollowing: this.props.cookies.get('username')}}, name: 'User' })
        )(PortfolioView);
    }

    render() {
        return (
            <div className="UserLayout">
                <this.UserProfileBanner user={this.state.user} />

                <ProfileContent>
                    <this.UserProfileNav accountUrl={this.props.match.url} />

                    <div className="content-wrapper">
                        <Switch>
                            <Route from={`${this.props.match.url}/timeline`} component={TimelineView}></Route>
                            <Route from={`${this.props.match.url}/lookbook/upload`} component={LookbookUploadsLayout}></Route>
                            <Route from={`${this.props.match.url}/lookbook`} component={this.UserLookbook}></Route>
                            <Route from={`${this.props.match.url}/portfolio/upload`} component={ProjectUploadsLayout}></Route>
                            <Route from={`${this.props.match.url}/portfolio/:projectId`} component={this.UserPortfolio}></Route>
                            <Route from={`${this.props.match.url}/portfolio`} component={this.UserPortfolio}></Route>
                            <Route from={`${this.props.match.url}/videos`} component={VideosView}></Route>
                            <Route from={`${this.props.match.url}/favorites`} component={FavoritesView}></Route>
                            <Route from={`${this.props.match.url}/about`} component={AboutView}></Route>
                            <Route from={`${this.props.match.url}`} component={TimelineView}></Route>
                        </Switch>
                    </div>
                </ProfileContent>
            </div>
        );
    }
}

UserLayout.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            username: PropTypes.string.isRequired
        })
    }),
};

export default withCookies(UserLayout);
