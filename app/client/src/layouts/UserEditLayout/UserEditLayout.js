import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, NavLink, Route, Switch } from 'react-router-dom';
import { Nav, NavItem} from 'reactstrap';

import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import BasicInformationView from '../../views/BasicInformationView';
import AboutMeView from '../../views/AboutMeView';
import SocialLinksView from '../../views/SocialLinksView';
import WorkExperienceView from '../../views/WorkExperienceView';
import SkillsView from '../../views/SkillsView';
import AwardsView from '../../views/AwardsView';
import ResumeUploadView from '../../views/ResumeUploadView';
// import NotificationView from '../../views/NotificationView'; //TODO: get back the checkbox view from peter-profile-build branch
import ChangePasswordView from '../../views/ChangePasswordView';
import SidebarNav from '../../components/SidebarNav';
import { clean } from '../../services/URL';


class UserEditLayout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};
	}

	render() {
		const { url, params: { username } } = this.props.match;

		return (
            <div className="UserEditLayout">
                <SidebarNav url={url} />

                <div className="content-wrapper">
                    <Switch>
                        { /* redirect default /edit route to BasicInformation */ }
                        <Route exact path={(`${url}`)} render={() => <Redirect to={clean(`${url}/basicinformation`)} />}></Route>

                        { /* UserEdit routes */ }
                        <Route from={`${url}/basicinformation`} component={() => <BasicInformationView username={username}/>}></Route>
                        <Route from={`${url}/aboutme`} component={AboutMeView}></Route>
                        <Route from={`${url}/sociallinks`} component={SocialLinksView}></Route>
                        <Route from={`${url}/workexperience`} component={WorkExperienceView}></Route>
                        <Route from={`${url}/skills`} component={SkillsView}></Route>
                        <Route from={`${url}/awards`} component={AwardsView}></Route>
                        <Route from={`${url}/resumeupload`} component={() => <ResumeUploadView username={username}/>}></Route>
                        {
                            /*
                            *<Route from={`${url}/notification`} component={NotificationView}></Route>
                            */
                            }
                        <Route from={`${url}/changepassword`} component={ChangePasswordView}></Route>
                    </Switch>
                </div>
            </div>
		);
	}
}

export default UserEditLayout;
