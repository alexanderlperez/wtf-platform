import React from 'react';
import {Route, Switch} from 'react-router-dom';

import ProjectUploadsView from '../../views/ProjectUploadsView';
import ProjectPreviewView from '../../views/ProjectPreviewView';

// NOTE: The goal of the layout is to 
// - handle routing for a specific context of ui, functionality
// - be responsible for the placement of views

class ProjectUploadsLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        const url = this.props.match.url;

        return (
            <div className="ProjectUploadsLayout">
                <Switch>
                    <Route from={`${url}/preview`} component={ProjectPreviewView}></Route>
                    <Route from={`${url}`} component={ProjectUploadsView}></Route>
                </Switch>
            </div>
        );
    }
}

export default ProjectUploadsLayout;
