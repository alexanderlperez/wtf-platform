export let prefixHash = tags => tags.map(tag => `#${tag} `); 

// keep titles and descriptions to a max length, with ellipses
export let clampString = (string, max=240) => {
    return string.length > max 
        ? string.slice(0, max) + '...'
        : string
}

// adds default country code
export let addCountryCode = (string, countryCode='1') => {
    return string.length < 11
        ? `${countryCode}${string}`
        : string
}
