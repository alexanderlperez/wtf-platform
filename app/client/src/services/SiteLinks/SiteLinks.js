// This service defines the canonical uris to site pages
function assignLinks(dest, base, links) {
    Object.entries(links).forEach(entry => dest[entry[0]] = base + entry[1]);
}

class SiteLinks {
    // TODO: needs to take into account usernames in urls
    constructor() {
        // initialize root-level uris
        const baseLinks = {
            discover: '/discover',
            account: '/account',
            logout: '/logout',
            jobs: '/jobs',
            lookbooks: '/lookbooks',
            blog: '/blog',
            uploads: '/uploads',
            messages: '/messages'
        };

        assignLinks(this, '', baseLinks);

        // messaging system pages
        // user account pages
        const messagePages = {
            notifications: '/notifications'
        };

        assignLinks(this, baseLinks.messages, messagePages);

        // user account pages
        const userAccountPages = {
            joblist: '/joblist',
            portfolio: '/portfolio'
        };

        assignLinks(this, baseLinks.account, userAccountPages);

        // profile uploads
        const profileUploads = {
            profileUploads: '/profile'
        };

        assignLinks(this, baseLinks.uploads, profileUploads);

        // discover categories
        const discoverCategories = {
            popular: '/popular',
            fresh: '/fresh',
            brand: '/brand',
        }

        assignLinks(this, baseLinks.discover, discoverCategories);
    }
}


export default new SiteLinks();
export let toUploadUrl = (filename) => {
    if (!filename) {
        return "";
    }

    // skip transformation if it's an amazon url
    if (filename.indexOf('amazonaws') > -1) {
        return filename;
    }

    return (new SiteLinks()).uploads + '/' + filename;
}

