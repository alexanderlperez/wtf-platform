export let renderLocation =  ({city, state, country}) => {
    if (state && country && state.name && state.country) {
        return `${state.name}, ${country.name}`;
    }

    if (city && country && country.name) {
        return `${city}, ${country.name}`;
    }

    if (city && state && state.name) {
        return `${city}, ${state.name}`;
    }
    return '';
};
