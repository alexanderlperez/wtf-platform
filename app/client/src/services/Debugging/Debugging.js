export let showChangedProps = (oldProps, newProps) => {
    for (const index in newProps) {
        if (newProps[index] !== oldProps[index]) {
            console.log(index, oldProps[index], '-->', newProps[index]);
        }
    }
}

