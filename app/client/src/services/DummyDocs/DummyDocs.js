export let exampleUser = { 
    "_id" : "5a90744baae8e8297692b5de", 
    "register_id" : 10, 
    "username" : "slatch", 
    "password" : "912EC803B2CE49E4A541068D495AB570", 
    "created_on" : "2016-07-04T23:46:26.000+0000", 
    "type" : "NU", 
    "register_for" : "RB", 
    "subscribed" : "FREE", 
    "profile_url" : "https://wethefashion.com/slatch", 
    "changed_username" : "Y", 
    "about" : {
        "title" : "About Me", 
        "description" : "<p>WeTheFashion is a portfolio and Fashion discovery website.</p>", 
        "uploadResumePath" : null, 
        "uploadBrochuresPath" : "0", 
        "gender" : "Male", 
        "birthdate" : "1983-12-30T07:00:00.000+0000"
    }, 
    "contact" : {
        "firstName" : "Tarranjeet", 
        "lastName" : "Slatch", 
        "phone" : "9910003662", 
        "email" : "taranjeetslatch@gmail.com", 
        "personalDetail" : "0", 
        "websiteUrl" : "https://macslatch.com", 
        "gender" : "Male", 
        "zipcode" : "110034", 
        "city" : "New Delhi", 
        "country" : "India", 
        "state" : "Delhi"
    }, 
    "language" : {
        "name" : "English", 
        "proficiency" : "Advanced"
    }, 
    "accountMeta" : {
        "changedUsername" : "Y", 
        "createdOn" : "2016-07-04T23:46:26.000+0000", 
        "memberType" : "FB", 
        "profileUrl" : null, 
        "registerFor" : "RB", 
        "registerType" : "FREE"
    }, 
    "profession" : {
        "name" : "Fashionista"
    }, 
    "avatar" : "26849c4080fe1cb2f6d24fdd50e6750f.JPG", 
    "following" : [
    ], 
    "timeline" : [
    ], 
    "stats" : {
        "views" : 59, 
        "favorites" : 13
    }
}

export let exampleTimelineActivity = { 
    "_id" : "5a91a915b835d000e96aecbe", 
    "user" : "5a90744baae8e8297692b930", 
    "content" : "test", 
    "cover" : "", 
    "likes" : [ ], 
    "comments" : [ ], 
    "likesTotal": 0,
    "commentsTotal": 0,
    "createdAt" : "2018-02-24T18:04:05.143+0000", 
    "updatedAt" : "2018-02-24T18:04:05.143+0000", 
    "__v" : 0
}

export let exampleProject = { 
    "cover_image" : "6bd0dee0f6a8c321b3f7236b6e503b75.jpg", 
    "editor_choice" : "N", 
    "moveContracts" : null, 
    "description" : "Fashion and Fashion with Team", 
    "project_for_users" : null, 
    "project_metadata" : null, 
    "project_privacy" : "PASSWORD", 
    "tags" : [
        "Fashion", 
        "New", 
        "Show"
    ], 
    "title" : "Fashion", 
    "createdOn" : "2016-07-16T18:29:13.000+0000", 
    "views" : 94, 
    "category" : {
        "categoryId" : 6, 
        "categoryName" : "Fashion photography"
    }, 
    "images" : [
        {
            "id" : 481, 
            "description" : "Some designers have attempted to modernize the style and presentation of fashion shows by integrating technological advances in experimental ways, such as including pre-recorded digital videos as backdrops444", 
            "filename" : "700a83f58a6544d238890a0c4122cab9.jpg"
        }, 
        {
            "id" : 482, 
            "description" : "Some designers have attempted to modernize the style and presentation of fashion shows by integrating technological advances in experimental ways, such as including pre-recorded digital videos as backdrops333", 
            "filename" : "cbd284fa752c51e14a5cfe14a9bdc68c.jpg"
        }, 
        {
            "id" : 483, 
            "description" : "Some designers have attempted to modernize the style and presentation of fashion shows by integrating technological advances in experimental ways, such as including pre-recorded digital videos as backdrops222", 
            "filename" : "34d8203d76bded3b9ee649badbadf60a.jpg"
        }, 
        {
            "id" : 484, 
            "description" : "Some designers have attempted to modernize the style and presentation of fashion shows by integrating technological advances in experimental ways, such as including pre-recorded digital videos as backdrops111", 
            "filename" : "4a3bdb80958aa3ba758d16437624ed36.jpg"
        }
    ], 
    "comments" : [

    ], 
    "stats" : {
        "likes" : [
            {
                "registerId" : 126, 
                "date" : "2016-07-16T18:32:38.000+0000"
            }
        ], 
        "favorites" : [

        ], 
        "shares" : [

        ]
    }
}

