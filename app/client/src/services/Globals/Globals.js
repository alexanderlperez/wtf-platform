// NOTE: in case you're wondering: this is encapsuled in a class to protect against breaking callers 
// - use sparingly, if ever

class Globals {
    constructor() {
        console.log("I see you're using Globals... make sure there's a GREAT reason for this...");
    }

    get(key) {
        return localStorage.getItem(key);
    }

    set (key, value) {
        localStorage.setItem(key, value);
    }
}

export default new Globals();

