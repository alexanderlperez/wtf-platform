import moment from 'moment';

const formats = {
    notification: 'D MMM YYYY, H:mm A',
    post: 'MMMM, YYYY',
}

/**
* formatting dates like "5 hrs ago"
*/
export let postedDate = date => moment(date).fromNow();

export let prettyDate = (date, format=formats.notification) => moment(date).format(format)

export { formats };
