import React from 'react';

const WAIT_INTERVAL = 1000;
const ENTER_KEY = 13;

class WithInputTimeout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.triggerChange = this.triggerChange.bind(this);
    }

    componentWillMount() {
        this.timer = null;
    }

    handleKeyDown(e) {
        if (e.keyCode == ENTER_KEY) {
            clearTimeout(this.timer)
            this.triggerChange();
        }
    }

    handleChange(e) {
        const value = e.target.value;

        clearTimeout(this.timer);
        this.setState({ value })
        this.timer = setTimeout(this.triggerChange, WAIT_INTERVAL)
    }

    triggerChange() {
        const { value } = this.state;
        this.props.changeHandler(value)
    }

    render() {
        return (
            <div className="WithInputTimeout">
                {this.props.render({
                    value: this.state.value,
                    handleChange: this.handleChange,
                    handleKeyDown: this.handleKeyDown,
                })}
            </div>
        );
    }
}

export default WithInputTimeout;
