import React from 'react';
import {Button} from 'reactstrap';
import { graphql, compose } from 'react-apollo';

class FollowConnectButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            following: this.props.following
        }

        this.onFollowClick = this.onFollowClick.bind(this);
        this.onUnfollowClick = this.onUnfollowClick.bind(this);
    }

    componentWillReceiveProps({ following }) {
        this.setState({
            following
        })
    }

    async onFollowClick() {
        //TODO: perform the steps needed for a "connection"

        const { followMutate } = this.props;
        
        let res;
        try {
            res = await followMutate({ variables: { username: this.props.username, targetname: this.props.targetname } })
            this.setState({
                following:true
            });
        } catch (err) {
            return;
        }

        this.props.handleFollowing({following: true});
    }

    async onUnfollowClick() {
        //TODO: perform the steps needed for a "remove connection"

        const { unfollowMutate } = this.props;
        
        let res;
        try {
            res = await unfollowMutate({ variables: { username: this.props.username, targetname: this.props.targetname } })
            this.setState({
                following:false
            });
        } catch (err) {
            return;
        }

        this.props.handleFollowing({following: false});
    }

    render() {
        const { loading, error } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { 
            color,
            following,
            followsBack,
        } = this.props;

        const connect = <Button onClick={this.onFollowClick} color={color || "danger"} className="FollowConnectButton">Connect</Button> 
        const pending = <Button color={color || "danger"} className="FollowConnectButton">Pending</Button>
        const remove = <Button onClick={this.onUnfollowClick} color={color || "danger"} className="FollowConnectButton">Remove</Button> 

        if (!following) {
            return connect;
        } else if (following && !followsBack) {
            return connect;
        } else if (following && followsBack) {
            return remove;
        } else if (false) {
            // TODO: handle "Pending" conditions first and introduce into component
        }

        // default
        return connect;
    }
}

export default FollowConnectButton
