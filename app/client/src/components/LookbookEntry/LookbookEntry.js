import React from 'react';
import SiteLinks, { toUploadUrl } from '../../services/SiteLinks';
import { clampString } from '../../services/Text';
import { Media } from 'reactstrap';
import Avatar from '../../components/Avatar';
import { Input } from 'reactstrap';
import classNames from 'classnames';       

const SetEntry = (props) => {
    return (
        <div className="SetEntry">
            <input type="checkbox" />
            <div className="title"></div>
            <span className="visibility"></span>
        </div>
    );
}

class LookbookEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAddToOpen: false
        }

        this.toggleAddTo = this.toggleAddTo.bind(this);
    }

    toggleAddTo(e) {
        // TODO:  change this silly bs to use refs

        // need to resort to hacky parent access because react-stonecutter's elements are inaccessible
        // for creating events. z-indexes of the parent "li" need to be modified to have the add-to popover
        // be visible above other react-stonecutter elements
        const parentLi = e.target.parentNode.parentNode.parentNode.parentNode;

        const self = this;

        this.setState((prev, props) => {
            const wasOpen = prev.isAddToOpen;

            if (!wasOpen) {
                // is open now, up parent's z-index
                parentLi.style.zIndex += 10;

                // handle clicking outside of the add-to popover to close it
                document.addEventListener('click', function (e) {
                    parentLi.style.zIndex -= 10;

                    self.setState({
                        isAddToOpen: false
                    })
                }, { once: true })
            } else {
                // is now closing, bring z-index back down
                parentLi.style.zIndex -= 10;
            }

            return Object.assign(prev, {
                isAddToOpen: !wasOpen
            })
        })
    }

    render () {
        // TODO: temp, ensure const
        let { likes, profession, fullname, avatar, clickHandler, description, tags, title, coverImage } = this.props;

        console.log('props', this.props);

        if (!profession) {
            profession = '';
        }

        return (
            <div className="LookbookEntry">
                <div className="entry-heading">
                    <Media>
                        <div className="content-wrapper">
                            <Media left href="#">
                                <Avatar filename={avatar}/>
                            </Media>
                            <Media body>
                                <Media heading>
                                    <div className="heading-wrapper">
                                        <div className="fullname">
                                            {fullname}
                                            <div className="online-wrapper"></div>
                                        </div>
                                        <span className="profession">#{profession}</span>
                                    </div>
        
                                    <span className="stats">
                                        <i className="far fa-heart"></i>{Array.isArray(likes) ? likes.length : likes}
                                    </span>
                                </Media>
                            </Media>
                        </div>
                    </Media>
        
                    <hr />
        
                    <div className="title">{title}</div>
                </div>
        
                <div className="overlay-wrapper" onClick={() => clickHandler(this.props)}>
                    <img src={toUploadUrl(coverImage)} className="entry-image" />
                    <div className={classNames('overlay', { active: this.state.isAddToOpen })}>
                        <i className="far fa-heart"></i>
                        <i className="fas fa-plus-circle" onClick={this.toggleAddTo}></i>
                        <i className="fas fa-share-square"></i>
                    </div>
                </div>
        
                <div className={classNames('add-to-wrapper', { 'hidden': !this.state.isAddToOpen })}>
                    <div className="heading">Add To</div>
                    <div className="search">
                        <Input />
                    </div>
                    <div className="sets-list">
                        <SetEntry />
                    </div>
                    <div className="new-set">
                        <span>Create New Set</span>
                    </div>
                </div>
            </div>
        ); 
    }
}

export default LookbookEntry;
