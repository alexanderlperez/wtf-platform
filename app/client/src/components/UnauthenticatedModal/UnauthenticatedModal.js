import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class UnauthenticatedModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: true
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.state.modal} fade={false} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>You need to have an account to see the res of this</ModalHeader>
                    <ModalBody>
                        Create an account with WeTheFashion.
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

export default UnauthenticatedModal;
