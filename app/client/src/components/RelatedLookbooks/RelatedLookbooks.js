import React from 'react';
import SiteLinks from '../../services/SiteLinks';
import times from 'lodash.times';
import ProjectEntry from '../../components/ProjectEntry';
import RelatedLookbooksGrid from '../../components/RelatedLookbooksGrid';

// NOTE: this component expects to be wrapped w/ graphql(...)

const RelatedLookbooks = (props) => {
    const { username, handler } = props;
    const { loading, error, getRelatedLookbooks: lookbooks } = props.data;
    
    if (loading) {
        return <div></div>
    } else if (error) {
        return <div>{error}</div>
    }

    return (
        <div className="RelatedProjects">
            <div className="heading">Related Lookbooks</div>
            <hr />
            <div className="content-wrapper">
                <RelatedLookbooksGrid lookbooks={lookbooks} handler={handler} username={username} />
            </div>
        </div>
    );
};

export default RelatedLookbooks;
