import React from 'react';
import proBadgeImg from '../../assets/images/pro-badge.png';
import proBadgeImgMed from '../../assets/images/pro-badge-small.png';
import classNames from 'classnames';

const ProBadge = ({ size, isVisible, proBadge }) => {
    const isSmall = size === 'small';
    const relBadgeImg = (size) => {
        let badgeImg;

        if (!size) {
            badgeImg = proBadgeImg;
        }

        if (size == 'medium') {
            badgeImg = proBadgeImgMed;
        }

        return <img src={badgeImg} />;
    }

    return (
        <div className={classNames('ProBadge', { 'small': isSmall, 'is-visible': isVisible })}>
            { isSmall && '#' }
            { !isSmall && relBadgeImg(size)}
        </div>
    )
}

export default ProBadge;
