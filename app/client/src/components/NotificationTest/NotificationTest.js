import React from 'react';

class NotificationTest extends React.Component {

    constructor(props) {
        super(props);

        console.log('notifications list')
        console.log(this.props)

        this.state = {
            activities: []
        }
    }
    componentWillReceiveProps(newProps) {
        if(!newProps.loading) {
            console.log(newProps)

        }
    }
    componentDidMount() {
        this.activities = [];


    }

    render() {
        const { loading, error, comments, cookies } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <h1>NotificationTest</h1>
        );
    }
}

export default NotificationTest;
