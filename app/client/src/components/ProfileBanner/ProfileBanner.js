import React from 'react';
import { graphql, compose } from 'react-apollo';
import { Modal, ButtonDropdown, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { withCookies } from 'react-cookie';

import { renderLocation } from '../../services/Location';
import SiteLinks from '../../services/SiteLinks';
import gear from '../../assets/images/gear.png';

import Avatar from '../../components/Avatar';
import FollowConnectButton from '../../components/FollowConnectButton';
import ActionButton from '../../components/ActionButton';
import WithClickToggle from '../../components/WithClickToggle';
import WithHoverToggle from '../../components/WithHoverToggle';
import EditButtonModal from '../../components/EditButtonModal';
import ProBadge from '../../components/ProBadge';

import FollowUser from '../../queries/FollowUser';
import UnfollowUser from '../../queries/UnfollowUser';
import FollowsBack from '../../queries/FollowsBack';

class ProfileCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props
        };

        this.toggleFollow = this.toggleFollow.bind(this);
        this.toggleFollowState = this.toggleFollowState.bind(this);

        this.FollowConnectButton = compose(
            graphql(FollowsBack, {
                props: ({ ownProps, data: { followsBack, loading, error }}) => ({
                    ...ownProps, followsBack, loading, error
                }),
                options: { variables: { 
                    username: props.loggedInUsername,
                    targetname: props.username
                }}
            }),
            graphql(FollowUser, { name: 'followMutate' }),
            graphql(UnfollowUser, { name: 'unfollowMutate' })
        )(FollowConnectButton);
    }

    async onUnfollowClick() {
        const { unfollowMutate } = this.props;
        
        let res;
        try {
            res = await unfollowMutate({ variables: { username: this.props.loggedInUsername, targetname: this.props.username } })
            this.setState({
                isFollowing: false
            });
        } catch (err) {
            return;
        }
    }

    async onFollowClick() {
        const { followMutate } = this.props;
        
        let res;
        try {
            res = await followMutate({ variables: { username: this.props.loggedInUsername, targetname: this.props.username } })
            this.setState({
                isFollowing: true
            });
        } catch (err) {
            return;
        }
    }

    async toggleFollow() {
        if (this.state.isFollowing) {
            await this.onUnfollowClick();
        } else {
            await this.onFollowClick();
        }
    }

    toggleFollowState({ following }) {
        this.setState({
            isFollowing: following
        })
    }

    componentWillUpdate(nextProps,nextState) {
        console.log('component updated', nextState);
    }

    render() {
        const { 
            isSelf, 
            accountMeta, 
            actionText, 
            modalToggle, 
            profileImages, 
            fullname, 
            profession, 
            contact, 
            isFollowing, 
            username, 
            loggedInUsername,
            about
        } = this.state;


        console.log('isFollowing', isFollowing);
        const isPro = accountMeta.subscription == "PRO";
        const followUnfollow = isFollowing ? 'Unfollow' : 'Follow';

        return (
            <div className="ProfileCard">
                <Avatar 
                    proBadge={() => <ProBadge isVisible={isPro} />}
                    filename={profileImages.avatar} />

                <div className="content-wrapper">
                    <div className="UserFullName">{fullname}</div>

                    <UserMeta 
                        type={about.profession} 
                        location={renderLocation(contact)} />

                    <div className="card-buttons">
                        { !isSelf && <this.FollowConnectButton handleFollowing={this.toggleFollowState} following={isFollowing} username={loggedInUsername} targetname={username}></this.FollowConnectButton> }

                        <WithClickToggle>
                            {(isOpen, toggle) => {
                                return (
                                    <ButtonDropdown isOpen={isOpen} toggle={toggle}>
                                        <DropdownToggle className="ellipses full-border" color="primary">...</DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={this.toggleFollow}>{followUnfollow}</DropdownItem>
                                            <DropdownItem>Share</DropdownItem>
                                        </DropdownMenu>
                                    </ButtonDropdown>
                                );
                            }}
                        </WithClickToggle>

                        <WithClickToggle>
                            {(isOpen, toggle) => {
                                return (
                                    <ButtonDropdown isOpen={isOpen} toggle={toggle}>
                                        <Button id="caret" color="success">{actionText}</Button>
                                        <DropdownToggle caret color="success" />
                                        <DropdownMenu>
                                            <DropdownItem onClick={modalToggle}>Edit Button</DropdownItem>
                                        </DropdownMenu>
                                    </ButtonDropdown>
                                );
                            }}
                        </WithClickToggle>
                    </div>
                </div>
            </div>
        );
    }
}

const UserMeta = (props) => {
    const { type, location } = props;

    return (
        <div className="UserMeta">
            <span className="user-type">{type}</span>
            <span className="location">
                <i className="fas fa-map-marker-alt"></i>
                {location}
            </span>
        </div>
    );
}

const MyAccountLink = (props) => {
    const { url, img } = props;
    return (
        <div className="MyAccountLink">
            <a href={url}>
                <img src={img} alt="Click to edit account information" />
            </a>
        </div>
    );
}

const ProfileMeta = (props) => {
    return (
        <div className="ProfileMeta">
            <div className="top">
                <span className="uploads">{props.uploads} Uploads</span>
                <span className="followers">{props.followers} Followers</span>
                <span className="following">{props.following} Following</span>
            </div>

            <div className="bottom">
                <span className="views">
                    {props.views} 
                    <i className="fas fa-eye"></i>
                </span>
                <span className="likes">
                    {props.likes}
                    <i className="far fa-heart"></i>
                </span>
            </div>
        </div>
    );
}

class ProfileBanner extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoggedIn: this.props.cookies.get('isLoggedIn'),
            loggedInUsername: this.props.cookies.get('username'),
            isEditButtonModalOpen: false,
            editButtonModalPage: 0,
            buttonText: 'Message',
        }

        this.toggleEditButtonModal = this.toggleEditButtonModal.bind(this);
        this.setResult = this.setResult.bind(this);

        this.ProfileCard = compose(
            graphql(FollowUser, { name: 'followMutate' }),
            graphql(UnfollowUser, { name: 'unfollowMutate' })
        )(ProfileCard);
    }

    toggleEditButtonModal() {
        const isOpen = !this.state.isEditButtonModalOpen;

        this.setState({
            isEditButtonModalOpen: isOpen,
            editButtonModalPage: 0
        })
    }

    setResult(arg) {
        console.log(arg);
        const {text, action} = arg

        this.setState({
            buttonText: text,
            buttonAction: action,
        })

        this.toggleEditButtonModal()
    }

    render() {
        const { loading, error } = this.props.data;

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { user } = this.props.data;
        const { username, fullname, profession, contact, stats, avatar } = user;
        const { uploads, following, followers, views, favorites } = stats;
        const { isLoggedIn, loggedInUsername } = this.state;

        return (
            <div className="ProfileBanner">
                <div className="profile-content-wrapper">
                    { isLoggedIn && loggedInUsername == username 
                        && <MyAccountLink 
                            url={`${SiteLinks.account}/${username}/edit`} 
                            img={gear} /> }

                    <this.ProfileCard 
                        { ...user }
                        loggedInUsername={loggedInUsername}
                        actionText={this.state.buttonText}
                        action={this.state.buttonAction}
                        isSelf={loggedInUsername == username}
                        modalToggle={this.toggleEditButtonModal} />

                    <EditButtonModal 
                        page={this.state.editButtonModalPage}
                        toggle={this.toggleEditButtonModal} 
                        setResult={this.setResult}
                        isOpen={this.state.isEditButtonModalOpen} />

                    <ProfileMeta 
                        uploads={uploads}
                        following={following}
                        followers={followers}
                        views={views}
                        likes={favorites} />
                </div>
            </div>
        );
    }
};

export default withCookies(ProfileBanner);
