import React from 'react';
import { Media } from 'reactstrap';
import Avatar from '../../components/Avatar';
import ProBadge from '../../components/ProBadge';
import { isSubscribed } from '../../services/UserMeta';
import { prettyDate } from '../../services/Dates';

const Notification = ({ activity }) => {
    if (!activity) {
        return null;
    }

    console.log(activity);

    const { user } = activity.activities[0].record;
    const { subscribed, contact, avatar, fullname } = user;

    return (
        <div className="Notification">
            <Media>
                <Media left href="#">
                    <Avatar proBadge={null} filename={avatar} />  
                    <div className="online-wrapper"></div>
                </Media>

                <Media body>
                    <div className="alignment-wrapper">
                        <div className="heading-wrapper">
                            <Media heading className="fullname">
                                {fullname} 
                            </Media>
                            <ProBadge size="regular" isVisible={isSubscribed(subscribed)} />
                        </div>
                        
                        <p className="description">
                            {activity.verb}
                        </p>
                    </div>
                    <div className="timestamp">
                        {prettyDate(activity.created_at)}
                    </div>
                </Media>
            </Media>
            <hr />
        </div>
    );
}

export default Notification;
