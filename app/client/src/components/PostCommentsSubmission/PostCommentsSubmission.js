import React from 'react';
import { withCookies } from 'react-cookie';
import Textarea from "react-textarea-autosize";
import Media from 'react-media';
import { Alert, Button, Form } from 'reactstrap';

class PostCommentsSubmission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alerts: { 
                success: true, 
                failure: false 
            },
            message: '',
            timelineId: this.props.timelineId,
            projectId: this.props.projectId,
            shareId: this.props.shareId,
            username: this.props.username
        }

        this.submitForm = this.submitForm.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleEnterSubmission = this.handleEnterSubmission.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    async submitForm(e) {
        e && e.preventDefault();

        console.log(this.state.username, this.props.cookies.get('username'), this.props.cookies);

        this.setState({ 
            alerts: { 
                success: false, 
                failure: false 
            } 
        })

        const { mutate } = this.props;

        let res;
        try {
            res = await mutate({ variables: {
                username: this.props.cookies.get('username'),
                message: this.state.message,
                timelineId: this.props.timelineId,
                projectId: this.props.projectId,
                shareId:this.props.shareId
             } })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    success: false, 
                    failure: true 
                } 
            })

            return;
        }

        if(this.props.onCommentSubmission) {
            this.props.onCommentSubmission(res);
         }

        this.setState({ 
            message: '',
            alerts: { 
                success: true, 
                failure: false 
            }
        })
    }

    handleEnterSubmission(e) {
        const textArea = e.target;
        const caret = getCaret(textArea);
        let content = textArea.value;

        if (!e.ctrlKey && e.key == 'Enter') {
            this.submitForm();
            e.preventDefault()
        } else if (e.ctrlKey && e.key =='Enter') {
            textArea.value = content.substring(0, caret) + "\n" + content.substring(caret, content.length);
        }

        function getCaret(el) { 
            if (el.selectionStart) { 
                return el.selectionStart; 
            } else if (document.selection) { 
                el.focus();
                var r = document.selection.createRange(); 
                if (r == null) { 
                    return 0;
                }
                var re = el.createTextRange(), rc = re.duplicate();
                re.moveToBookmark(r.getBookmark());
                rc.setEndPoint('EndToStart', re);
                return rc.text.length;
            }  
            return 0; 
        }
    }

    render() {
        const { loading, error } = this.props;
        
        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <div className="PostCommentsSubmission">
                <Form onSubmit={this.submitForm}>
                    <Textarea name="message" minRows={3}  placeholder="Comment" value={this.state.message} onKeyPress={this.handleEnterSubmission} onChange={this.handleInputChange} />
                    <div className="alert-wrapper">
                        {
                            // <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                        }
                        <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                    </div>
                </Form>
            </div>
        );
    }
    
}


export default withCookies(PostCommentsSubmission);
