import React from 'react';
import { Media } from 'reactstrap';
import { postedDate } from '../../services/Dates';
import Avatar from '../../components/Avatar';
import ProBadge from '../../components/ProBadge';
import withUser from '../../hocs/withUser'

const CommentHeading = (props) => {
    const { username, fullname, avatar, isOnline, date} = props;
    const UserAvatar = withUser(username)

    return (
        <div className="CommentHeading">
            <Media>
                <Media left href="#">
                    <UserAvatar render={(user) => {
                        const isSubscribed =  user.subscribed == "PRO";   
                        return <Avatar proBadge={() => <ProBadge size="small" isVisible={isSubscribed} />} filename={avatar} />  
                    }} />
                </Media>

                <Media body>
                    <Media heading className="fullname">
                        {fullname}
                        <div className="online-wrapper"></div>
                    </Media>
                    <div className="meta">
                        <span className="date">{postedDate(date)}</span>
                    </div>
                </Media>
            </Media>
        </div>
    );
};

export default CommentHeading;
