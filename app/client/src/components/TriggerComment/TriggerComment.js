import React from 'react';

class TriggerComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

        this.triggerComment = this.triggerComment.bind(this);
    }

    triggerComment(e) {
        e.preventDefault();
        this.props.toggle();
    }

    render() {
        return (
            <a href="#" className="TriggerComment" onClick={this.triggerComment}>
                <i className="fas fa-comment"></i> Comment
            </a>
        );
    }
}

export default TriggerComment;
