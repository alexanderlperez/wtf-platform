import React from 'react';
import ProjectEntry from '../../components/ProjectEntry';
import { CSSGrid, measureItems, makeResponsive } from 'react-stonecutter';

const Grid = makeResponsive(
    measureItems(CSSGrid), 
    { maxWidth: 1170, minPadding: 0 }
);

const gridProps = {
    duration: 0,
    component: "ul",
    columns: 4,
    columnWidth: 270,
    gutterWidth: 30,
    gutterHeight: 30,
    itemHeight: 270,
};

const ProjectGrid = (props) => {
    const { projects, handler } = props;

    // Each ProjectEntry's internal overlay will deal with toggling WrapWithModal
    // - clicking a grid item will fire the handler
    // - the handler is passed the project

    const gridItems = projects.map((proj, i) => (
        <li className="entry-wrapper" key={i}>
            <ProjectEntry clickHandler={() => handler(proj)} {...proj} />
        </li>
    ));

    return (
        <Grid {...gridProps}>{gridItems}</Grid>
    );
}

export default ProjectGrid;
