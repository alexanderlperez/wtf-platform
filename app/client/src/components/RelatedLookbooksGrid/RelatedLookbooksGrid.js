import React from 'react';
import LookbookEntry from '../../components/LookbookEntry';
import { CSSGrid, layout, measureItems, makeResponsive } from 'react-stonecutter';
import SiteLinks, { toUploadUrl } from '../../services/SiteLinks';
import {NavLink as Link} from 'react-router-dom';

// TODO: merge w/ Projects grid, make configurable, with sane defaults

const gridProps = {
        layout: layout.pinterest,
        component: "ul",
        columns: 4,
        columnWidth: 212,
        gutterWidth: 24,
        gutterHeight: 20,
        duration: 0,
};

const RelatedLookbooksGrid = (props) => {
    const { lookbooks, handler, username } = props;

    // Each ProjectEntry's internal overlay will deal with toggling WrapWithModal
    // - clicking a grid item will fire the handler
    // - the handler is passed the project

    const gridItems = lookbooks.map((lookbook, i) => (
        <li className="entry-wrapper" key={i}>
            <LookbookEntry 
                textLimit={80} 
                clickHandler={() => handler(lookbook)} 
                {...lookbook} 
            />
        </li>
    ))

    const Grid = makeResponsive(
        measureItems(CSSGrid), 
        { maxWidth: 970, minPadding: 10 }
    );

    return (
        <Grid {...gridProps}>{gridItems}</Grid>
    );
}

export default RelatedLookbooksGrid;
