import React from 'react';
import classNames from 'classnames';
import {Link} from 'react-router-dom';

class MobileToggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMenuRevealed: false,
            isSubmenuOpen: {
                discover: false,
            }
        };

        this.toggleMenu = this.toggleMenu.bind(this);
        this.toggleSubmenu = this.toggleSubmenu.bind(this);
    }

    toggleSubmenu(e, key) {
        e.preventDefault();

        this.setState(function (prev) {
            return {
                isSubmenuOpen: {
                    [key]: !prev.isSubmenuOpen[key]
                }
            }
        });
    }

    toggleMenu() {
        this.setState({
            isMenuRevealed: !this.state.isMenuRevealed
        })
    }

    render() {
        const menuClasses = classNames('OffCanvasMenu', {
            'menu-revealed': this.state.isMenuRevealed,
            'menu-hidden': !this.state.isMenuRevealed,
        });

        return (
            <div className="MobileToggle">
                <div className="ToggleButton" onClick={this.toggleMenu}>
                    <i className="fas fa-bars"></i>
                </div>

                <div className={menuClasses}>
                    <ul>
                        <li>
                            <a href="#" onClick={(e) => this.toggleSubmenu(e, 'discover')}>Discover</a>
                        </li>
                        <li className={classNames('submenu', { isVisible: this.state.isSubmenuOpen.discover })}>
                            <ul>
                                <li><Link to="">Popular</Link></li>
                                <li><Link to="">Fresh</Link></li>
                                <li><Link to="">Brand</Link></li>
                            </ul>
                        </li>
                        <li><Link to="/lookbooks">LookBook</Link></li>
                        <li><Link to="/jobs">Jobs</Link></li>
                        <li><Link to="/blog">Blog</Link></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default MobileToggle;
