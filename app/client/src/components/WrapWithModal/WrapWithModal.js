import React from 'react';
import { Button, Modal,  ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import classNames from 'classnames';

const CloseButton = ({toggle}) => {
    return (
        <span className="CloseButton" onClick={toggle}>
            <i className="fas fa-times"></i>
        </span>
    );
}

class WrapWithModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} backdrop={true} className={classNames("WrapWithModal", this.props.className)}>
                <CloseButton toggle={this.props.toggle} />
                <ModalBody>
                    {this.props.children}
                </ModalBody>
            </Modal>
        )
    }
}

export default WrapWithModal;
