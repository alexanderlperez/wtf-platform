import React from 'react';
import { graphql, compose } from 'react-apollo';
import { Alert, Button, Form } from 'reactstrap';
import Textarea from "react-textarea-autosize";
import Media from 'react-media';

import AddProjectComment from '../../queries/AddProjectComment';
import { desktop } from '../../services/MediaQueries';

class CommentSubmission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alerts: { success: false, failure: false },
            username: this.props.username,
            projectId: this.props.projectId,
            message: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.handleEnterSubmission = this.handleEnterSubmission.bind(this)
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    async submitForm(e) {
        e && e.preventDefault();

        this.setState({ 
            alerts: { 
                comment: '',
                success: false, 
                failure: false 
            } 
        })

        const { mutate } = this.props;

        let res;
        try {
            res = await mutate({ variables: this.state })
        } catch (err) {
            this.setState({ 
                alerts: { 
                    success: false, 
                    failure: true 
                } 
            })

            return;
        }

        this.setState({ 
            alerts: { 
                success: true, 
                failure: false 
            }
        })

        this.props.handler(res.data.addProjectComment)
    }

    handleEnterSubmission(e) {
        const textArea = e.target;
        const caret = getCaret(textArea);
        let content = textArea.value;

        if (!e.ctrlKey && e.key == 'Enter') {
            this.submitForm();
            e.preventDefault()
        } else if (e.ctrlKey && e.key =='Enter') {
            textArea.value = content.substring(0, caret) + "\n" + content.substring(caret, content.length);
        }

        function getCaret(el) { 
            if (el.selectionStart) { 
                return el.selectionStart; 
            } else if (document.selection) { 
                el.focus();
                var r = document.selection.createRange(); 
                if (r == null) { 
                    return 0;
                }
                var re = el.createTextRange(), rc = re.duplicate();
                re.moveToBookmark(r.getBookmark());
                rc.setEndPoint('EndToStart', re);
                return rc.text.length;
            }  
            return 0; 
        }
    }

    render() {
        return (
            // TODO: submit comment on enter for desktop
            <div className="CommentSubmission">
                <Form onSubmit={this.submitForm}>
                    <textarea name="message" rows={2} placeholder="Comment" value={this.state.message} onKeyPress={this.handleEnterSubmission} onChange={this.handleInputChange} />
                    <div className="alert-wrapper">
                        {
                            // <Alert isOpen={this.state.alerts.success} color="success">Saved successfully</Alert>
                        }
                        <Alert isOpen={this.state.alerts.failure} color="danger">There was a problem saving</Alert>
                    </div>

                    <Media query={desktop}>
                        {isDesktop => isDesktop ? "" : <Button type="submit">Save</Button>}
                    </Media>
                </Form>
            </div>
        );
    }
}

export default graphql(AddProjectComment)(CommentSubmission);
