import React from 'react';
import { NavLink } from 'react-router-dom';
import { Nav, NavItem} from 'reactstrap';

const SidebarNav = (props) => {
    const { url } = props;

    return (
        <div className="SidebarNav">
            <Nav vertical>
                <NavItem>
                    <NavLink to={`${url}/basicinformation`}>
                        Basic Information
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/aboutme`}>
                        About Me
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/sociallinks`}>
                        Social Links
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/workexperience`}>
                        Work Experience
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/skills`}>
                        Skills
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/awards`}>
                        Awards
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/resumeupload`}>
                        Resume Upload
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/notification`}>
                        Notification
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink to={`${url}/changepassword`}>
                        Change Password
                    </NavLink>
                </NavItem>
            </Nav>
        </div>
    );
};


export default SidebarNav;
