import React from 'react';
import { prefixHash } from '../../services/Text';
import { formatCategory } from '../../services/Categories';
import Licenses from '../../components/Licenses';
import RevealText from '../../components/RevealText';

class DetailsMeta extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };

        this.toggleReveal = this.toggleReveal.bind(this);
    }

    toggleReveal() {
        console.log('called');
        this.setState({
            isRevealed: !this.state.isRevealed,
        })
    }

    render() {
        const { category, tags, description, licenses } = this.props;
        const maxLength = 250;
        const needsReveal = this.props.description.length > maxLength;

        return (
            <div className="DetailsMeta">
                <div className="left">
                    <div className="content-wrapper">
                        <div className="heading">Category</div>
                        <div className="category"><span>#</span>{formatCategory(category)}</div>
                    </div>

                    <div className="content-wrapper Tags">
                        <div className="heading">Tags</div>
                        <div className="tags">{prefixHash(tags || [])}</div>
                    </div>
                </div>
                <div className="right">
                    <div className="content-wrapper">
                        <div className="heading">Project Description</div>
                        { needsReveal 
                            ? (<RevealText 
                                className="description"
                                isRevealed={this.state.isRevealed}
                                handler={this.toggleReveal}
                                text={description} 
                                maxLength={maxLength}/>)
                            : (
                                <p className="description">{description}</p>
                            )
                        }
                    </div>
                    <div className="content-wrapper">
                        <Licenses licenses={licenses} />
                    </div>
                </div>
            </div>
        );
    }
}

export default DetailsMeta;
