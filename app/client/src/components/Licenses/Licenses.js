import React from 'react';
import byCC from '../../assets/images/cc-icons/by.png';
import ncCC from '../../assets/images/cc-icons/nc.png';

const Licenses = (props) => {
    const hasLicenses = !!props.licenses;
    const DefaultLicenses = () => {
        return (
            <div>
                <img src={byCC} alt="" />
                <img src={ncCC} alt="" />
            </div>
        )
    }

    return (
        <div className="Licenses">
            {!hasLicenses ? (<DefaultLicenses />) : (<div>Licenses</div>)}
        </div>
    );
};

export default Licenses;
