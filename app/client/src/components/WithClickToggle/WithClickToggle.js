import React from 'react';

/**
 * Can use this to quickly scaffold reactstrap components that need toggle and isToggled props:
 * - leave 'component' prop off
 */

class WithClickToggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggled: false,
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isToggled: !this.state.isToggled
        })
    }

    render() {
        const { component = null, dropdown } = this.props;

        return (
            <div className="WithClickToggle">
                <div onClick={this.toggle}>
                    {component}
                </div>

                {this.props.children(this.state.isToggled, this.toggle)}
            </div>
        );
    }
}

export default WithClickToggle;
