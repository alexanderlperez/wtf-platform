import React from 'react';
import SiteLinks from '../../services/SiteLinks';

const DetailsImages = (props) => {
    return (
        <div className="DetailsImages">
            {props.images.map((image, i) => {
                let coverImagePath = '';
                let testPath = image.path.split(':')[0];
                if(testPath=='http' || testPath=='https') {
                    coverImagePath = `${image.path}`
                } else {
                    coverImagePath = `${SiteLinks.uploads}/${image.path}`
                }
                return (
                    <div key={i} className="image-wrapper">
                        <img src={coverImagePath} alt="" />
                    </div>
                )
            })}
        </div>
    );
}

export default DetailsImages;
