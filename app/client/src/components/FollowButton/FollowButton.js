import React from 'react';
import {Button} from 'reactstrap';
import { graphql, compose } from 'react-apollo';

class FollowButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            following: this.props.following
        }

        this.onFollowClick = this.onFollowClick.bind(this);
        this.onUnfollowClick = this.onUnfollowClick.bind(this);
    }

    componentWillReceiveProps({ following }) {
        this.setState({
            following
        })
    }

    async onFollowClick() {
        //TODO: perform the steps needed for a "connection"

        const { followMutate } = this.props;
        
        let res;
        try {
            res = await followMutate({ variables: { username: this.props.username, targetname: this.props.targetname } })
            this.setState({
                following:true
            });
        } catch (err) {
            return;
        }
    }

    async onUnfollowClick() {
        //TODO: perform the steps needed for a "remove connection"

        const { unfollowMutate } = this.props;
        
        let res;
        try {
            res = await unfollowMutate({ variables: { username: this.props.username, targetname: this.props.targetname } })
            this.setState({
                following:false
            });
        } catch (err) {
            return;
        }
    }

    render() {
        const { loading, error } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { 
            color,
            following,
            followsBack,
        } = this.props;

        const follow = <Button onClick={this.onFollowClick} color={color || "danger"} className="follow">Follow</Button> 
        const unfollow = <Button onClick={this.onUnfollowClick} color={color || "danger"} className="follow">Unfollow</Button>

        if (!this.state.following) {
            return follow;
        }
        return unfollow;
    }
}

export default FollowButton
