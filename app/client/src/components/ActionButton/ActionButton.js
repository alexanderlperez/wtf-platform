import React from 'react';
import { Button } from 'reactstrap';

const ActionButton = ({ children, action, ...props }) => {
    return (
        <Button {...props} className="ActionButton" onClick={action}>
            {children}
        </Button>
    );
}


export default ActionButton;
