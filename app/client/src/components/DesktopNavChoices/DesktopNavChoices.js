import React from 'react';
import {Nav, NavItem, UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu} from 'reactstrap';
import {NavLink} from 'react-router-dom';
import SiteLinks from '../../services/SiteLinks';

const DesktopNavChoices = () => {
    return (
        <div className="DesktopNavChoices">
            <Nav className="desktop-nav-list">
                <UncontrolledDropdown nav className="left-arrow">
                    <DropdownToggle nav>
                        Discover
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem><NavLink to={SiteLinks.popular}>Popular</NavLink></DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem><NavLink to={SiteLinks.fresh}>Fresh</NavLink></DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem><NavLink to={SiteLinks.brand}>Brand</NavLink></DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>

                <NavItem><NavLink to={SiteLinks.lookbooks}>LookBook</NavLink></NavItem>
                <NavItem><NavLink to={SiteLinks.jobs}>Jobs</NavLink></NavItem>
                <NavItem><NavLink to={SiteLinks.blog}>Blog</NavLink></NavItem>
            </Nav>
        </div>
    );
};

export default DesktopNavChoices;
