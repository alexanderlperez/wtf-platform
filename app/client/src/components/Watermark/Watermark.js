import React from 'react';

import Slider from 'rc-slider';
import { Button  } from 'reactstrap';
import watermark from 'watermarkjs';


class Watermark extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            images:this.props.images,
            currentImage:null,
            position:'center',
            username:this.props.username || '',
            displayImage:null
        }

        this.selectImage(this.props.images[0], 'center')



        this.selectImage = this.selectImage.bind(this);
        this.onClickSave = this.onClickSave.bind(this);
        this.textManipulation = this.textManipulation.bind(this);
        this.onChangePosition = this.onChangePosition.bind(this);
    }

    textManipulation(username, position) {
        
        let translationRatios = {
            center: {
                x:1/2,
                y:1/2
            },
            topleft: {
                x:1/3,
                y:1/3
            },
            topright: {
                x:2/3,
                y:1/3
            },
            bottomleft:{
                x:1/3,
                y:2/3
            },
            bottomright:{
                x:2/3,
                y:2/3
            }
        }

        return (target) => {
            let context = target.getContext('2d');

            let fontSize = Math.round(target.width*0.06);

            context.globalAlpha = 0.6;
            context.fillStyle = '#fff';
            context.font = fontSize+'px Josefin Slab';

            let metrics = context.measureText(username);

            let x = Math.round(target.width * translationRatios[position].x) - Math.round(metrics.width/2);
            let y = Math.round(target.height * translationRatios[position].y) - Math.round(fontSize/2);
            
            console.log(target, translationRatios[position], metrics, x, y);

            context.translate(x, y);
            context.fillText(username, 0, 0)

            return target;
        }
    }

    selectImage(image, position) {
        watermark([image.data])
            .image(this.textManipulation(this.state.username, position))
            .then(img => {
                this.onChangePosition(position);
                this.setState({
                    displayImage: img,
                    currentImage: image,
                    position: position
                })
            })
    }   

    onChangePosition(position) {
        this.props.onChangePosition(position);
    }

    onClickSave() {
        this.props.onSave(this.state.position)
    }

    render() {
        const imageItems = this.state.images.map((image, i) => (
            <div className={image.id} key={i} style={{display:'inline-block'}} onClick={() => this.selectImage(image, this.state.position)}>
                <img src={image.preview}  width="200" />
            </div>
        ));
        return (
            <div className="WatermarkTool">
                <img src={this.state.displayImage && this.state.displayImage.src} width="400" />
                <div>
                    <Button onClick={() => {this.selectImage(this.state.currentImage, 'center')}}>Center</Button>
                    <Button onClick={() => {this.selectImage(this.state.currentImage, 'topleft')}}>Top Left</Button>
                    <Button onClick={() => {this.selectImage(this.state.currentImage, 'topright')}}>Top Right</Button>
                    <Button onClick={() => {this.selectImage(this.state.currentImage, 'bottomleft')}}>Bottom Left</Button>
                    <Button onClick={() => {this.selectImage(this.state.currentImage, 'bottomright')}}>Bottom Right</Button>
                    <Button onClick={this.onClickSave}>Save</Button>
                </div>
                <div>
                    {imageItems}
                </div>
            </div>
        )
    }
}

export default Watermark;
