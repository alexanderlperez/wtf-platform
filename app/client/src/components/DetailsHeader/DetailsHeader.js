import React from 'react';
import { Media, Button } from 'reactstrap';
import { withCookies } from 'react-cookie';
import { graphql, compose } from 'react-apollo';

import Avatar from '../../components/Avatar';
import FollowConnectButton from '../../components/FollowConnectButton';
import ProBadge from '../../components/ProBadge';
import ShareItem from '../../components/ShareItem';
import WithFollowUnfollow from '../../components/WithFollowUnfollow';

import SiteLinks from '../../services/SiteLinks';
import { isSubscribed } from '../../services/UserMeta';
import { renderLocation } from '../../services/Location';

import withUser from '../../hocs/withUser'

const DetailsHeader = (props) => {
    const Stats = ({ likes, comments, shares }) => ( 
        <div className="stats">
            <span><i className="likes far fa-heart"></i>{likes}</span>
            <span><i className="comments far fa-comment"></i>{comments}</span>
            <span><i className="shares fas fa-share-square"></i>{shares}</span>
        </div>
     );

    const UserAvatar = withUser(props.username);
    const loggedInUsername = props.cookies.get('username');

    return (
        <div className="DetailsHeader">
            <div className="top-wrapper">
                <Media>
                    <Media left href="#">

                    <UserAvatar render={(user) => {
                        return <Avatar proBadge={() => <ProBadge size="small" isVisible={isSubscribed(user.accountMeta.subscription)} />} filename={props.profileImages.avatar} />  
                    }} />

                    </Media>
                    <Media body>
                        <Media heading className="fullname">
                            {props.fullname}
                        </Media>
                        <div className="meta">
                            <span className="type">
                                {props.about.profession}
                            </span>
                            <span className="location"><i className="fas fa-map-marker-alt"></i> {renderLocation(props.contact)}</span>
                        </div>
                    </Media>
                </Media>

                { 
                    props.isPreview || 
                    <WithFollowUnfollow 
                        loggedInUsername={loggedInUsername} 
                        username={props.username} 
                        isFollowing={props.isFollowing} 
                        render={(toggleFollowing, isFollowing) => {
                            return (
                                <Button onClick={toggleFollowing} color="success">
                                    { isFollowing ? 'Unfollow' : 'Follow' }
                                </Button>
                            )
                    }}/>
                }
            </div>
            <div className="bottom-wrapper">
                <div className="title">{props.title}</div>
                { props.isPreview || <Stats likes={props.likes} comments={props.comments} shares={props.shares} /> }
            </div>
        </div>
    );
}

export default withCookies(DetailsHeader);
