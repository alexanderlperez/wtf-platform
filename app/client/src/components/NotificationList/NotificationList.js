import React from 'react';
import * as stream from 'getstream';

class NotificationList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activities: [],
            tokens:{
                notification:'',
                timeline:'',
                user:''
            }
        }
    }
    componentWillReceiveProps(newProps) {
        console.log('newprops', newProps);

        if(!newProps.loading) {
            // Use these methods to listen for notification updates
            // Use the counter to display on notification icon
            // There is a separate endpoint in NotificationsView for retrieving list of notifications
            // This endpoint will trigger a mark_seen, mark_read event

            this.setState({
                activities: newProps.notifications
            })

            var client = stream.connect(
                newProps.tokens.api,
                null,
                newProps.tokens.appId
            );

            var feed = client.feed(
                "notification",
                newProps.tokens.userId,
                newProps.tokens.notification
            );

            feed.subscribe((response) => {
                    console.log('feed update', response);
                }).then((success) => {
                    console.log('success connection', success)
                }, (error) => {
                    console.log('error connection', error);
                })
        }
    }

    componentDidMount() {
        this.activities = [];
    }

    render() {
        const { loading, error, comments, cookies } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <div className="NotificationList">
                {this.props.render(this.state.activities)}
            </div>
        );
    }
}

export default NotificationList;
