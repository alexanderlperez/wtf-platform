import React from 'react';
import PostUserHeading from '../../components/PostUserHeading';

const PostMeta = ({ avatar, subscribed, fullname, profession, likes, comments }) => {
    console.log('post meta', likes, comments);
    return (
        <div className="PostMeta">
            <PostUserHeading 
                avatar={avatar} 
                subscribed={subscribed} 
                fullname={fullname} 
                professionName={profession} />

            <div className="PostStats">
                <i className="fas fa-heart"></i> {likes || '0'}
                <i className="fas fa-comment"></i> {comments || '0'}
            </div>
        </div>
    );
}

export default PostMeta;
