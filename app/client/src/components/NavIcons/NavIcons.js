import React from 'react';
import { graphql } from 'react-apollo';
import { NavLink as Link } from 'react-router-dom';
import {Nav, NavItem, NavLink, UncontrolledDropdown, DropdownItem, DropdownToggle, DropdownMenu} from 'reactstrap';

import SiteLinks from '../../services/SiteLinks';
import Avatar from '../../components/Avatar';
import User from '../../queries/User';

class NavIcons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };

        this.UserAvatar = graphql(User, { options: { variables: { username: props.username } } })(Avatar)
    }

    toggleSearch() {
        // TODO:
        // activate reveal of search bar
        // "press enter to search"
    }

    render() {
        return (
           <Nav className="NavIcons">
                <NavItem onClick={this.toggleSearch}><span className="icon"><i className="fa fa-search"></i></span></NavItem>
                <NavItem><span className="icon"><Link to={`${SiteLinks.notifications}`}><i className="far fa-bell"></i></Link></span></NavItem>
                <NavItem><span className="icon"><Link to={`${SiteLinks.messages}`}><i className="far fa-envelope"></i></Link></span></NavItem>

                <UncontrolledDropdown nav className="right-arrow">
                    <DropdownToggle nav>
                        <this.UserAvatar />
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem><NavLink href={`${SiteLinks.account}/${this.props.username}`}>View Profile</NavLink></DropdownItem>
                        <DropdownItem><NavLink href={`${SiteLinks.account}/${this.props.username}/edit`}>My Account</NavLink></DropdownItem>
                        {
                            // <DropdownItem><NavLink href={SiteLinks.joblist}>Job List</NavLink></DropdownItem>
                        }
                        <DropdownItem divider />
                        <DropdownItem><NavLink href={SiteLinks.logout}>Log Out</NavLink></DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
        );
    }

}

export default NavIcons;
