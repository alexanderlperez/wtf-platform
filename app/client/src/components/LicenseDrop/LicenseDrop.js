import React from 'react';
import createClass from 'create-react-class';
import PropTypes from 'prop-types';
import Select from 'react-select';
import Gravatar from 'react-gravatar';
import { licenses } from '../../services/Constants'

const GRAVATAR_SIZE = 15;

const stringOrNode = PropTypes.oneOfType([
	PropTypes.string,
	PropTypes.node,
]);

const IconOption = createClass({
	propTypes: {
		children: PropTypes.node,
		className: PropTypes.string,
		isDisabled: PropTypes.bool,
		isFocused: PropTypes.bool,
		isSelected: PropTypes.bool,
		onFocus: PropTypes.func,
		onSelect: PropTypes.func,
		option: PropTypes.object.isRequired,
	},
	handleMouseDown (event) {
		event.preventDefault();
		event.stopPropagation();
		this.props.onSelect(this.props.option, event);
	},
	handleMouseEnter (event) {
		this.props.onFocus(this.props.option, event);
	},
	handleMouseMove (event) {
		if (this.props.isFocused) return;
		this.props.onFocus(this.props.option, event);
	},
	render () {
        var icons = this.props.option.icons.map((icon, i) => {
            var styles = {
                display:"inline-block",
                width:15,
                height:15,
                backgroundImage:`url(${icon})`,
                backgroundPosition:'center',
                backgroundSize:'contain',
                backgroundPosition:'no-repeat',
                marginLeft:5
            }
            return (
                <div key={i} style={styles}></div>
            )
        })
		return (
			<div className={this.props.className}
				onMouseDown={this.handleMouseDown}
				onMouseEnter={this.handleMouseEnter}
				onMouseMove={this.handleMouseMove}
				title={this.props.option.content}>
				{icons}
				<div>{this.props.option.content}</div>
			</div>
		);
	}
});

const IconValue = createClass({
	propTypes: {
		children: PropTypes.node,
		placeholder: stringOrNode,
		value: PropTypes.object
	},
	render () {
        var icons = this.props.value.icons.map((icon, i) => {
            var styles = {
                display:"inline-block",
                width:15,
                height:15,
                backgroundImage:`url(${icon})`,
                backgroundPosition:'center',
                backgroundSize:'contain',
                backgroundPosition:'no-repeat',
                marginLeft:5,
                marginBottom:-2
            }
            return (
                <div key={i} style={styles}></div>
            )
        })

		return (
			<div className="Select-value" title={this.props.value.content}>
				<span className="Select-value-label">
					{icons}
					{this.props.value.content}
				</span>
			</div>
		);
	}
});

const LicenseDrop = createClass({
	propTypes: {
		hint: PropTypes.string,
		label: PropTypes.string,
	},
	getInitialState () {
		return {};
	},
	setValue (value) {
        if(this.props.onChange) {
            this.props.onChange(value);
        }
		this.setState({ value });
	},
	render () {
		var placeholder = <span>Select License</span>;

		return (
			<div className="section">
				<Select
					arrowRenderer={arrowRenderer}
					onChange={this.setValue}
					optionComponent={IconOption}
					options={licenses}
					placeholder={placeholder}
					value={this.state.value}
					valueComponent={IconValue}
					/>
			</div>
		);
	}
});

function arrowRenderer () {
	return (
		<span>+</span>
	);
}

export default LicenseDrop;