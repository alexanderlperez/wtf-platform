import React from 'react';
import { graphql, compose } from 'react-apollo';

import LikeItem from '../../components/LikeItem';
import TriggerComment from '../../components/TriggerComment';
import ShareItem from '../../components/ShareItem';

import AddTimelineLike from '../../queries/AddTimelineLike';
import RemoveTimelineLike from '../../queries/RemoveTimelineLike';
import AddProjectLike from '../../queries/AddProjectLike';
import RemoveProjectLike from '../../queries/RemoveProjectLike';
import AddShareLike from '../../queries/AddShareLike';
import RemoveShareLike from '../../queries/RemoveShareLike';

import AddTimelineShare from '../../queries/AddTimelineShare';
import AddProjectShare from '../../queries/AddProjectShare';

const PostControls = (props) => {
    let addLike;
    let removeLike;

    let addShare;

    if(props.timelineId) {
        addLike = AddTimelineLike;
        removeLike = RemoveTimelineLike;
    }
    if(props.projectId) {
        addLike = AddProjectLike;
        removeLike = RemoveProjectLike;
    }
    if(props.shareId) {
        addLike = AddShareLike;
        removeLike = RemoveShareLike;
    }

    if(props.shareType=='Timelines') {
        addShare = AddTimelineShare;
    }
    if(props.shareType=='Projects') {
        addShare = AddProjectShare;
    }

    this.LikeItem = compose(
        graphql(addLike, { name: 'addLikeMutate' }),
        graphql(removeLike, { name: 'removeLikeMutate' })
    )(LikeItem);

    this.ShareItem = graphql(addShare)(ShareItem);

    return (
        <div className="PostControls">
            <this.LikeItem timelineId={props.timelineId} projectId={props.projectId} shareId={props.shareId} username={props.username} hasLiked={props.hasLiked} />
            <TriggerComment toggle={props.toggleComments} />
            { /* TODO: is hasShared attr important?  */ }
            <this.ShareItem hasShared={false} username={props.username} sharePostId={props.sharePostId} />
        </div>
    );
}

export default PostControls;
