import React from 'react';
import { graphql, compose } from 'react-apollo';
import { Media, Input, Alert, Button, Form } from 'reactstrap';
import Textarea from "react-textarea-autosize";
import { withCookies } from 'react-cookie';
import classNames from 'classnames';

import Avatar from '../../components/Avatar';
import ProBadge from '../../components/ProBadge';
import { isSubscribed } from '../../services/UserMeta';
import { prefixHash } from '../../services/Text';
import WithInputTimeout from '../../components/WithInputTimeout';
import FollowersFollowing from '../../queries/FollowersFollowing';

const UserEntry = ({ subscribed, profileImages, fullname, professionName }) => {
    return (
        <Media className="UserEntry">
            <Media left href="#">
                <Avatar  filename={profileImages.avatar} />  
            </Media>

            <Media body>
                <Media heading className="fullname-wrapper">
                    {fullname}
                    <div className="online-wrapper"></div>
                    <ProBadge size="medium" isVisible={isSubscribed(subscribed)} />
                </Media>
                <div className="meta">
                    <span className="profession">{prefixHash([professionName])}</span>
                </div>
            </Media>
        </Media>
    );
};

class Conversations extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hidden: false,
            active: 'following',
            following: [],
            followers: [],
        }

        this.handleSearchFiltering = this.handleSearchFiltering.bind(this);
    }

    toggleNav(active) {
        this.setState({
            active
        })
    }

    handleSearchFiltering(value) {
        // reset on empty value
        if (!value) {
            const following = this.followingCache;
            const followers = this.followersCache;

            this.setState({
                following,
                followers
            })
            return;
        }

        // set initial cache values if cache hasn't been set yet
        if (!this.followingCache.length || !this.followersCache.length) {
            this.followersCache = this.state.followers;
            this.followingCache = this.state.following;

        }

        switch(this.state.active) {
            case 'following': 
                this.setState({
                    following: this.state.following.filter(entry => {
                        return entry.fullname.toLowerCase().includes(value.toLowerCase())
                    })
                })
                break;
            case 'followers': 
                this.setState({
                    followers: this.state.followers.filter(entry => {
                        return entry.fullname.toLowerCase().includes(value.toLowerCase())
                    })
                })
                break;
        }
    }

    componentWillReceiveProps(props) {
        const { loading, error, followersFollowing } = props;

        if (loading) {
            return; 
        } else if (error) {
            return;
        }

        this.currentUser = this.props.cookies.get('username');

        const followers = followersFollowing.followers.filter(user => user.username !== this.currentUser)
        const following = followersFollowing.following.filter(user => user.username !== this.currentUser)

        this.followingCache = [];
        this.followersCache = [];

        this.setState({
            following,
            followers
        })
    }

    render() {
        const { loading, error} = this.props;

        if (loading) {
            return <div>Loading</div>
        } else if (error) {
            return <div>{error}</div>
        }

        const { active, following, followers } = this.state;
        const isToggled = (itemName) => active == itemName ? 'active' : '';

        return (
            <div className="FollowingFollowers">
                <ul className="nav">
                    <li className={isToggled('followers')} onClick={() => this.toggleNav('followers')}>
                        Followers
                    </li>
                    <li className={isToggled('following')} onClick={() => this.toggleNav('following')}>
                        Following
                    </li>
                    <li className={isToggled('hide')} onClick={() => this.toggleNav('hide')}>
                        Hide
                    </li>
                </ul>

                <hr />

                <div className="input-wrapper">
                    <WithInputTimeout changeHandler={this.handleSearchFiltering} render={({value, handleChange, handleKeyDown}) =>
                        <Input className="search" type="text" value={this.value} onChange={handleChange} onKeyDown={handleKeyDown} />
                    } />
                    <i className="fas fa-search"></i>
                </div>
                    
                <div className={classNames("users-list", {'hidden' : active == 'hide'})}>
                    <div className={classNames("followers", {'hidden': active !== 'followers'})}>
                        {followers.map((user, i) => <UserEntry {...user} professionName={user.about.profession} key={i} />)}
                    </div>
                    <div className={classNames("following", {'hidden': active !== 'following'})}>
                        {following.map((user, i) => <UserEntry {...user} professionName={user.about.profession} key={i} />)}
                    </div>
                </div>

                <hr />
            </div>
        );
    }
}

export default Conversations;
