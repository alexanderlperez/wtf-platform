import React from 'react';
import Textarea from "react-textarea-autosize";
import { graphql, compose } from 'react-apollo';
import { Button } from 'reactstrap';
import { withCookies } from 'react-cookie';
import SubmitTimelinePost from '../../queries/SubmitTimelinePost';

class TimelineWritePost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            content: '',
            alerts: { 
                failure: false 
            } 
        };

        this.submitPostMutation = props.mutate;
        this.username = props.cookies.get('username');

        this.submitPost = this.submitPost.bind(this);
        this.updatePostContent = this.updatePostContent.bind(this);
    }

    async submitPost(e) {
        try {
            const res = await this.submitPostMutation({variables: { 
                username: this.username, 
                content: this.state.content 
            }})
        } catch (err) {

        }
    }

    updatePostContent(e) {
        this.setState({
            content: e.target.value,
        })
    }

    render() {
        return (
            <div className="TimelineWritePost">
                <Textarea placeholder="Write a post..." minRows={3} onChange={this.updatePostContent} />
                <div className="button-wrapper">
                    <Button outline className="upload-image">Upload Photo</Button>
                    <Button outline disabled={!(this.state.content.length > 0)} className="submit-post" onClick={this.submitPost}>Post</Button>
                </div>
            </div>
        );
    }
}

export default compose(
    graphql(SubmitTimelinePost),
    withCookies,
)(TimelineWritePost);
