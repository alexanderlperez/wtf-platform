import React from 'react';
import _ from 'lodash';
import { 
    Form, 
    FormGroup, 
    Label, 
    Input, 
    Collapse, 
    ListGroup, 
    ListGroupItem,  
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter 
} from 'reactstrap';

import WithClickToggle from '../../components/WithClickToggle';

class SelectButtonView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMessageOpen: false,
            isCallMeOpen: false,
            isShopOpen: false,
            isHireMeOpen: false,
        };
    }

    toggle(key) {
        this.setState((prev) => {
            const allReset = { ..._.omit(prev, 'page') }

            Object.keys(allReset).forEach(key => {
                allReset[key] = false;
            })

            return {
                ...allReset,
                [key]: !this.state[key],
            }
        })
    }


    render() {
        const { selectionToggle, toggle, isVisible, page, pageHandler } = this.props
        const { isMessageOpen, isCallMeOpen, isShopOpen, isHireMeOpen } = this.state;

        const MessageOptions = (props) => {
            return (
                <div className="MessageOptions">
                    <h3>Start a new message</h3>
                </div>
            );
        };

        const CallMeOptions = (props) => {
            return (
                <div className="CallMeOptions">
                    <h3>Start a call to you</h3>
                </div>
            );
        };

        const ShopOpenOptions = (props) => {
            return (
                <div className="ShopOpenOptions">
                    <h3>Take users to your shop</h3>
                    <Input type="url" placeholder="URL" />
                </div>
            );
        };

        const HireMeOptions = (props) => {
            return (
                <div className="Hire">
                    <h3>Start the hiring process</h3>
                </div>
            );
        };

        return (
            <div className="SelectButtonView">
                <ModalHeader toggle={toggle}>Which button do you want people to see?</ModalHeader>
                <ModalBody>
                    <Button outline size="lg" onClick={() => {
                        this.toggle('isMessageOpen')
                        selectionToggle('isMessageOpen')
                    }} block>Message Me</Button>
                    <Collapse isOpen={isMessageOpen}>
                        <MessageOptions />
                    </Collapse>
                    <Button outline size="lg" onClick={() => {
                        this.toggle('isCallMeOpen')
                        selectionToggle('isCallMeOpen')
                    }} block>Call Me</Button>
                    <Collapse isOpen={isCallMeOpen}>
                        <CallMeOptions />
                    </Collapse>
                    <Button outline size="lg" onClick={() => {
                        this.toggle('isShopOpen')
                        selectionToggle('isShopOpen')
                    }} block>Shop</Button>
                    <Collapse isOpen={isShopOpen}>
                        <ShopOpenOptions />
                    </Collapse>
                    <Button outline size="lg" onClick={() => {
                        this.toggle('isHireMeOpen')
                        selectionToggle('isHireMeOpen')
                    }} block>Hire Me</Button>
                    <Collapse isOpen={isHireMeOpen}>
                        <HireMeOptions />
                    </Collapse>
                </ModalBody>
            </div>
        );
    }
}

class EditButtonModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: props.page,
            isMessageOpen: false,
            isCallMeOpen: false,
            isShopOpen: false,
            isHireMeOpen: false,
        }

        this.setPage = this.setPage.bind(this)
        this.toggleOpen = this.toggleOpen.bind(this)
    }

    toggleOpen(key) {
        this.setState((prev) => {
            const allReset = { ..._.omit(prev, 'page') }

            Object.keys(allReset).forEach(key => {
                allReset[key] = false;
            })

            return {
                ...allReset,
                [key]: !this.state[key],
            }
        })
    }

    setPage(num) {
        this.setState({
            page: num
        })
    }

    render() {
        const { toggle, isOpen, setResult } = this.props;
        const { page } = this.state;
        const { toggleOpen, setPage } = this;

        const pages = [ 
            <SelectButtonView toggle={toggle} selectionToggle={toggleOpen} page={page} pageHandler={setPage}/>,
         ];

        const lastPage = pages.length - 1;
        const result = () => {
            let res;

            // 6am hack, get results from looking up by state boolean key
            // - introspection by string names is silly
            // TODO: better handling of passing children rather than props
            Object.keys(this.state)
                .filter((key) => key.includes('Open'))
                .forEach((key) => {
                    if (this.state[key]) {
                        res = { 
                            isMessageOpen: { text: 'Message', action: () => {} },
                            isCallMeOpen: { text: 'Call Me', action: () => {} },
                            isShopOpen: { text: 'Shop', action: () => {} },
                            isHireMeOpen: { text: 'Hire Me', action: () => {} },
                        }[key];
                    }
                })

            return res;
        }

        return (
            <div className="EditButtonModal">
                <Modal isOpen={isOpen} toggle={toggle} className={this.props.className}>
                    {pages[page]}
                    <ModalFooter>
                        {page === 0 && <Button color="secondary" onClick={toggle}>Cancel</Button>}
                        {page >= 1 && <Button color="primary" onClick={() => this.setPage(page - 1)}>Back</Button>}
                        {page < lastPage && <Button color="primary" onClick={() => this.setPage(page + 1)}>Next</Button>}
                        {page === lastPage && <Button color="secondary" onClick={() => setResult(result())}>Finish</Button>}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default EditButtonModal;
