import React from 'react';
import {NavLink as Link} from 'react-router-dom';
import classNames from 'classnames';
import { withCookies } from 'react-cookie';

class ProfileNav extends React.Component {
	constructor(props) {
		super(props)
        this.state = {
            isLoggedIn: this.props.cookies.get('isLoggedIn'),
            loggedInUsername: this.props.cookies.get('username'),
        }
	}

	render() {
        const { loading, error } = this.props.data;

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        const { user } = this.props.data;
        const registerType = user.accountMeta 
            ? user.accountMeta.subscription 
            : "FREE";
        const isPro = registerType == "PRO";
        const { username } = user;
        const { accountUrl } = this.props;
        const { pathname } = window.location;
        const { isLoggedIn, loggedInUsername } = this.state;

        const isActive = pathname.includes('timeline') || pathname.split('/').pop() == '' || pathname.split('/').pop() == username

		return (
			<div className="ProfileNav">
				<ul className="nav-items">
					{ /* TODO: get the correct link urls here using SiteLinks */ }
                    { isLoggedIn && loggedInUsername == username 
                        && <li className="item"><Link className={ classNames({ active: isActive })} to={`${accountUrl}/timeline`}>Timeline</Link></li> }

                    { isPro 
                        ? (<li className="item"><Link className={classNames({ active: pathname.includes('portfolio') })} to={`${accountUrl}/portfolio`}>Portfolio</Link></li>)
                        : (<li className="item"><Link className={classNames({ active: pathname.includes('lookbook') })} to={`${accountUrl}/lookbook`}>LookBook</Link></li>) }
					
					<li className="item"><Link className={classNames({ active: pathname.includes('videos') })} to={`${accountUrl}/videos`}>Videos</Link></li>
					<li className="item"><Link className={classNames({ active: pathname.includes('favorites') })} to={`${accountUrl}/favorites`}>Favorites</Link></li>
					<li className="item"><Link className={classNames({ active: pathname.includes('about') })} to={`${accountUrl}/about`}>About</Link></li>
				</ul>
			</div>
		);
	}
}

export default withCookies(ProfileNav);
