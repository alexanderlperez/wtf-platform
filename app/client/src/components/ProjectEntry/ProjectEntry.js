import React from 'react';
import { graphql, compose } from 'react-apollo';

import GetTimelineLikes from '../../queries/GetTimelineLikes';
import GetProjectShares from '../../queries/GetProjectShares';

import SiteLinks from '../../services/SiteLinks';
import { clampString } from '../../services/Text';

const ProjectEntry = (props) => {
    let coverImagePath = '';
    let testPath = props.coverImage.split(':')[0];
    if(testPath=='http' || testPath=='https' || testPath=='data') {
        coverImagePath = `url(${props.coverImage})`
    } else {
        coverImagePath = `url(${SiteLinks.uploads}/${props.coverImage})`
    }

    const projectStyle = {
        backgroundImage: coverImagePath
    }

    const textLimit = props.textLimit || 240;

    const likes = props.likes ? props.likes.length : 0;
    const shares = props.shares ? props.shares.length : 0;

    return (
        <div className="ProjectEntry img-wrapper" style={projectStyle}>
            <div className="overlay" onClick={props.clickHandler}>
                <span className="heading">{props.title}</span>
                <p className="description">{clampString(props.description, textLimit)}</p>
                <div className="meta">
                    <span className="category">{clampString(props.tags.map(tag => `#${tag}`).join(' '), 50)}</span>
                    <span className="stats">
                        <span>
                            <i className="far fa-heart"></i> {likes}
                        </span>
                        <span>
                            <i className="fas fa-share-square"></i> {shares}
                        </span>
                    </span>
                </div>
            </div>
        </div>
    ); 
}

export default ProjectEntry;
