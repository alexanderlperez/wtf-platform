import React from 'react';
import Slider from 'react-slick';
import prevArrowImg from '../../assets/images/left-slider-control.png';
import nextArrowImg from '../../assets/images/right-slider-control.png';
import prevArrowImgActive from '../../assets/images/left-slider-control-active.png';
import nextArrowImgActive from '../../assets/images/right-slider-control-active.png';
import SiteLinks, { toUploadUrl } from '../../services/SiteLinks';
import {NavLink as Link} from 'react-router-dom';


const ProjectsSlider = (props) => {
    const { loading, error, allProjects } = props.data;
    const { username } = props.user;
    const { handler } = props;

    if (loading) {
        return <div></div>
    } else if (error) {
        return <div>Error {error}</div>
    }

    // TODO: this needs to use Sitelinks once it takes usernames for profile links
    const items = allProjects.map((entry, i) => {
        let coverImagePath = '';
        let testPath = entry.coverImage.split(':')[0];
        if(testPath=='http' || testPath=='https') {
            coverImagePath = entry.coverImage
        } else {
            coverImagePath = toUploadUrl(entry.coverImage)
        }
        return (
            <div key={i} className="slide-wrapper">
                <img onClick={() => handler(entry)} src={coverImagePath} alt="" />
            </div>
        )
    })

    const PrevArrow = (props) => {
        return (
            <span className={(props.className + " PrevArrow")} style={props.style} onClick={props.onClick}>
                <img className="normal" src={prevArrowImg} />
                <img className="active" src={prevArrowImgActive} />
            </span>
        );
    };  

    const NextArrow = (props) => {
        return (
            <span className={(props.className + " NextArrow")} style={props.style} onClick={props.onClick}>
                <img className="normal" src={nextArrowImg} />
                <img className="active" src={nextArrowImgActive} />
            </span>
        );
    };  

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        //TODO: make this responsive
    };

    return (
        <Slider className="ProjectsSlider" {...settings}>
            {items}
        </Slider>
    );
};

export default ProjectsSlider;
