import React from 'react';
import { Media } from 'reactstrap';
import Avatar from '../../components/Avatar';
import ProBadge from '../../components/ProBadge';
import { isSubscribed } from '../../services/UserMeta';
import { prefixHash } from '../../services/Text';

// TODO: unnest this and sibling components
const PostUserHeading = ({ subscribed, avatar, fullname, professionName }) => {
    return (
        <div className="PostUserHeading">
            <Media>
                <Media left href="#">
                    <Avatar proBadge={() => <ProBadge size="small" isVisible={isSubscribed(subscribed)} />} filename={avatar} />  
                </Media>

                <Media body>
                    <Media heading className="fullname">
                        {fullname}
                        <div className="online-wrapper"></div>
                    </Media>
                    <div className="meta">
                        <span className="profession">{prefixHash([ professionName ])}</span>
                    </div>
                </Media>
            </Media>
        </div>
    );
}

export default PostUserHeading;
