import React from 'react';
import { Button, Modal,  ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import classNames from 'classnames';

class AlertModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal 
                className={classNames("AlertModal", this.props.className)}
                backdrop={true} 
                backdropClassName="alert-modal-backdrop" 
                isOpen={this.props.isOpen} 
                toggle={this.props.toggle}>
                <ModalBody>
                    {this.props.children}
                </ModalBody>
            </Modal>
        )
    }
}

export default AlertModal;
