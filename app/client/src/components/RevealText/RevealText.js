import React from 'react';
import { clampString } from '../../services/Text';

const RevealText = ({ className, handler, text, isRevealed, maxLength }) => {
    const classNames = ["RevealText", className].join(' ');

    return (
        <div className={classNames}>
            {isRevealed 
                ?(<p className="revealed">{text}<span onClick={handler}>Close</span></p>)
                :(<p className="shortened">{clampString(text, maxLength)}<span onClick={handler}>Read More</span></p>)}
            </div>
    )
}

export default RevealText;
