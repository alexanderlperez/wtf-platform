import React from 'react';
import RevealText from '../../components/RevealText';

class CommentBody extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isRevealed: false
        }

        this.toggleReveal = this.toggleReveal.bind(this);
    }

    toggleReveal() {
        console.log('called');
        this.setState({
            isRevealed: !this.state.isRevealed,
        })
    }

    render() {
        const maxLength = 250;
        const needsReveal = this.props.text.length > maxLength;

        return (
            <div className="CommentBody">
                <div className="content-wrapper">
                    { needsReveal 
                        ? (<RevealText 
                        isRevealed={this.state.isRevealed}
                        handler={this.toggleReveal}
                        text={this.props.text} 
                        maxLength={maxLength}/>)
                        : (
                            <p>{this.props.text}</p>
                        )
                    }
                    
                </div>
            </div>
        );
    }
};


export default CommentBody;
