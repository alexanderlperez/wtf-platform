import React from 'react';
import { graphql, compose } from 'react-apollo';
import { withCookies } from 'react-cookie';
import CommentSubmission from '../../components/CommentSubmission';
import CommentsList from '../../components/CommentsList';
import Globals from '../../services/Globals';
import GetProjectComments from '../../queries/GetProjectComments';

class CommentsView extends React.Component {
    constructor(props) {
        super(props);

        this.listUpdateHandler = this.listUpdateHandler.bind(this);
    }

    listUpdateHandler(newList) {
        // takes res from comment submission, expecting all the comments back
        // this.setState({
        //     comments: newList
        // })
    }

    render() {
        const { loading, error, comments, cookies } = this.props

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        return (
            <div className="CommentsView">
                <CommentsList comments={comments} />
                <CommentSubmission handler={this.listUpdateHandler} projectId={this.props.project.id} username={cookies.get('username')} />
            </div>
        );
    }

}


export default withCookies(CommentsView);
