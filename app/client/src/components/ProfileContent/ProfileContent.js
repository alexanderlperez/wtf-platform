import React from 'react';

class ProfileContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div className="ProfileContent">
                {this.props.children}
            </div>
        );
    }
}

export default ProfileContent;
