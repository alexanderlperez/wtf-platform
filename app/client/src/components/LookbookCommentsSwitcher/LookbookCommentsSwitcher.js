import React from 'react';
import { graphql, compose } from 'react-apollo';
import { withCookies } from 'react-cookie';
import classNames from 'classnames';

import CommentSubmission from '../../components/CommentSubmission';
import CommentsList from '../../components/CommentsList';

class LookbookCommentsSwitcher extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            active: 'comments',
        }

        this.listUpdateHandler = this.listUpdateHandler.bind(this);
    }

    listUpdateHandler() {
        this.setState({
            active: 'comments'
        })
    }

    toggleNav(active) {
        this.setState({
            active
        })
    }

    render() {
        const { active } = this.state;
        const isToggled = (itemName) => active == itemName ? 'active' : '';
        const { cookies, comments, project } = this.props;

        return (
            <div className="LookbookCommentsSwitcher">
                <ul className="nav">
                    <li className={isToggled('comments')} onClick={() => this.toggleNav('comments')}>
                        Comments
                    </li>
                    <li className={isToggled('submit')} onClick={() => this.toggleNav('submit')}>
                        Leave Comment
                    </li>
                </ul>

                <hr />

                <div className={classNames("comments", {'hidden': active !== 'comments'})}>
                    <CommentsList comments={comments} />
                </div>

                <div className={classNames("submit", {'hidden': active !== 'submit'})}>
                    <CommentSubmission handler={this.listUpdateHandler} projectId={this.props.project.id} username={cookies.get('username')} />
                </div>

                <hr />
            </div>
        );
    }
}

export default withCookies(LookbookCommentsSwitcher);
