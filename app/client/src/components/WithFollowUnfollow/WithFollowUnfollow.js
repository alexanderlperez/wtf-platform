import React from 'react';
import PropTypes from 'prop-types';
import { graphql, compose } from 'react-apollo';

import FollowUser from '../../queries/FollowUser';
import UnfollowUser from '../../queries/UnfollowUser';

// See PropTypes at bottom for expected params

class WithFollowUnfollow extends React.Component {
    static propTypes = {
        loggedInUsername: PropTypes.string.isRequired,
        username: PropTypes.string.isRequired,
        isFollowing: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            isFollowing: props.isFollowing,
        };

        this.onFollowClick = this.onFollowClick.bind(this)
        this.onUnfollowClick = this.onUnfollowClick.bind(this)
        this.toggleFollowing = this.toggleFollowing.bind(this)
    }

    async onUnfollowClick() {

        const { unfollowMutate } = this.props;

        let res;
        try {
            res = await unfollowMutate({ variables: { username: this.props.loggedInUsername, targetname: this.props.username } })
            this.setState({
                isFollowing: false
            });
        } catch (err) {
            console.error(err);
            return;
        }
    }

    async onFollowClick() {

        const { followMutate } = this.props;

        let res;
        try {
            res = await followMutate({ variables: { username: this.props.loggedInUsername, targetname: this.props.username } })
            this.setState({
                isFollowing: true
            });
        } catch (err) {
            console.error(err);
            return;
        }
    }

    async toggleFollowing() {
        if (this.state.isFollowing) {
            await this.onUnfollowClick();
        } else {
            await this.onFollowClick();
        }
    }

    render() {
        const { isFollowing } = this.state

        return (
            <div className="WithFollowUnfollow">
                {this.props.render(this.toggleFollowing, isFollowing)}
            </div>
        );
    }
}

export default compose(
    graphql(FollowUser, { name: 'followMutate' }),
    graphql(UnfollowUser, { name: 'unfollowMutate' })
)(WithFollowUnfollow);
