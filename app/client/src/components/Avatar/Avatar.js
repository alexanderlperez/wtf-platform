import React from 'react';
import SiteLinks from '../../services/SiteLinks';
import classNames from 'classnames';

const Avatar = (props) => {
    let filename;

    // TODO : convert callers to use the props option in graphql() to decouple from apollo
    if (props.data) {
        const { loading, error } = props.data;

        if (!loading && !error) {
            filename = props.data.user.profileImages.avatar
        }
    } else {
        filename = props.filename;
    }

    const { loading, error, user } = props;

    if (loading) {
        return <h1>Loading</h1>
    } else if (error) {
        return <h1>{error}</h1>
    }

    const bgImage = {
        backgroundImage: `url(${SiteLinks.profileUploads}/${filename})`
    };
    const defaultImage = {
        backgroundImage: `url(default.png)`
    };

    return (
        <div className="Avatar">
            { props.proBadge && props.proBadge(user && user.subscribed) }
            <div className="avatar-image" style={(filename && bgImage) || defaultImage}></div>
        </div>
    );
}

export default Avatar;
