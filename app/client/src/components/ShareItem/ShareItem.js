import React from 'react';
import {Button} from 'reactstrap';

class ShareItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasShared: this.props.hasShared
        };
        this.shareItem = this.shareItem.bind(this);
    }

    async shareItem(e) {
        e.preventDefault();

        const { mutate } = this.props;
        
        let res;
        try {
            res = await mutate({
                variables: {
                    username: this.props.username,
                    timelineId: this.props.sharePostId,
                    projectId: this.props.sharePostId
                }
            })
            this.setState({
                hasShared:true
            });
        } catch (err) {
            return;
        }
    }

    render() {
        return (
            <a href="#" className="ShareItem" onClick={this.shareItem}>
                <i className="fas fa-share-square"></i> <span>Share</span>
            </a>
        );
    }
}

export default ShareItem;
