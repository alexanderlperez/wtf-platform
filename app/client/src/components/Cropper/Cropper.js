import React from 'react';

import AvatarEditor from 'react-avatar-editor';
import Slider from 'rc-slider';
import { Button  } from 'reactstrap';

import Uppy from 'uppy/lib/core';
import GoogleDrive from 'uppy/lib/plugins/GoogleDrive';
import Dropbox from 'uppy/lib/plugins/Dropbox';
import Instagram from 'uppy/lib/plugins/Instagram';
import AwsS3 from 'uppy/lib/plugins/AwsS3';
import Dashboard from 'uppy/lib/react/Dashboard';

class Cropper extends React.Component {
    constructor(props) {
        super(props);

        this.editor = null;

        this.state = {
            images:this.props.images || [],
            scale:1,
            rotate:0,
            image:null
        }

        this.uppy = Uppy({
            debug: true,
            autoProceed: true,
            restrictions: {
                maxFileSize: 5242880,
                maxNumberOfFiles: 18,
                minNumberOfFiles: 1,
                allowedFileTypes: ['image/*']
            },
            onBeforeFileAdded: (currentFile, files) => Promise.resolve(),
            onBeforeUpload: (files) => Promise.resolve(),
        })
        // .use(GoogleDrive, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(Dropbox, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(Instagram, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(AwsS3, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        .use(GoogleDrive, { host: 'http://localhost:3000' })
        // .use(Dropbox, { host: 'http://localhost:3000' })
        // .use(Instagram, { host: 'http://localhost:3000' })
        .use(AwsS3, { host: 'http://localhost:3000' })

        this.uppy.run();

        this.uppy.on('complete', result => {
            console.log('successful files:', result.successful)
            console.log('failed files:', result.failed)
            this.handleUpload(result.successful)
        })

        this.uppy.on('upload', () => {
            console.log('Starting upload...')
        })

        this.uppy.on('upload-progress', (file, progress) => {
            // file: { id, name, type, ... }
            // progress: { uploader, bytesUploaded, bytesTotal }
            console.log(file.id, progress.bytesUploaded, progress.bytesTotal)
        })

        this.uppy.on('upload-success', (file, resp, uploadURL) => {
            console.log(file.name, uploadURL)
        })

        this.uppy.on('upload-error', (file, error) => {
            console.log('error with file:', file.id)
            console.log('error message:', error)
        })

        this.uppy.on('error', (error) => {
            console.log('error state:', this.uppy.state.error)
            console.log('error message:', error)
        })

        this.setEditorRef = this.setEditorRef.bind(this);
        this.changeZoom = this.changeZoom.bind(this);
        this.rotateClockwise = this.rotateClockwise.bind(this);
        this.rotateCouterClockwise = this.rotateCouterClockwise.bind(this);
        this.onClickSave = this.onClickSave.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
        this.selectImage = this.selectImage.bind(this);
        this.clearImage = this.clearImage.bind(this);
    }

    changeZoom(data) {
        console.log(this.editor);
        this.setState({
            scale:data
        })
    }

    rotateClockwise() {
        let rotate = this.state.rotate+90;
        if(rotate>270) {
            rotate = 0;
        }
        this.setState({
            rotate:rotate
        })
    }

    rotateCouterClockwise() {
        let rotate = this.state.rotate-90;
        if(rotate<0) {
            rotate = 270;
        }
        this.setState({
            rotate:rotate
        })
    }

    setEditorRef(editor) {
        this.editor = editor;
    }

    handleUpload(files) {
        if(files.length) {
            this.setState({
                image:files[0]
            })
        }
    }

    selectImage(image) {
        this.setState({
            image:image
        })
    }

    clearImage() {
        this.setState({
            image:null,
            scale:1,
            rotate:0
        })
        this.uppy.reset();
    }

    onClickSave() {
        if (this.editor) {
            let canvas = this.editor.getImageScaledToCanvas();
            console.log('canvase get stuff', canvas.toDataURL());
            this.props.onSave({
                coverImage: canvas.toDataURL(this.state.image.meta.type),
                type:this.state.image.meta.type
            })



            // Coordinates before image is rotated
            // {
            //     x:0, // top of image location to crop
            //     y:0, // left of image location to crop
            //     width:0, // multiply this by pixel width of image
            //     height:0 // multiple this by pixel height of image
            // }
            // Graphicsmagick should first crop the image based on provided information above (.crop(width, height, x, y))
            // Then should be resized to 275x275 (Maybe 350x350 for high dpi displays) (.resize(275, 275))
            // Set gravity to center (.gravity('Center'))
            // Last rotate image based on degrees provided (.rotate('#FFF', 0|90|180|270))
        }
    }

    render() {
        if(!this.state.image) {
            const imageItems = this.state.images.map((image, i) => (
                <div className={image.id} key={i} style={{display:'inline-block'}} onClick={() => this.selectImage(image)}>
                    <img src={image.preview}  width="200" />
                </div>
            ));
            return (
                <div className="CropperUploader">
                    <Dashboard
                        uppy={this.uppy}
                        inline={true}
                        note={'1 Photo, 5MB per photo max'}
                        maxHeight={450}
                        metaField={[
                            { id: 'license', name: 'License', placeholder: 'specify license' },
                            { id: 'caption', name: 'Caption', placeholder: 'describe what the image is about' }
                        ]}
                        showProgressDetails={true}
                        hideProgressAfterFinish={true}
                        closeModalOnClickOutside={false}
                        replaceTargetContent={true}
                        hideUploadButton={true}
                        plugins={['GoogleDrive']}
                    />

                    {imageItems}
                </div>
            )
        }
        return (
            <div className="Cropper">
                <AvatarEditor
                    ref={this.setEditorRef}
                    image={URL.createObjectURL(this.state.image.original)}
                    width={275}
                    height={275}
                    border={50}
                    color={[0, 0, 0, 0.6]} // RGBA
                    scale={this.state.scale}
                    rotate={this.state.rotate}
                />
                <Slider
                    min={1}
                    max={4}
                    defaultValue={1}
                    step={0.01}
                    dots={false}
                    dotStyle={null}
                    handleStyle={{
                        borderColor: '#ff0800',
                        borderWidth: 1,
                        height: 11,
                        width: 11,
                        marginLeft: -6,
                        marginTop: -4,
                        backgroundColor: 'white',
                      }}
                    activeDotStyle={null}
                    railStyle={null}
                    onChange={this.changeZoom}
                    trackStyle={{
                      backgroundColor: '#ff0800',
                    }}
                 />
               <div className="btn-wrapper">
                   <Button onClick={this.rotateCouterClockwise}>
                     <i className="fas fa-redo" style={{transform:'scaleX(-1)'}}></i>
                   </Button>
                   <Button onClick={this.rotateClockwise}>
                     <i className="fas fa-redo"></i>
                   </Button>

                   <Button onClick={this.clearImage}>Back</Button>
                   <Button onClick={this.onClickSave}>Save</Button>
               </div>

            </div>
        )
    }
}

export default Cropper;
