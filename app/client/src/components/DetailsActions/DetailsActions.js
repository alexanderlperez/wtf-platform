import React from 'react';
import { Button } from 'reactstrap';
import { withCookies } from 'react-cookie';
import { graphql, compose } from 'react-apollo';

import AddProjectShare from '../../queries/AddProjectShare';
import AddProjectLike from '../../queries/AddProjectLike';
import RemoveProjectLike from '../../queries/RemoveProjectLike';
import AlertModal from '../../components/AlertModal';

const AddToButton = (props) => {
    return (
        <span className="AddToButton"><i className="fas fa-plus-circle"></i>Add To</span>
    );
}

class ShareButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasShared: this.props.hasShared,
            isOpen: false,
        };
        this.shareItem = this.shareItem.bind(this);
    }

    async shareItem(e) {
        e.preventDefault();

        console.log('sharing');

        const { mutate } = this.props;
        
        let res;
        try {
            res = await mutate({ variables: { username: this.props.username, projectId: this.props.projectId } })
            this.setState({
                hasShared:true,
                isOpen: true
            });

            setTimeout(() => {
                this.setState({
                    isOpen: false
                })
            }, 2000)
        } catch (err) {
            return;
        }
    }

    render() {
        return (
            <a href="#" className="ShareItem" onClick={this.shareItem}>
                <i className="fas fa-share-square"></i>Share
                { this.props.showAlert &&
                    <AlertModal isOpen={this.state.isOpen}>
                        Shared
                    </AlertModal>
                }
            </a>
        );
    }
}

class LikeButton extends React.Component {
    constructor(props) {
        super(props);


        this.likeItem = this.likeItem.bind(this);
        this.unlikeItem = this.unlikeItem.bind(this);
    }

    async likeItem(e) {
        console.log('liked')
        e && e.preventDefault();
        const { addLikeMutate } = this.props;
        
        let res;
        try {
            res = await addLikeMutate({
                variables: {
                    username: this.props.username,
                    projectId: this.props.projectId
                }
            })
        } catch (err) {
            return;
        }
    }

    async unlikeItem(e) {
        console.log('unlike');
        e && e.preventDefault();
        const { removeLikeMutate } = this.props;
        
        let res;
        try {
            res = await removeLikeMutate({
                variables: {
                    username: this.props.username,
                    projectId: this.props.projectId
                }
            })
        } catch (err) {
            return;
        }
    }

    render() {
        return (
            this.props.hasLiked ? (
                <Button className="LikeButton" onClick={this.unlikeItem} color={this.props.color || "danger"}><i className="far fa-heart"></i>Unlike</Button>
            ) : (
                <Button className="LikeButton" onClick={this.likeItem} color={this.props.color || "danger"}><i className="far fa-heart"></i>Like</Button>                
            )
        );
    }

}

class DetailsActions extends React.Component {
    constructor(props) {
        super(props);

        this.LikeButton = compose(
            graphql(AddProjectLike, { name: 'addLikeMutate' }),
            graphql(RemoveProjectLike, { name: 'removeLikeMutate' })
        )(LikeButton);

        this.ShareButton = graphql(AddProjectShare)(ShareButton);
    }
    render() {
        return (
            <div className="DetailsActions">
                <div className="social-wrapper">
                    <span><i className="fab fa-google-plus-square"></i></span>
                    <span><i className="fab fa-facebook-square"></i></span>
                    <span><i className="fab fa-twitter-square"></i></span>
                </div>
    
                <div className="button-wrapper">
                    <AddToButton />
                    <this.ShareButton showAlert={true} projectId={this.props.projectId} username={this.props.cookies.get('username')} />
                    <this.LikeButton projectId={this.props.projectId} username={this.props.cookies.get('username')} hasLiked={this.props.hasLiked} />
                </div>
            </div>
        );
    }

}

export default withCookies(DetailsActions);
