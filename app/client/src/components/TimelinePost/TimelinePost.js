import React from 'react';
import { graphql, compose, withApollo } from 'react-apollo';
import { withCookies } from 'react-cookie';
import * as stream from 'getstream';
import classNames from 'classnames';

import SiteLinks, { toUploadUrl } from '../../services/SiteLinks';
import withUser from '../../hocs/withUser';

import PostControls from '../../components/PostControls';
import PostMeta from '../../components/PostMeta';
import CommentHeading from '../../components/CommentHeading';
import CommentsList from '../../components/CommentsList';
import PostCommentsSubmission from '../../components/PostCommentsSubmission';
import Avatar from '../../components/Avatar';

import AddTimelineComment from '../../queries/AddTimelineComment';
import AddProjectComment from '../../queries/AddProjectComment';
import AddShareComment from '../../queries/AddShareComment';
import GetTimelineComments from '../../queries/GetTimelineComments';
import GetProjectComments from '../../queries/GetProjectComments';
import GetTimelineLikes from '../../queries/GetTimelineLikes';

class ProjectTemplate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openComments: false,
            comments: this.props.post.comments || [],
            likes: this.props.post.likes || []
        };

        this.toggleComments = this.toggleComments.bind(this);

        this.PostCommentsSubmission = graphql(AddProjectComment)(PostCommentsSubmission)

        var feed = this.props.getstreamClient.feed(
            "timeline",
            this.props.post.object._id,
            this.props.post.object.token
        );
        feed.subscribe((response) => {
            console.log('project update: '+this.props.post.object._id, response);

            this.props.client.query({
                query: GetProjectComments,
                variables: {
                    projectId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    comments: res.data.projectComments
                })
            });

            this.props.client.query({
                query: GetTimelineLikes,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    likes: res.data.timelineLikes
                })
            });

        }).then((success) => {
            console.log('success connection project: '+this.props.post.object._id, success)
        }, (error) => {
            console.log('error connection', error);
        })

        this.CurrentUser = withUser(props.username);
        this.onComments = this.onComments.bind(this);
    }

    toggleComments() {

        this.setState({
            openComments: !this.state.openComments
        })
    }

    onComments(res) {
        this.setState({
            comments: res.data.addProjectComment
        })
    }

    render() {
        const { user, description, images, title, tags, category, createdAt, createdOn, coverImage, _id } = this.props.post.object;
        const { profileImages, accountMeta, fullname, about } = user;
        const { openComments, comments } = this.state;

        const showCommentsSubmission = openComments || comments.length;

        const hasLiked = !!this.state.likes.filter((like) => {
            return like.user.username == this.props.username
        }).length

        const meta = {
            avatar: profileImages.avatar, 
            subscribed: accountMeta.subscription, 
            fullname, 
            profession: about.profession,
            likes: this.state.likes.length, 
            comments: this.state.comments.length
        };

        const hasCoverImage = !!coverImage;

        let coverImagePath = '';
        if(coverImage) {
            let testPath = coverImage.split(':')[0];
            if(testPath=='http' || testPath=='https' || testPath=='data') {
                coverImagePath = `${coverImage}`
            } else {
                coverImagePath = `${SiteLinks.uploads}/${coverImage}`
            }
        }



        return (
            <div className={classNames("TimelinePost", { 'no-cover': !hasCoverImage })}>
                <div className="post-wrapper">
                    <h5>Project</h5>
                    <div className="border-wrapper">
                        <div className="image">
                            <img src={coverImagePath} alt="" />
                        </div>
                    
                        <div className="content">
                            <PostMeta {...meta} />
                    
                            <hr />
                    
                            <div className="PostContent">
                                {description}
                            </div>
                        </div>
                    </div>
                    
                    <div className="controls">
                        <PostControls toggleComments={this.toggleComments} shareType={'Projects'} sharePostId={_id} projectId={_id} hasLiked={hasLiked} username={this.props.username}  />

                        <div className="PostComments">
                            <CommentsList comments={comments || []} />

                            <div className="visibility-wrapper" style={{display: showCommentsSubmission ? 'block' : 'none'}}>
                                <div className="content-wrapper">
                                    <this.CurrentUser render={user => 
                                        <Avatar filename={user.profileImages.avatar} />
                                    } />
                                    <this.PostCommentsSubmission projectId={_id} username={this.props.username} onCommentsSubmission={this.onComments} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class ProjectShareTemplate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openComments: false,
            comments: this.props.post.comments || [],
            likes: this.props.post.likes || []
        };

        this.toggleComments = this.toggleComments.bind(this);

        this.PostCommentsSubmission = graphql(AddShareComment)(PostCommentsSubmission)

        var feed = this.props.getstreamClient.feed(
            "timeline",
            this.props.post.object._id,
            this.props.post.object.token
        );
        feed.subscribe((response) => {
            console.log('project update: '+this.props.post.object._id, response);

            this.props.client.query({
                query: GetTimelineComments,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    comments: res.data.timelineComments
                })
            });

            this.props.client.query({
                query: GetTimelineLikes,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    likes: res.data.timelineLikes
                })
            });

        }).then((success) => {
            console.log('success connection project: '+this.props.post.object._id, success)
        }, (error) => {
            console.log('error connection', error);
        })

        this.CurrentUser = withUser(props.username);
        this.onComments = this.onComments.bind(this);
    }

    toggleComments() {

        this.setState({
            openComments: !this.state.openComments
        })
    }

    onComments(res) {
        this.setState({
            comments: res.data.addShareComment
        })
    }

    render() {
        const { user, description, images, title, tags, category, createdAt, createdOn, _id } = this.props.post.object.item;
        const coverImage = this.props.post.object.item.cover_image || this.props.post.object.item.coverImage ;
        const shareId = this.props.post.object._id;
        const { profileImages, accountMeta, fullname, about } = this.props.post.object.owner;
        const shareFullname = this.props.post.object.user.fullname;
        const { openComments, comments } = this.state;

        const showCommentsSubmission = openComments || comments.length;

        const hasLiked = !!this.state.likes.filter((like) => {
            return like.user.username == this.props.username
        }).length

        const meta = {
            avatar: profileImages.avatar, 
            subscribed: accountMeta.subscription, 
            fullname, 
            profession: about.profession,
            likes: this.state.likes.length, 
            comments: this.state.comments.length
        };

        const hasCoverImage = !!coverImage;
        
        let coverImagePath = '';
        if(coverImage) {
            let testPath = coverImage.split(':')[0];
            if(testPath=='http' || testPath=='https' || testPath=='data') {
                coverImagePath = `${coverImage}`
            } else {
                coverImagePath = `${SiteLinks.uploads}/${coverImage}`
            }
        }

        return (
            <div className={classNames("TimelinePost", { 'no-cover': !hasCoverImage })}>
                <div className="post-wrapper">
                    <h5>Project shared by {shareFullname}</h5>
                    <div className="border-wrapper">
                        <div className="image">
                            <img src={coverImagePath} alt="" />
                        </div>
                    
                        <div className="content">
                            <PostMeta {...meta} />
                    
                            <hr />
                    
                            <div className="PostContent">
                                {description}
                            </div>
                        </div>
                    </div>
                    
                    <div className="controls">
                        <PostControls toggleComments={this.toggleComments} shareType={'Projects'} sharePostId={_id} shareId={shareId} hasLiked={hasLiked} username={this.props.username} />

                        <div className="PostComments">
                            <CommentsList comments={comments || []} />

                            <div className="visibility-wrapper" style={{display: showCommentsSubmission ? 'block' : 'none'}}>
                                <div className="content-wrapper">
                                    <this.CurrentUser render={user => 
                                        <Avatar filename={user.ProfileImages.avatar} />
                                    } />
                                    <this.PostCommentsSubmission onCommentsSubmission={this.onComments} shareId={shareId} username={this.props.username} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class TimelineTemplate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openComments: false,
            comments: this.props.post.comments || [],
            likes: this.props.post.likes || []
        };

        this.toggleComments = this.toggleComments.bind(this);

        this.PostCommentsSubmission = graphql(AddTimelineComment)(PostCommentsSubmission)

        var feed = this.props.getstreamClient.feed(
            "timeline",
            this.props.post.object._id,
            this.props.post.object.token
        );
        feed.subscribe((response) => {
            console.log('timeline update: '+this.props.post.object._id, response);

            this.props.client.query({
                query: GetTimelineComments,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    comments: res.data.timelineComments
                })
            });

            this.props.client.query({
                query: GetTimelineLikes,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    likes: res.data.timelineLikes
                })
            });

        }).then((success) => {
            console.log('success connection timeline: '+this.props.post.object._id, success)
        }, (error) => {
            console.log('error connection', error);
        })

        this.CurrentUser = withUser(props.username);
        this.onComments = this.onComments.bind(this);
    }

    toggleComments() {
        this.setState({
            openComments: !this.state.openComments
        })
    }

    onComments(res) {
        this.setState({
            comments: res.data.addTimelineComment
        })
    }

    render() {
        console.log('timeline render called', this.props)
        const { user, content, cover, createdAt, _id } = this.props.post.object;
        const { profileImages, accountMeta, fullname, about } = user;
        const { openComments, comments } = this.state;

        const showCommentsSubmission = openComments || comments.length;

        const hasLiked = !!this.state.likes.filter((like) => {
            return like.user.username == this.props.username
        }).length

        const meta = {
            avatar: profileImages.avatar, 
            subscribed: accountMeta.subscription, 
            fullname, 
            profession: about.profession,
            likes: this.state.likes.length, 
            comments: this.state.comments.length
        };

        const hasCoverImage = false;

        return (
            <div className={classNames("TimelinePost", { 'no-cover': !hasCoverImage })}>
                <div className="post-wrapper">
                    <div className="border-wrapper">
                        <div className="image">
                            <img src={cover} alt="" />
                        </div>
                    
                        <div className="content">
                            <PostMeta {...meta} />
                    
                            <hr />
                    
                            <div className="PostContent">
                                {content}
                            </div>
                        </div>
                    </div>
                    
                    <div className="controls">
                        <PostControls toggleComments={this.toggleComments} shareType={'Timelines'} sharePostId={_id} timelineId={_id} hasLiked={hasLiked} username={this.props.username} />

                        <div className="PostComments">
                            <div className="visibility-wrapper" style={{display: comments.length ? 'block' : 'none'}}>
                                <hr />
                                <CommentsList comments={comments || []} />
                            </div>

                            <div className="visibility-wrapper" style={{display: showCommentsSubmission ? 'block' : 'none'}}>
                                <div className="content-wrapper">
                                    <this.CurrentUser render={user => 
                                        <Avatar filename={user.profileImages.avatar} />
                                    } />
                                    <this.PostCommentsSubmission onCommentsSubmission={this.onComments} timelineId={_id} username={this.props.username} />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

class TimelineShareTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openComments: false,
            comments: this.props.post.comments || [],
            likes: this.props.post.likes || [],
        };

        this.toggleComments = this.toggleComments.bind(this);

        this.PostCommentsSubmission = graphql(AddShareComment)(PostCommentsSubmission)

        var feed = this.props.getstreamClient.feed(
            "timeline",
            this.props.post.object._id,
            this.props.post.object.token
        );
        feed.subscribe((response) => {
            console.log('timeline update: '+this.props.post.object._id, response);

            this.props.client.query({
                query: GetTimelineComments,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    comments: res.data.timelineComments
                })
            });

            this.props.client.query({
                query: GetTimelineLikes,
                variables: {
                    timelineId: this.props.post.object._id
                },
                fetchPolicy:'network-only'
            }).then(res => {
                this.setState({
                    likes: res.data.timelineLikes
                })
            });

        }).then((success) => {
            console.log('success connection timeline: '+this.props.post.object._id, success)
        }, (error) => {
            console.log('error connection', error);
        })

        this.CurrentUser = withUser(props.username);
        this.onComments = this.onComments.bind(this);
    }

    toggleComments() {

        this.setState({
            openComments: !this.state.openComments
        })
    }

    onComments(res) {
        this.setState({
            comments: res.data.addShareComment
        })
    }

    render() {
        const { content, cover, likes, likesTotal, commentsTotal, createdAt, _id, coverImage } = this.props.post.object.item;
        const shareId = this.props.post.object._id;
        const { profileImages, accountMeta, fullname, about } = this.props.post.object.owner;
        const shareFullname = this.props.post.object.user.fullname;
        const { openComments, comments } = this.state;

        const showCommentsSubmission = openComments || comments.length;

        const hasLiked = !!this.state.likes.filter((like) => {
            return like.user.username == this.props.username
        }).length

        const meta = {
            avatar: profileImages.avatar, 
            subscribed: accountMeta.subscription, 
            fullname, 
            profession: about.profession,
            likes: this.state.likes.length, 
            comments: this.state.comments.length
        };

        const hasCoverImage = !!coverImage;

        return (
            <div className={classNames("TimelinePost", { 'no-cover': !hasCoverImage })}>
                <div className="post-wrapper">
                    <h5>Shared by {shareFullname}</h5>
                    <div className="border-wrapper">
                        <div className="image">
                            <img src={cover} alt="" />
                        </div>
                    
                        <div className="content">
                            <PostMeta {...meta} />
                    
                            <hr />
                    
                            <div className="PostContent">
                                {content}
                            </div>
                        </div>
                    </div>
                    
                    <div className="controls">
                        <PostControls toggleComments={this.toggleComments} shareType={'Timelines'} sharePostId={_id} shareId={shareId} hasLiked={hasLiked} username={this.props.username} />
                        
                        <hr />

                        <div className="PostComments">
                            <CommentsList comments={comments || []} />

                            <div className="visibility-wrapper" style={{display: showCommentsSubmission ? 'block' : 'none'}}>
                                <div className="content-wrapper">
                                    <this.CurrentUser render={user => 
                                        <Avatar filename={user.profileImages.avatar} />
                                    } />
                                    <this.PostCommentsSubmission onCommentsSubmission={this.onComments} shareId={shareId} username={this.props.username} />
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}

class TimelinePost extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const { loading, error, post } = this.props;
        const { foreign_id } = post;
        const { col } = post.object;

        const templateType = foreign_id.split(":")[0];

        if (loading) {
            return <h1>Loading</h1>
        } else if (error) {
            return <h1>{error}</h1>
        }

        switch(templateType) {
            case 'Projects':
                return (<ProjectTemplate client={this.props.client} getstreamClient={this.props.getstreamClient} post={post} username={this.props.cookies.get('username')} />);
            case 'Timelines':
                return (<TimelineTemplate client={this.props.client} getstreamClient={this.props.getstreamClient} post={post} username={this.props.cookies.get('username')} />);
            case 'Shares':
                switch(col) {
                    case 'Timelines':
                        return (<TimelineShareTemplate client={this.props.client} getstreamClient={this.props.getstreamClient} post={post} username={this.props.cookies.get('username')} />);
                    case 'Projects':
                        return (<ProjectShareTemplate client={this.props.client} getstreamClient={this.props.getstreamClient} post={post} username={this.props.cookies.get('username')} />);                    
                }
                break;
            default:
                return (<TimelineTemplate client={this.props.client} getstreamClient={this.props.getstreamClient} post={post} username={this.props.cookies.get('username')} />);
        }

    }
}

export default compose(
    withCookies,
    withApollo
)(TimelinePost);
