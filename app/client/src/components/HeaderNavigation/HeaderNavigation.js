import React from 'react';
import Media from 'react-media';
import { withCookies } from 'react-cookie';

import classNames from 'classnames';
import deepmerge from 'deepmerge';

import DesktopNavChoices from '../../components/DesktopNavChoices';
import NavIcons from '../../components/NavIcons';
import LoginButtons from '../../components/LoginButtons';

import LogoIconRed from '../../assets/images/wtf-logo-icon-red.png';
import LogoFullBlack from '../../assets/images/wtf-logo-full-black.png';

class HeaderNavigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSearchVisible: false,
            isLoggedIn: this.props.cookies.get('isLoggedIn'),
        };

        this.toggleSearchVisibility = this.toggleSearchVisibility.bind(this);
    }

    toggleSearchVisibility(e) {
        this.setState({
            isSearchVisible: !this.state.isSearchVisible,
        })
    }

    render() {
        const Logo = () => {
            return (
                <div className="Logo">
                    <img className="mobile" src={LogoIconRed} alt="" />
                    <img className="non-mobile" src={LogoFullBlack} alt="" />
                </div>
            );
        };

        const DesktopSearch = () => {
            const toggledNames = classNames({ 
                'search-revealed': this.state.isSearchVisible,
                'search-hidden': !this.state.isSearchVisible,
            });

            return (
                <div className="DesktopSearch">
                    <input type="text" className={toggledNames} />
                </div>
            );
        };

        const isLoggedIn = this.state.isLoggedIn;
        const navIconsOnAuth = isLoggedIn && (<NavIcons username={this.props.cookies.get('username')}/>)
        const loginButtonsNoAuth = !isLoggedIn && (<LoginButtons />)
        const classes = classNames('StyleWrapper', { 'no-auth': !isLoggedIn });

        return (
            <div className="HeaderNavigation">
                <div className={classes}>
                    <Logo />
                    <DesktopNavChoices />
                    <DesktopSearch />
                    {navIconsOnAuth}
                    {loginButtonsNoAuth}
                </div>
            </div>
        );
    }
}

export default withCookies(HeaderNavigation);
