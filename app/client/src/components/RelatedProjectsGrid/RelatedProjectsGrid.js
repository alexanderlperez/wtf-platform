import React from 'react';
import ProjectEntry from '../../components/ProjectEntry';
import { CSSGrid, measureItems, makeResponsive } from 'react-stonecutter';
import SiteLinks, { toUploadUrl } from '../../services/SiteLinks';
import {NavLink as Link} from 'react-router-dom';

// TODO: merge w/ Projects grid, make configurable, with sane defaults

const Grid = makeResponsive(
    measureItems(CSSGrid), 
    { maxWidth: 700, minPadding: 0 }
);

const gridProps = {
    duration: 0,
    component: "ul",
    columns: 4,
    columnWidth: 218,
    itemHeight: 218,
    gutterWidth: 15,
    gutterHeight: 15,
};

const RelatedProjectsGrid = (props) => {
    const { projects, handler, username } = props;

    // Each ProjectEntry's internal overlay will deal with toggling WrapWithModal
    // - clicking a grid item will fire the handler
    // - the handler is passed the project

    const gridItems = projects.map((proj, i) => (
        <li className="entry-wrapper" key={i}>
            <ProjectEntry textLimit={80} clickHandler={() => handler(proj)} {...proj} />
        </li>
    ));

    return (
        <Grid {...gridProps}>{gridItems}</Grid>
    );
}

export default RelatedProjectsGrid;
