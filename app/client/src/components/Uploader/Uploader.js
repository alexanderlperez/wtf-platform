import React from 'react';

import Uppy from 'uppy/lib/core';
import GoogleDrive from 'uppy/lib/plugins/GoogleDrive';
import Dropbox from 'uppy/lib/plugins/Dropbox';
import Instagram from 'uppy/lib/plugins/Instagram';
import AwsS3 from 'uppy/lib/plugins/AwsS3';
import Plugin from 'uppy/lib/core/Plugin';
import ProgressBar from 'uppy/lib/react/ProgressBar';
import Dashboard from 'uppy/lib/react/Dashboard';
import ThumbnailGenerator from 'uppy/lib/plugins/ThumbnailGenerator';
import watermark from 'watermarkjs';
import EXIF from 'exif-js';

class WatermarkPlugin extends Plugin {
    constructor(uppy, opts) {
        super(uppy, opts);
        console.log('what is opts', opts);
        this.id = opts.id || 'WatermarkPlugin';
        this.type = 'modifier';
        this.getState = this.opts.getState;
        this.addWatermark = this.addWatermark.bind(this);
    }

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }

    textManipulation(username, position) {

        let translationRatios = {
            center: {
                x:1/2,
                y:1/2
            },
            topleft: {
                x:1/3,
                y:1/3
            },
            topright: {
                x:2/3,
                y:1/3
            },
            bottomleft:{
                x:1/3,
                y:2/3
            },
            bottomright:{
                x:2/3,
                y:2/3
            }
        }

        return (target) => {
            let context = target.getContext('2d');

            let fontSize = Math.round(target.width*0.06);

            context.globalAlpha = 0.6;
            context.fillStyle = '#fff';
            context.font = fontSize+'px Josefin Slab';

            let metrics = context.measureText(username);

            let x = Math.round(target.width * translationRatios[position].x) - Math.round(metrics.width/2);
            let y = Math.round(target.height * translationRatios[position].y) - Math.round(fontSize/2);
            
            console.log(target, translationRatios[position], metrics, x, y);

            context.translate(x, y);
            context.fillText(username, 0, 0)

            return target;
        }
    }

    compressImage(image, quality, type) {
        let canvas = document.createElement('canvas');

        canvas.width = image.width;
        canvas.height = image.height;

        let ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0);
        return canvas.toDataURL(type, quality)
    }

    addWatermark(fileIDs) {
        let stateOptions = this.getState();
        if(!stateOptions.watermark) {
            fileIDs.forEach((fileID) => {
                let file = uppy.getFile(fileID);
                this.uppy.setFileState(file.id, {
                    original: file.data
                })
            })
            return Promise.resolve();
        }
        let files = fileIDs.map((fileID) => {
            return uppy.getFile(fileID);
        })

        return Promise.all(files.map((file) => {
            return watermark([file.data])
                    .image(this.textManipulation(stateOptions.username, stateOptions.position))
                    .then(img => {
                        let newFile = this.dataURLtoFile(this.compressImage(img, 0.8, file.meta.type), file.name);
                        this.uppy.setFileState(file.id, {
                            data: newFile,
                            original: file.data
                        })
                        return Promise.resolve()
                    })
        })).then((fileDataUrls) => {
            return Promise.resolve();
        })
    }
    install() {
        this.uppy.addPreProcessor(this.addWatermark)
    }
    uninstall() {
        this.uppy.addPreProcessor(this.addWatermark)
    }
}

class ExifOrientPlugin extends ThumbnailGenerator {
    constructor(uppy, opts) {
        super(uppy, opts);

        this.id = opts.id || 'ExifOrientPlugin';
        this.type = 'modifier';

        this.rotateImages = this.rotateImages.bind(this);
    }

    dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }

    resetOrientation(fileData, fileName, orientation, type) {
        return new Promise((resolve, reject) => {
            var img = new Image();
            
            img.onload = () => {
                var width = img.width,
                    height = img.height,
                    canvas = document.createElement('canvas'),
                    ctx = canvas.getContext("2d");

                // set proper canvas dimensions before transform & export
                if (4 < orientation && orientation < 9) {
                    canvas.width = height;
                    canvas.height = width;
                } else {
                    canvas.width = width;
                    canvas.height = height;
                }

                // transform context before drawing image
                switch (orientation) {
                    case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
                    case 3: ctx.transform(-1, 0, 0, -1, width, height ); break;
                    case 4: ctx.transform(1, 0, 0, -1, 0, height ); break;
                    case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
                    case 6: ctx.transform(0, 1, -1, 0, height , 0); break;
                    case 7: ctx.transform(0, -1, -1, 0, height , width); break;
                    case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
                    default: break;
                }

                // draw image
                ctx.drawImage(img, 0, 0);

                // export base64
                let newFile = this.dataURLtoFile(canvas.toDataURL(type, 0.8), fileName);
                resolve(newFile);
            };

            img.src = URL.createObjectURL(fileData);
        })

    };

    rotateImages(file) {
        var fr = new FileReader();
        fr.onload = () => {
            let tags = EXIF.readFromBinaryFile(fr.result);
            this.resetOrientation(file.data, file.name, tags['Orientation'], file.meta.type).then(newFile => {
                this.uppy.setFileState(file.id, {
                    data: newFile
                })
                this.createThumbnail(this.uppy.getFile(file.id), this.opts.thumbnailWidth)
                    .then(preview => {
                        this.setPreviewURL(file.id, preview)
                    })
                    .catch(err => {
                        console.warn(err.stack || err.message)
                    })
            })
        };
        fr.readAsArrayBuffer(file.data);
    }

    install() {
        this.uppy.on('file-added', this.rotateImages)
    }
    uninstall() {
        this.uppy.off('file-added', this.rotateImages)
    }
}

class Uploader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            maxFileLimit: this.props.maxFileLimit || 18,
            note: this.props.note ? this.props.note : '18 Photos, 5MB per photo max'
        }

        this.uppy = Uppy({
            debug: true,
            autoProceed: false,
            restrictions: {
                maxFileSize: 5242880,
                maxNumberOfFiles: this.state.maxFileLimit,
                minNumberOfFiles: 1,
                allowedFileTypes: ['image/*']
            },
            onBeforeFileAdded: (currentFile, files) => Promise.resolve(),
            onBeforeUpload: (files) => {
                console.log('uppy files', files);
                return Promise.resolve();
            }
        })
        // .use(GoogleDrive, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(Dropbox, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(Instagram, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        // .use(AwsS3, { host: 'http://ec2-18-217-76-103.us-east-2.compute.amazonaws.com:3000' })
        .use(GoogleDrive, { host: 'http://localhost:3000' })
        .use(Dropbox, { host: 'http://localhost:3000' })
        .use(Instagram, { host: 'http://localhost:3000' })
        .use(ExifOrientPlugin, {
            thumbnailWidth: 280
        })
        .use(WatermarkPlugin, {
            getState: () => {
                console.log('what is state', this.props.username, this.props.watermark)
                return {
                    username: this.props.username || '',
                    position: this.props.position,
                    watermark: this.props.watermark=="yes" || false
                }
            }
        })
        .use(AwsS3, { host: 'http://localhost:3000' })

        this.uppy.run();

        if(this.props.onLoad)
            this.props.onLoad(uppy);

        this.uppy.on('complete', result => {
            console.log('successful files:', result.successful)
            console.log('failed files:', result.failed)
            if(this.props.handleUploadComplete) {
                this.props.handleUploadComplete(result.successful)
            }
            if(this.props.handleUploadFails) {
                this.props.handleUploadFails(result.failed);
            }
        })

        this.uppy.on('file-added', (file) => {
            console.log('Add file', file)
            if(this.props.handleAdd) {
                this.props.handleAdd(file)
            }
        })

        this.uppy.on('file-removed', (file) => {
            console.log('Removed file', file)
            if(this.props.handleRemoval) {
                this.props.handleRemoval(file)
            }
        })

        this.uppy.on('upload', (data) => {
            console.log('Starting upload...', data)
            if(this.props.handleUploadStart)
                this.props.handleUploadStart(data)
        })

        this.uppy.on('upload-progress', (file, progress) => {
            // file: { id, name, type, ... }
            // progress: { uploader, bytesUploaded, bytesTotal }
            console.log(file.id, progress.bytesUploaded, progress.bytesTotal)
        })

        this.uppy.on('upload-success', (file, resp, uploadURL) => {
            console.log('upload successful', file.id)
            if(this.props.handleUploadSuccess)
                this.props.handleUploadSuccess(file)
        })
        
        this.uppy.on('upload-error', (file, error) => {
            console.log('error with file:', file.id)
            console.log('error message:', error)
        })

        this.uppy.on('error', (error) => {
            console.log('error state:', this.uppy.state.error)
            console.log('error message:', error)
        })
    }
    render() {
        return (
            <div>
                <Dashboard
                    uppy={this.uppy}
                    inline={true}
                    note={this.state.note}
                    maxHeight={450}
                    showProgressDetails={true}
                    hideProgressAfterFinish={true}
                    closeModalOnClickOutside={false}
                    replaceTargetContent={true}
                    hideUploadButton={true}
                    plugins={['GoogleDrive']}
                />
            </div>
        )
    }
}

export default Uploader;
