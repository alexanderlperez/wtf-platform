import React from 'react';

class WithHoverToggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggled: false,
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle(bool) {
        this.setState({
            isToggled: bool
        })
    }

    render() {
        const { component = null, children } = this.props;

        return (
            <div className="WithHoverToggle">
                <div onMouseEnter={() => this.toggle(true)} onMouseLeave={() => this.toggle(false)}>
                    {component}
                </div>

                {children(this.state.isToggled, this.toggle)}
            </div>
        );
    }
}

export default WithHoverToggle;
