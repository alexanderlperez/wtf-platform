import React from 'react';
import SiteLinks from '../../services/SiteLinks';
import times from 'lodash.times';
import ProjectEntry from '../../components/ProjectEntry';
import RelatedProjectsGrid from '../../components/RelatedProjectsGrid';

// NOTE: this component expects to be wrapped w/ graphql(...)

const RelatedProjects = (props) => {
    const { username, handler } = props;
    const { loading, error, getRelatedProjects: projects } = props.data;
    
    if (loading) {
        return <div></div>
    } else if (error) {
        return <div>{error}</div>
    }

    return (
        <div className="RelatedProjects">
            <div className="heading">Related Projects</div>
            <hr />
            <div className="content-wrapper">
                <RelatedProjectsGrid projects={projects} handler={handler} username={username} />
            </div>
        </div>
    );
};

export default RelatedProjects;
