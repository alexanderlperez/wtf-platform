import React from 'react';
import { Button } from 'reactstrap';
import { graphql, compose } from 'react-apollo';
// TODO: this needs to support liking a project, lookbook, or timeline post

class LikeItem extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            hasLiked: this.props.hasLiked
        }

        this.likeItem = this.likeItem.bind(this);
        this.unlikeItem = this.unlikeItem.bind(this);
    }

    async likeItem(e) {
        e && e.preventDefault();
        const { addLikeMutate } = this.props;
        
        let res;
        try {
            res = await addLikeMutate({
                variables: {
                    username: this.props.username,
                    timelineId: this.props.timelineId,
                    projectId: this.props.projectId,
                    shareId: this.props.shareId
                }
            })
            this.setState({
                hasLiked:true
            });
        } catch (err) {
            return;
        }
    }

    async unlikeItem(e) {
        console.log('unlike');
        e && e.preventDefault();
        const { removeLikeMutate } = this.props;
        
        let res;
        try {
            res = await removeLikeMutate({
                variables: {
                    username: this.props.username,
                    timelineId: this.props.timelineId,
                    projectId: this.props.projectId,
                    shareId: this.props.shareId
                }
            })
            this.setState({
                hasLiked:false
            });
        } catch (err) {
            return;
        }
    }

    render() {

        return (
            this.state.hasLiked ? (
                <a href="#" className="LikeItem" onClick={this.unlikeItem}>
                    <i className="far fa-heart"></i> Unlike
                </a>
            ) : (
                <a href="#" className="LikeItem" onClick={this.likeItem}>
                    <i className="far fa-heart"></i> Like
                </a>
            )
            
        );
    }
}

export default LikeItem;
