#!/bin/bash

NETWORK_NAME=$1
CONTAINER_NAME=$2

docker network inspect $NETWORK_NAME \
    | jq ".[].Containers[] | select(.Name | contains(\"$CONTAINER_NAME\")) | .IPv4Address" \
    | sed 's/\"//g' \
    | awk -F'/' '{print $1}'
